I. Set up environment
------------------------------------------------------

This project contains Enterprise Java Beans (EJB's) modules pre-configured for using with JBoss EAP 7 server.

1. Download and install JBoss EAP (https://developers.redhat.com/products/eap/download/).
2. Set up MySQL(https://www.mysql.com/) on server with 'Domain' submodule;
    * Create MySQL user for accessing data. You can add default user with username `test` and password `test1` as it's default combination for developer's setup;
    * Add MySQL datasource to JBoss instance: datasource name is specified inside '/bank-domain-impl/src/main/resources/persistence.xml' file and defaults to `artezio-school-datasource`. Do not forget to import MySQL connector as JBoss module.   
    * Refer to https://docs.jboss.org/author/display/AS71/DataSource+configuration for datasource configuration example.
    * Install liquibase (http://download.liquibase.org/).   
4. Install Maven (https://maven.apache.org/)
5. Install Node.js and NPM (https://nodejs.org/en/download/)
6. Make sure 'mvn', 'npm' are available in $PATH

II. Build
---------------------------------------------------------

1. Navigate to project root
2. Choose one that applies:
    * If you are going to run all modules on single JBoss instance - run ```mvn clean install -P BuildForLocalDeploy```
    * If you are going to run all modules on different JBoss instances - Run ```mvn clean install```

III. Deploy
---------------------------------------------------------

Before deploying 'Domain'(bank-domain) submodule, one must run liquibase using either Maven or Liquibase.

First, specify connection params in /bank-domain/bank-domain-impl/src/resources/liquibase/liquibase.properties

Then run update:

 - (option a) Using Maven. Navigate to project root and run `mvn -P   LiquibaseProduction liquibase:update`

 - (option b) Using Liquibase: Add Mysql connector JAR to classpath, then navigate to /bank-domain/bank-domain-impl/src/resources/liquibase/ and run ```liquibase update```

Web application is designed to serve at server root (`/`). Remove default server root context ('/') by editing standalone.xml file.

III A. Deploying all modules to a single server instance
---------------------------------------------------------
Just deploy bank-domain-1.0.ear, bank-services-1.0.ear and bank-web.war in standalone mode (put them in `server/standalone/deployments`)

III B. Deploying modules to multiple server instances
---------------------------------------------------------
Configure JBoss security domains for EJB access, as stated in docs:

1. Add remote application users to servers containing Domain and Service modules.

2. Create outbound domain and connection for Web and Service servers, then edit connection names in corresponding files

    - bank-services/bank-services-ear/META-INF/jboss-ejb-client.xml - Services module for accessing Domain module

    - bank-web/WEB-INF/jboss-ejb-client.xml - Web module for accessing Services module

3. Check each jboss-ejb-client.properties file and set corresponding security options and connection attributes

4. Rebuild the project

IV. Using the application
---------------------------------------------------------
Navigate to http://your-host:your-port/ page with bank-web module deployed.

