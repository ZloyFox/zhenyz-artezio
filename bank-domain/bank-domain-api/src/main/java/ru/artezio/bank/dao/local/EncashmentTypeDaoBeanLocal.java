package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.EncashmentTypeDao;

import javax.ejb.Local;

@Local
public interface EncashmentTypeDaoBeanLocal extends EncashmentTypeDao {
}
