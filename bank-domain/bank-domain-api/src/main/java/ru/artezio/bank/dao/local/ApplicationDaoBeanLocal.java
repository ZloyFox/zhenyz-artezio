package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.ApplicationDao;

import javax.ejb.Local;

@Local
public interface ApplicationDaoBeanLocal extends ApplicationDao {
}
