package ru.artezio.bank.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@DynamicUpdate
@Table(name = "application")
@NamedQueries({
	@NamedQuery(name = "ApplicationModel.findByCreatorId", query = "SELECT o FROM ApplicationModel o WHERE o.creator.id=:userId ORDER BY :sortFields"),
	@NamedQuery(name = "ApplicationModel.findByIdAndCreator", query = "SELECT o FROM ApplicationModel o WHERE o.id=:id AND o.creator.id=:creatorId"),
})
public class ApplicationModel extends BaseEntity<Long> {

	@Column(name="id")
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "creator_id", nullable = false, updatable = false)
	private UserModel creator;

	@ManyToOne
	@JoinColumn(name = "bank_object_id", nullable = false)
	private BankObjectModel bankObject;

	@ManyToOne
	@JoinColumn(name = "application_type_id", nullable = false)
	private ApplicationTypeModel applicationType;

	@Column(name = "date")
	private Date date;

	@Column(name = "inn")
	private String inn;	// local taxpayer id

	@Column(name = "kpp")
	private String kpp;	// tax service id

	@Column(name = "organization_name")
	private String organizationName;

	@Column(name = "ogrn")
	private String ogrn;	// organization registration code

	@Column(name = "contact_name")
	private String contactName;

	@Column(name = "contact_phone")
	private String contactPhone;

	@Column(name = "client_account")
	private String clientAccount; // destination account

	@Column(name = "bank_code")
	private String bankCode; // destination bank

	@Column(name = "bank_account_number")
	private String bankAccountNumber;	// destination bank account

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "bank_swift")
	private String bankSwift;

	@Column(name = "bank_other_info")
	private String bankOtherInfo;

	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name = "application_incassation_object",
			joinColumns = { @JoinColumn(name = "application_id", nullable = false, updatable = false) },
			inverseJoinColumns = {@JoinColumn(name = "incassation_object_id", nullable = false, updatable = false)})
	private List<IncassationObjectModel> incassationObjects = new ArrayList<>();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public UserModel getCreator() {
		return creator;
	}

	public void setCreator(UserModel creator) {
		this.creator = creator;
	}

	public BankObjectModel getBankObject() {
		return bankObject;
	}

	public void setBankObject(BankObjectModel bankObject) {
		this.bankObject = bankObject;
	}

	public ApplicationTypeModel getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(ApplicationTypeModel applicationType) {
		this.applicationType = applicationType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankSwift() {
		return bankSwift;
	}

	public void setBankSwift(String bankSwift) {
		this.bankSwift = bankSwift;
	}

	public String getBankOtherInfo() {
		return bankOtherInfo;
	}

	public void setBankOtherInfo(String bankOtherInfo) {
		this.bankOtherInfo = bankOtherInfo;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

	public String getKpp() {
		return kpp;
	}

	public void setKpp(String kpp) {
		this.kpp = kpp;
	}

	public String getOgrn() {
		return ogrn;
	}

	public void setOgrn(String ogrn) {
		this.ogrn = ogrn;
	}

	public String getClientAccount() {
		return clientAccount;
	}

	public void setClientAccount(String clientAccount) {
		this.clientAccount = clientAccount;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bank) {
		this.bankCode = bank;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public List<IncassationObjectModel> getIncassationObjects() {
		return incassationObjects;
	}

	public void setIncassationObjects(List<IncassationObjectModel> incassationObjects) {
		this.incassationObjects = incassationObjects;
	}

	@Override
	public String toString() {
		return String.format("Application[id=%d]", id);
	}
}
