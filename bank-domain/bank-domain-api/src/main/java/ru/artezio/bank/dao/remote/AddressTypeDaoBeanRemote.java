package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.AddressTypeDao;

import javax.ejb.Remote;

@Remote
public interface AddressTypeDaoBeanRemote extends AddressTypeDao {
}
