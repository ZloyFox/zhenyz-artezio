package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.entity.BankObjectModel;

import java.util.List;

public interface BankObjectDao {

	List<BankObjectModel> getAll(QueryInfo queryInfo, Long demanderUserId) throws AccessViolationException;
	List<BankObjectModel> findByAddress(String address, QueryInfo queryInfo);
	BankObjectModel getById(Long id);
	BankObjectModel update(BankObjectModel model, Long updaterUserId) throws AccessViolationException;
	BankObjectModel create(BankObjectModel model, Long creatorId) throws AccessViolationException;

}
 