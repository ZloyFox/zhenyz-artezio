package ru.artezio.bank.dao;

import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.entity.ApplicationModel;

import java.util.List;

public interface ApplicationDao {
	ApplicationModel getById(Long id, Long userId);
	List<ApplicationModel> findByCreatorId(Long userId, QueryInfo queryInfo);
	ApplicationModel create(ApplicationModel model);
	void update(ApplicationModel model, Long updaterId);
}
