package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.AddressTypeDao;

import javax.ejb.Local;

@Local
public interface AddressTypeDaoBeanLocal extends AddressTypeDao {
}
