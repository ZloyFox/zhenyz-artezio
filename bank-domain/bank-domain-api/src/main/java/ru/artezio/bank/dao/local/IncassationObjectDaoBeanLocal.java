package ru.artezio.bank.dao.local;

import javax.ejb.Local;

@Local
public interface IncassationObjectDaoBeanLocal extends ru.artezio.bank.dao.IncassationObjectDao {
}
