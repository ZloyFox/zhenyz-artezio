package ru.artezio.bank.dao.helper;

import java.io.Serializable;

public class PagingInfo implements Serializable {

	private final Integer offset;
	private final Integer count;

	public PagingInfo(Integer offset, Integer count) {
		this.offset = offset;
		this.count = count;
	}

	public Integer getOffset() {
		return offset;
	}

	public Integer getCount() {
		return count;
	}

}
