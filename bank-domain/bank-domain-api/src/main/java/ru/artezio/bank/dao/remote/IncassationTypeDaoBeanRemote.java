package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.IncassationTypeDao;

import javax.ejb.Remote;

@Remote
public interface IncassationTypeDaoBeanRemote extends IncassationTypeDao {
}
