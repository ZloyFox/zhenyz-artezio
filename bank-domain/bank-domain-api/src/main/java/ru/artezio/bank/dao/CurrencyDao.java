package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.CurrencyModel;

import java.util.List;

public interface CurrencyDao {

	List<CurrencyModel> getAll();
	CurrencyModel getById(Short id);
	CurrencyModel update(CurrencyModel model, Long updaterUserId) throws AccessViolationException;

}
