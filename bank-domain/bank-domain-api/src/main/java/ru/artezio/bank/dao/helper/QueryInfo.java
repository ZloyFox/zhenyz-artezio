package ru.artezio.bank.dao.helper;

import java.io.Serializable;

public class QueryInfo implements Serializable {
	private final OrderDirection orderDirection;
	private final PagingInfo pagingInfo;
	private String[] sortedFields;

	public QueryInfo(OrderDirection dir, PagingInfo paging) {
		this.orderDirection = dir;
		this.pagingInfo = paging;
	}

	public String[] getSortedFields() {
		return sortedFields;
	}

	public void setSortedFields(String... sortedFields) {
		this.sortedFields = sortedFields;
	}

	public OrderDirection getOrderDirection() {
		return orderDirection;
	}

	public PagingInfo getPagingInfo() {
		return pagingInfo;
	}
}
