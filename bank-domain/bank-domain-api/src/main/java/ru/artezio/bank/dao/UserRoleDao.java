package ru.artezio.bank.dao;

import ru.artezio.bank.entity.UserRoleModel;

import java.util.List;
import java.util.Set;

public interface UserRoleDao {
	Set<UserRoleModel> getGuestRoles();
	UserRoleModel findByName(String name);
	List<UserRoleModel> getAll();
	UserRoleModel getById(Long id);
	UserRoleModel getAdminRole();
}
