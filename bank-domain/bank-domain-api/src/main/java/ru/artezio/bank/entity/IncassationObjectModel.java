package ru.artezio.bank.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

@Entity
@DynamicUpdate
@Table(name = "incassation_object")
@NamedQueries({
	@NamedQuery(name = "IncassationObjectModel.findByCreatorId", query = "SELECT o FROM IncassationObjectModel o WHERE o.creator.id=:userId ORDER BY :sortFields"),
})
public class IncassationObjectModel extends BaseEntity<Long> {

	@Column(name="id")
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "creator_id", nullable = false, updatable = false)
	private UserModel creator;

	@Column(name = "incassation_time")
	private Time time;

	@JoinColumn(name="encashment_type_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private EncashmentTypeModel encashmentType;

	@JoinColumn(name="incassation_type_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private IncassationTypeModel incassationType;

	@JoinColumn(name="incassation_period_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private IncassationPeriodModel incassationPeriod;

	@Column(name = "dayofweek")
	private Integer dayOfWeek;

	@Column(name = "approx_cash")
	private BigDecimal approxCash;

	@JoinColumn(name = "currency_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private CurrencyModel currency;

	@Column(name="director_contact_phone")
	private String directorContactPhone;

	@Column(name = "opening_date")
	private Date openingDate;

	@Column(name = "is_shared")
	private Boolean shared;

	@Column(name = "status")
	@Enumerated(EnumType.ORDINAL)
	private IncassationObjectStatus status = IncassationObjectStatus.ACTIVE;

	@Column(name = "opening_time_weekdays")
	private Time openingTimeWeekdays;
	@Column(name = "opening_time_saturday")
	private Time openingTimeSaturday;
	@Column(name = "opening_time_sunday")
	private Time openingTimeSunday;
	@Column(name = "closing_time_weekdays")
	private Time closingTimeWeekdays;
	@Column(name = "closing_time_saturday")
	private Time closingTimeSaturday;
	@Column(name = "closing_time_sunday")
	private Time closingTimeSunday;

	@JoinColumn(name = "address_type_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private AddressTypeModel addressType;

	@JoinColumn(name = "settlement_type_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private SettlementTypeModel settlementType;

	@Column(name = "settlement_name", length = 100)
	private String settlementName;

	@Column(name = "street_name", length = 100)
	private String streetName;

	@Column(name = "building_number", length = 20)
	private String buildingNumber;

	@Column(name = "building_block_number", length = 20)
	private String buildingBlockNumber;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public EncashmentTypeModel getEncashmentType() {
		return encashmentType;
	}

	public void setEncashmentType(EncashmentTypeModel encashmentType) {
		this.encashmentType = encashmentType;
	}

	public IncassationPeriodModel getIncassationPeriod() {
		return incassationPeriod;
	}

	public void setIncassationPeriod(IncassationPeriodModel incassationPeriod) {
		this.incassationPeriod = incassationPeriod;
	}

	public Integer getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public BigDecimal getApproxCash() {
		return approxCash;
	}

	public void setApproxCash(BigDecimal approxCash) {
		this.approxCash = approxCash;
	}

	public CurrencyModel getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyModel currency) {
		this.currency = currency;
	}

	public String getDirectorContactPhone() {
		return directorContactPhone;
	}

	public void setDirectorContactPhone(String directorContactPhone) {
		this.directorContactPhone = directorContactPhone;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public Time getOpeningTimeWeekdays() {
		return openingTimeWeekdays;
	}

	public void setOpeningTimeWeekdays(Time openingTimeWeekdays) {
		this.openingTimeWeekdays = openingTimeWeekdays;
	}

	public Time getOpeningTimeSaturday() {
		return openingTimeSaturday;
	}

	public void setOpeningTimeSaturday(Time openingTimeSaturday) {
		this.openingTimeSaturday = openingTimeSaturday;
	}

	public Time getOpeningTimeSunday() {
		return openingTimeSunday;
	}

	public void setOpeningTimeSunday(Time openingTimeSunday) {
		this.openingTimeSunday = openingTimeSunday;
	}

	public Time getClosingTimeWeekdays() {
		return closingTimeWeekdays;
	}

	public void setClosingTimeWeekdays(Time closingTimeWeekdays) {
		this.closingTimeWeekdays = closingTimeWeekdays;
	}

	public Time getClosingTimeSaturday() {
		return closingTimeSaturday;
	}

	public void setClosingTimeSaturday(Time closingTimeSaturday) {
		this.closingTimeSaturday = closingTimeSaturday;
	}

	public Time getClosingTimeSunday() {
		return closingTimeSunday;
	}

	public void setClosingTimeSunday(Time closingTimeSunday) {
		this.closingTimeSunday = closingTimeSunday;
	}

	public AddressTypeModel getAddressType() {
		return addressType;
	}

	public void setAddressType(AddressTypeModel addressType) {
		this.addressType = addressType;
	}

	public SettlementTypeModel getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(SettlementTypeModel settlementType) {
		this.settlementType = settlementType;
	}

	public String getSettlementName() {
		return settlementName;
	}

	public void setSettlementName(String settlementName) {
		this.settlementName = settlementName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingBlockNumber() {
		return buildingBlockNumber;
	}

	public void setBuildingBlockNumber(String buildingBlockNumber) {
		this.buildingBlockNumber = buildingBlockNumber;
	}

	public Boolean isShared() {
		return shared;
	}

	public void setShared(Boolean shared) {
		this.shared = shared;
	}

	public IncassationObjectStatus getStatus() {
		return status;
	}

	public void setStatus(IncassationObjectStatus status) {
		this.status = status;
	}

	public IncassationTypeModel getIncassationType() {
		return incassationType;
	}

	public void setIncassationType(IncassationTypeModel incassationType) {
		this.incassationType = incassationType;
	}

	public UserModel getCreator() {
		return creator;
	}

	public void setCreator(UserModel creator) {
		this.creator = creator;
	}

	@Override
	public String toString() {
		return String.format("IncassationObject[id=%d]", id);
	}
}
