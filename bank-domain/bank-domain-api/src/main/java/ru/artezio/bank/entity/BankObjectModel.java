package ru.artezio.bank.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@DynamicUpdate
@Table(name = "bank_object")
@NamedQueries({
	@NamedQuery(name = "BankObjectModel.findByAddress", query = "SELECT bo FROM BankObjectModel bo WHERE bo.address LIKE :addr"),
})
public class BankObjectModel extends BaseEntity<Long> {

	@Column(name="id")
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "creator_id", nullable = false, updatable = false)
	private UserModel creator;

	@Column(name = "address")
	private String address;

	@Column(name="name")
	private String name;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public UserModel getCreator() {
		return creator;
	}

	public void setCreator(UserModel creator) {
		this.creator = creator;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("BankObject[id=%d]", id);
	}
}
