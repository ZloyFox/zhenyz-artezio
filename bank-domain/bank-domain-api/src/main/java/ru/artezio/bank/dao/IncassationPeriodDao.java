package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.IncassationPeriodModel;

import java.util.List;

public interface IncassationPeriodDao {

	List<IncassationPeriodModel> getAll();
	IncassationPeriodModel getById(Long id);
	IncassationPeriodModel update(IncassationPeriodModel model, Long updaterUserId) throws AccessViolationException;

}
 