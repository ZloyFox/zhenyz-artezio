package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.ApplicationTypeDao;

import javax.ejb.Remote;

@Remote
public interface ApplicationTypeDaoBeanRemote extends ApplicationTypeDao {
}
