package ru.artezio.bank.entity;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;

@MappedSuperclass
public abstract class BaseEntity<T> implements Serializable {

	abstract public T getId();

	abstract public void setId(T id);

	@Transient
	public Boolean isNew() {
		return getId() == null;
	}

	@Override
	public String toString() {
		return String.format("%s[%s]", getClass().getName(), getId());
	}

}