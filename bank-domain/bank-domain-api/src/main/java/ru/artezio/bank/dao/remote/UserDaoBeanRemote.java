package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.UserDao;

import javax.ejb.Remote;

@Remote
public interface UserDaoBeanRemote extends UserDao {

}
