package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.IncassationTypeModel;

import java.util.List;

public interface IncassationTypeDao {

	List<IncassationTypeModel> getAll();
	IncassationTypeModel getById(Long id);
	IncassationTypeModel update(IncassationTypeModel model, Long updaterUserId) throws AccessViolationException;

}
