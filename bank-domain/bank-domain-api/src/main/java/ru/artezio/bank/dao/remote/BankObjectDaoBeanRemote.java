package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.BankObjectDao;

import javax.ejb.Remote;

@Remote
public interface BankObjectDaoBeanRemote extends BankObjectDao {
}
