package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.UserDao;

import javax.ejb.Local;

@Local
public interface UserDaoBeanLocal extends UserDao {

}
