package ru.artezio.bank.dao.exception;

public class AccessViolationException extends Exception {
	public AccessViolationException() {
		super("Access denied");
	}
}
