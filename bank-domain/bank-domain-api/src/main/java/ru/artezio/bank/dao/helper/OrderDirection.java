package ru.artezio.bank.dao.helper;

public enum OrderDirection {
	ASC("ASC"),
	DESC("DESC");

	private String value;

	OrderDirection(String value) {
		this.value = value.toUpperCase();
	}

	@Override
	public String toString() {
		return this.value;
	}
}
