package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.SettlementTypeModel;

import java.util.List;

public interface SettlementTypeDao {

	List<SettlementTypeModel> getAll();
	SettlementTypeModel getById(Long id);
	SettlementTypeModel update(SettlementTypeModel model, Long updaterUserId) throws AccessViolationException;

}
 