package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.IncassationPeriodDao;

import javax.ejb.Remote;

@Remote
public interface IncassationPeriodDaoBeanRemote extends IncassationPeriodDao {
}
