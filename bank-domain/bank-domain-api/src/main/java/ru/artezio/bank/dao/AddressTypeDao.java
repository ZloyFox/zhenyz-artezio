package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.AddressTypeModel;

import java.util.List;

public interface AddressTypeDao {

	List<AddressTypeModel> getAll();
	AddressTypeModel getById(Long id);
	AddressTypeModel update(AddressTypeModel model, Long updaterUserId) throws AccessViolationException;

}
