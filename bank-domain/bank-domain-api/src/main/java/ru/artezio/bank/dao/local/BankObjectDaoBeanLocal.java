package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.BankObjectDao;

import javax.ejb.Local;

@Local
public interface BankObjectDaoBeanLocal extends BankObjectDao {
}
