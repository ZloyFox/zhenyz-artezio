package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.UserRoleDao;

import javax.ejb.Local;

@Local
public interface UserRoleDaoBeanLocal extends UserRoleDao {

}
