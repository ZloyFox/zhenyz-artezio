package ru.artezio.bank.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DynamicUpdate
@Table(name = "user")
@NamedQueries({
		@NamedQuery(name = "UserModel.findByLogin", query = "SELECT u FROM UserModel u WHERE u.login=:login"),
		@NamedQuery(name = "UserModel.findByLoginPassword", query = "SELECT u FROM UserModel u WHERE u.login=:login AND u.password=:password"),
})
public class UserModel extends BaseEntity<Long> {

	@Column(name = "login", unique = true, updatable = false, length = 50)
	private String login;

	@Column(name = "password", length = 50)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(
			name = "user_user_role",
			joinColumns = { @JoinColumn(name = "user_id", nullable = false, updatable = false) },
			inverseJoinColumns = {@JoinColumn(name = "role_id", nullable = false, updatable = false)})
	private Set<UserRoleModel> userRoles = new HashSet<>();

	@Column(name="is_blocked")
	private Boolean blocked = false;

	@Column(name="id")
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany(mappedBy = "creator", cascade = {CascadeType.DETACH, CascadeType.PERSIST})
	private List<IncassationObjectModel> ownedIncassationObjects;

	@Override
	public Long getId() {
		return id;
	}

	public Set<UserRoleModel> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set<UserRoleModel> userRoles) {
		this.userRoles = userRoles;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public List<IncassationObjectModel> getOwnedIncassationObjects() {
		return ownedIncassationObjects;
	}

	public boolean hasRole(UserRoleModel role) {
		return this.userRoles.contains(role);
	}

	@Override
	public String toString() {
		return String.format("UserModel[id=%d, login=%s, roles=%s, %s]", id, login, userRoles, Boolean.toString(blocked));
	}

}
