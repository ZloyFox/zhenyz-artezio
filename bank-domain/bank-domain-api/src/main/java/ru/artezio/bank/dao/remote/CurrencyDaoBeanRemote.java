package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.CurrencyDao;

import javax.ejb.Remote;

@Remote
public interface CurrencyDaoBeanRemote extends CurrencyDao {
}
