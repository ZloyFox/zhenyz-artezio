package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.EncashmentTypeModel;

import java.util.List;

public interface EncashmentTypeDao {

	List<EncashmentTypeModel> getAll();
	EncashmentTypeModel getById(Long id);
	EncashmentTypeModel update(EncashmentTypeModel model, Long updaterUserId) throws AccessViolationException;

}
