package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.entity.IncassationObjectModel;

import java.util.List;

public interface IncassationObjectDao {

	List<IncassationObjectModel> getAll(Long userId, QueryInfo queryInfo) throws AccessViolationException;
	IncassationObjectModel getById(Long id, Long userId) throws AccessViolationException;
	List<IncassationObjectModel> findByCreatorId(Long userId, QueryInfo queryInfo);
	IncassationObjectModel create(IncassationObjectModel model);
	IncassationObjectModel update(IncassationObjectModel model, Long updaterId) throws AccessViolationException;

}
