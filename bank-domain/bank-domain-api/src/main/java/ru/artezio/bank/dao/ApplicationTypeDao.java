package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.entity.ApplicationTypeModel;

import java.util.List;

public interface ApplicationTypeDao {

	List<ApplicationTypeModel> getAll();
	ApplicationTypeModel getById(Long id);
	ApplicationTypeModel update(ApplicationTypeModel model, Long updaterUserId) throws AccessViolationException;

}
 