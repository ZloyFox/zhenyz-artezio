package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.SettlementTypeDao;

import javax.ejb.Local;

@Local
public interface SettlementTypeDaoBeanLocal extends SettlementTypeDao {
}
