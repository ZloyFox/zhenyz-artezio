package ru.artezio.bank.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@DynamicUpdate
@Table(name = "user_role")
@NamedQueries({
		@NamedQuery(name = "UserRoleModel.getGuestRoles", query = "SELECT ur FROM UserRoleModel ur WHERE ur.guest<>0"),
		@NamedQuery(name = "UserRoleModel.findByName", query = "SELECT ur FROM UserRoleModel ur WHERE ur.name=:name"),
})
public class UserRoleModel extends BaseEntity<Long> {

	@Column(name = "id",unique = true)
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name",unique = true,length = 40)
	private String name;

	@Column(name="is_guest")
	private boolean guest;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isGuest() {
		return guest;
	}

	public void setGuest(boolean guest) {
		this.guest = guest;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserRoleModel)) {
			return false;
		}
		return this.id.equals(((UserRoleModel)obj).getId());
	}

	@Override
	public int hashCode() {
		return new Long(id).hashCode();
	}

	@Override
	public String toString() {
		return String.format("UserRoleModel[id=%d, name=%s, guest=%s]", id, name, guest);
	}

}
