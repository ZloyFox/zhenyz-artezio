package ru.artezio.bank.dao;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.exception.UserAlreadyExistsException;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.entity.UserModel;

import java.util.List;

public interface UserDao {
	UserModel getById(Long id);
	List<UserModel> getAll(Long demanderId, QueryInfo queryInfo) throws AccessViolationException;
	UserModel create(UserModel model) throws UserAlreadyExistsException;
	UserModel update(UserModel model, Long demanderId) throws AccessViolationException;
	UserModel findByLoginPassword(String login, String password);

	boolean isUserAdmin(Long userId);
	boolean isLoginExists(String login);
}
