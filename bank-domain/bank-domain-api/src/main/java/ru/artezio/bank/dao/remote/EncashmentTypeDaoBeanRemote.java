package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.EncashmentTypeDao;

import javax.ejb.Remote;

@Remote
public interface EncashmentTypeDaoBeanRemote extends EncashmentTypeDao {
}
