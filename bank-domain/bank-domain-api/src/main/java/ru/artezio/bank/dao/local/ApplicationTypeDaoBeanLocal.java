package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.ApplicationTypeDao;

import javax.ejb.Local;

@Local
public interface ApplicationTypeDaoBeanLocal extends ApplicationTypeDao {
}
