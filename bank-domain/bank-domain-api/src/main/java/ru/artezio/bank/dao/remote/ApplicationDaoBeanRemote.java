package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.ApplicationDao;

import javax.ejb.Remote;

@Remote
public interface ApplicationDaoBeanRemote extends ApplicationDao {
}
