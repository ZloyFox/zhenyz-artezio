package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.IncassationObjectDao;

import javax.ejb.Remote;

@Remote
public interface IncassationObjectDaoBeanRemote extends IncassationObjectDao {
}
