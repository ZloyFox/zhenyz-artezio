package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.IncassationPeriodDao;

import javax.ejb.Local;

@Local
public interface IncassationPeriodDaoBeanLocal extends IncassationPeriodDao {
}
