package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.UserRoleDao;

import javax.ejb.Remote;

@Remote
public interface UserRoleDaoBeanRemote extends UserRoleDao {

}
