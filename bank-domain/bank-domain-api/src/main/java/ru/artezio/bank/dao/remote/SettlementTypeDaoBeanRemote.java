package ru.artezio.bank.dao.remote;

import ru.artezio.bank.dao.SettlementTypeDao;

import javax.ejb.Remote;

@Remote
public interface SettlementTypeDaoBeanRemote extends SettlementTypeDao {
}
