package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.IncassationTypeDao;

import javax.ejb.Local;

@Local
public interface IncassationTypeDaoBeanLocal extends IncassationTypeDao {
}
