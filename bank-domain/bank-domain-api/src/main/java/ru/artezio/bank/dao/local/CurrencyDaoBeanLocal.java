package ru.artezio.bank.dao.local;

import ru.artezio.bank.dao.CurrencyDao;

import javax.ejb.Local;

@Local
public interface CurrencyDaoBeanLocal extends CurrencyDao {
}
