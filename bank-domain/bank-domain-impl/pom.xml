<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	
	<modelVersion>4.0.0</modelVersion>
	<groupId>artezio-school</groupId>
	<artifactId>bank-domain-impl</artifactId>
	<version>1.0</version>
	<packaging>ejb</packaging>

	<properties>
		<log4j.version>2.8</log4j.version>
		<slf4j.version>1.7.22</slf4j.version>
		<hibernate.version>5.2.6.Final</hibernate.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<maven.compiler.target>1.8</maven.compiler.target>
		<maven.compiler.source>1.8</maven.compiler.source>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-ejb-plugin</artifactId>
				<version>2.5.1</version>
				<configuration>
					<ejbVersion>3.2</ejbVersion>
					<archive>
						<manifest>
							<addClasspath>true</addClasspath>
							<classpathPrefix>lib/</classpathPrefix>
						</manifest>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.10</version>
				<executions>
<!--					<execution>
						<id>add-api-classes</id>
						<phase>compile</phase>
						<goals>
							<goal>unpack</goal>
						</goals>
						<configuration>
							<artifactItems>
								<artifactItem>
									<groupId>artezio-school</groupId>
									<artifactId>bank-domain-api</artifactId>
									<version>${project.version}</version>
									<type>jar</type>
									<overWrite>false</overWrite>
									<outputDirectory>${project.build.outputDirectory}</outputDirectory>
									<includes>**/*.class</includes>
								</artifactItem>
							</artifactItems>
						</configuration>
					</execution>-->
					<execution>
						<id>add-dependencies</id>
						<phase>process-resources</phase>
						<goals>
							<goal>copy-dependencies</goal>
						</goals>
						<configuration>
							<outputDirectory>target/classes/WEB-INF/lib</outputDirectory>
							<includeScope>runtime</includeScope>
							<excludeTransitive>true</excludeTransitive>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>7.0</version>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>5.1.25</version>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>artezio-school</groupId>
			<artifactId>bank-domain-api</artifactId>
			<version>${project.version}</version>
		</dependency>

		<!-- Persistence -->
		<!-- https://mvnrepository.com/artifact/org.hibernate.javax.persistence/hibernate-jpa-2.0-api -->
		<!-- Hibernate -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
			<version>${hibernate.version}</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate.javax.persistence</groupId>
			<artifactId>hibernate-jpa-2.1-api</artifactId>
			<version>1.0.0.Final</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
			<version>RELEASE</version>
		</dependency>

		<!-- Logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<!-- Log4j API and Core implementation required for binding -->
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-api</artifactId>
			<version>${log4j.version}</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
			<version>${log4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-slf4j-impl</artifactId>
			<version>${log4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-core</artifactId>
			<version>3.1.0.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>RELEASE</version>
		</dependency>

		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-core</artifactId>
			<version>3.5.3</version>
		</dependency>
	</dependencies>

	<profiles>
		<profile>
			<id>LiquibaseTest</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<build>
				<plugins>
					<!-- Liquibase -->
					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.0.5</version>
						<configuration>
							<propertyFile>/src/main/resources/liquibase/liquibase-test.properties</propertyFile>
							<outputFileEncoding>UTF-8</outputFileEncoding>
						</configuration>
						<executions>
							<execution>
								<phase>generate-resources</phase>
								<goals>
									<goal>update</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<id>LiquibaseProduction</id>
			<build>
				<plugins>
					<!-- Liquibase -->
					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.0.5</version>
						<configuration>
							<propertyFile>/src/main/resources/liquibase/liquibase.properties</propertyFile>
							<outputFileEncoding>UTF-8</outputFileEncoding>
						</configuration>
						<executions>
							<execution>
								<phase>generate-resources</phase>
								<goals>
									<goal>update</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

	</profiles>

</project>