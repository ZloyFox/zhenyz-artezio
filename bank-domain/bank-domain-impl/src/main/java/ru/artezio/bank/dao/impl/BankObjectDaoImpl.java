package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.QueryParam;
import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.local.BankObjectDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.BankObjectDaoBeanRemote;
import ru.artezio.bank.entity.BankObjectModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class BankObjectDaoImpl extends GenericDaoImpl<Long, BankObjectModel>
		implements BankObjectDaoBeanRemote, BankObjectDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public BankObjectModel update(BankObjectModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}

	@Override
	public BankObjectModel create(BankObjectModel model, Long creatorId) throws AccessViolationException {
		if (!userDao.isUserAdmin(creatorId)) {
			throw new AccessViolationException();
		}
		return save(model);
	}

	@Override
	public List<BankObjectModel> findByAddress(String address, QueryInfo queryInfo) {
		List<QueryParam> params = new ArrayList<>();
		params.add(new QueryParam("address", address));
		queryInfo.setSortedFields("id");
		return findObjectsByNamedQuery("BankObjectModel.findByAddress", params, queryInfo);
	}

	@Override
	public BankObjectModel getById(Long id) {
		return get(id);
	}

	@Override
	public List<BankObjectModel> getAll(QueryInfo queryInfo, Long demanderUserId) throws AccessViolationException {
		return list(queryInfo);
	}

}
