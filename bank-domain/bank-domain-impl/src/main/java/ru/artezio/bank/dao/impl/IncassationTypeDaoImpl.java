package ru.artezio.bank.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.IncassationTypeDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.IncassationTypeDaoBeanRemote;
import ru.artezio.bank.entity.IncassationTypeModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Repository
@Transactional
@Stateless
public class IncassationTypeDaoImpl extends GenericDaoImpl<Long, IncassationTypeModel>
		implements IncassationTypeDaoBeanRemote, IncassationTypeDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<IncassationTypeModel> getAll() {
		return list();
	}

	@Override
	public IncassationTypeModel getById(Long id) {
		return get(id);
	}

	@Override
	public IncassationTypeModel update(IncassationTypeModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}
}
