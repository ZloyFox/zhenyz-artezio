package ru.artezio.bank.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.artezio.bank.dao.QueryParam;
import ru.artezio.bank.dao.helper.PagingInfo;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.entity.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.List;

import static org.springframework.util.StringUtils.arrayToDelimitedString;

public abstract class GenericDaoImpl<U, T extends BaseEntity<U>> {

	private static final Integer MAX_COUNT = 50;

	@PersistenceContext(unitName = "artezio_bank_persistence") private EntityManager entityManager;

	private Class<T> entityClass;
	protected Logger log = LoggerFactory.getLogger(getClass().getName());

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.entityClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
		log.trace("Constructing for {}", this.entityClass);
	}

	@SuppressWarnings("unchecked")
	T findObjectByNamedQuery(String queryName, List<QueryParam> queryParams) throws NoResultException {
		log.trace("Query {}, params {}", queryName, queryParams);
		Query namedQuery = prepareNamedQuery(queryName,queryParams);
		return (T)namedQuery.getSingleResult();
	}

	T findObjectByNamedQuery(String queryName) {
		return findObjectByNamedQuery(queryName, Collections.<QueryParam>emptyList());
	}

	@SuppressWarnings("unchecked")
	List<T> findObjectsByNamedQuery(String queryName, List<QueryParam> queryParams) {
		Query namedQuery = prepareNamedQuery(queryName, queryParams);
		return namedQuery.getResultList();
	}

	@SuppressWarnings("unchecked")
	List<T> findObjectsByNamedQuery(String queryName, List<QueryParam> queryParams, QueryInfo queryInfo) {
		Query namedQuery = prepareNamedQuery(queryName, queryParams);
		applyPagingToQuery(namedQuery, queryInfo.getPagingInfo());

		namedQuery.setParameter("sortFields", arrayToDelimitedString(queryInfo.getSortedFields(), ","));
		return namedQuery.getResultList();
	}

	List<T> findObjectsByNamedQuery(String queryName) {
		return findObjectsByNamedQuery(queryName, Collections.<QueryParam>emptyList());
	}

	List<T> findObjectsByNamedQuery(String queryName, QueryInfo queryInfo) {
		return findObjectsByNamedQuery(queryName, Collections.<QueryParam>emptyList(), queryInfo);
	}

	T save(T model) {
		return entityManager.merge(model);
	}

	T update(T model) {
		return entityManager.merge(model);
	}

	T get(U id) {
		return entityManager.find(entityClass, id);
	}

	void persist(T model) {
		entityManager.persist(model);
	}

	void remove(T model) {
		entityManager.remove(model);
	}

	@SuppressWarnings("unchecked")
	List<T> list() {
		Query q = entityManager.createQuery("FROM " + entityClass.getSimpleName());
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	List<T> list(QueryInfo queryInfo) {
		String queryString = "FROM " + entityClass.getSimpleName();
		if (queryInfo.getSortedFields().length > 0) {
			queryString += " ORDER BY " + arrayToDelimitedString(queryInfo.getSortedFields(), ",") + " ";
			queryString += queryInfo.getOrderDirection();
		}
		Query query = entityManager.createQuery(queryString);
		applyPagingToQuery(query, queryInfo.getPagingInfo());
		return query.getResultList();
	}

	// internal

	private Query prepareNamedQuery(String queryName, List<QueryParam> params) {
		Query namedQuery = entityManager.createNamedQuery(queryName);
		for (QueryParam param : params) {
			namedQuery.setParameter(param.getKey(), param.getValue());
		}
		return namedQuery;
	}

	private Query applyPagingToQuery(Query query, PagingInfo pagingInfo) {
		Integer count = pagingInfo.getCount();
		query.setMaxResults(count > MAX_COUNT ? MAX_COUNT : count);
		query.setFirstResult(pagingInfo.getOffset());
		return query;
	}

}
