package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.CurrencyDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.CurrencyDaoBeanRemote;
import ru.artezio.bank.entity.CurrencyModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CurrencyDaoImpl extends GenericDaoImpl<Short, CurrencyModel>
		implements CurrencyDaoBeanRemote, CurrencyDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<CurrencyModel> getAll() {
		return list();
	}

	@Override
	public CurrencyModel getById(Short id) {
		return get(id);
	}

	@Override
	public CurrencyModel update(CurrencyModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}
}
