package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.EncashmentTypeDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.EncashmentTypeDaoBeanRemote;
import ru.artezio.bank.entity.EncashmentTypeModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class EncashmentTypeDaoImpl extends GenericDaoImpl<Long, EncashmentTypeModel>
		implements EncashmentTypeDaoBeanRemote, EncashmentTypeDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<EncashmentTypeModel> getAll() {
		return list();
	}

	@Override
	public EncashmentTypeModel getById(Long id) {
		return get(id);
	}

	@Override
	public EncashmentTypeModel update(EncashmentTypeModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}
}
