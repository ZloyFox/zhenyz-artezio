package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.AddressTypeDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.AddressTypeDaoBeanRemote;
import ru.artezio.bank.entity.AddressTypeModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class AddressTypeDaoImpl extends GenericDaoImpl<Long, AddressTypeModel>
		implements AddressTypeDaoBeanRemote, AddressTypeDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public AddressTypeModel update(AddressTypeModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}

	@Override
	public List<AddressTypeModel> getAll() {
		return list();
	}

	@Override
	public AddressTypeModel getById(Long id) {
		return get(id);
	}

}
