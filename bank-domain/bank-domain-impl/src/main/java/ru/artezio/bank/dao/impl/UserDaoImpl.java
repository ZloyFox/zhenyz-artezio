package ru.artezio.bank.dao.impl;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.artezio.bank.dao.QueryParam;
import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.exception.UserAlreadyExistsException;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.local.UserRoleDaoBeanLocal;
import ru.artezio.bank.dao.remote.UserDaoBeanRemote;
import ru.artezio.bank.entity.UserModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserDaoImpl extends GenericDaoImpl<Long, UserModel> implements UserDaoBeanLocal, UserDaoBeanRemote {

	@EJB private UserRoleDaoBeanLocal userRoleDao;

	@Override
	public UserModel getById(Long id) {
		return get(id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<UserModel> getAll(Long demanderId, QueryInfo queryInfo) throws AccessViolationException {
		// Only admin can view users list
		if (!isUserAdmin(demanderId)) {
			throw new AccessViolationException();
		}
		return list(queryInfo);
	}

	@Override
	public UserModel create(UserModel model) throws UserAlreadyExistsException {
		if (isLoginExists(model.getLogin())) {
			throw new UserAlreadyExistsException();
		}
		String passwordHash = BCrypt.hashpw(model.getPassword(), BCrypt.gensalt());
		model.setPassword(passwordHash);
		return save(model);
	}

	@Override
	public UserModel update(UserModel model, Long demanderId) throws AccessViolationException {
		if (!model.getId().equals(demanderId) && !isUserAdmin(demanderId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}

	@Override
	public boolean isUserAdmin(Long userId) {
		return getById(userId).hasRole(userRoleDao.getAdminRole());
	}

	@Override
	public boolean isLoginExists(String login) {
		return findByLogin(login) != null;
	}

	@Override
	public UserModel findByLoginPassword(String login, String password) {
		UserModel model = findByLogin(login);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		if (password != null && model != null && encoder.matches(password, model.getPassword())) {
			return model;
		}
		return null;
	}

	// internal

	private UserModel findByLogin(String login) {
		List<QueryParam> params = new ArrayList<>();
		params.add(new QueryParam("login",login));
		UserModel model;
		try {
			model = findObjectByNamedQuery("UserModel.findByLogin", params);
		} catch (NoResultException ex) {
			model = null;
		}
		return model;
	}

}
