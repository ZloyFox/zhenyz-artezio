package ru.artezio.bank.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.bank.dao.QueryParam;
import ru.artezio.bank.dao.local.UserRoleDaoBeanLocal;
import ru.artezio.bank.dao.remote.UserRoleDaoBeanRemote;
import ru.artezio.bank.entity.UserRoleModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
@Transactional
@Stateless
public class UserRoleDaoImpl extends GenericDaoImpl<Long, UserRoleModel>
		implements UserRoleDaoBeanLocal, UserRoleDaoBeanRemote {

	@Override
	public Set<UserRoleModel> getGuestRoles() {
		return new HashSet<>(findObjectsByNamedQuery("UserRoleModel.getGuestRoles"));
	}

	@Override
	public UserRoleModel findByName(String name) {
		List<QueryParam> params = new ArrayList<>();
		params.add(new QueryParam("name", name));
		return findObjectByNamedQuery("UserRoleModel.findByName", params);
	}

	@Override
	public List<UserRoleModel> getAll() {
		return list();
	}

	@Override
	public UserRoleModel getById(Long id) {
		return get(id);
	}

	@Override
	public UserRoleModel getAdminRole() {
		return findByName("Admin");
	}

}
