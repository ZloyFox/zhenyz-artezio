package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.QueryParam;
import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.local.IncassationObjectDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.IncassationObjectDaoBeanRemote;
import ru.artezio.bank.entity.IncassationObjectModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class IncassationObjectDaoImpl extends GenericDaoImpl<Long, IncassationObjectModel>
		implements IncassationObjectDaoBeanRemote, IncassationObjectDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<IncassationObjectModel> getAll(Long userId, QueryInfo queryInfo)
			throws AccessViolationException {
		// Only admin can list all incassation objects
		if (!userDao.isUserAdmin(userId)) {
			throw new AccessViolationException();
		}
		queryInfo.setSortedFields("id");
		return list(queryInfo);
	}

	@Override
	public IncassationObjectModel getById(Long id, Long userId) throws AccessViolationException {
		// Only creator and admin can
		IncassationObjectModel model = get(id);
		if (userId.equals(model.getCreator().getId()) || userDao.isUserAdmin(userId)) {
			return model;
		}
		throw new AccessViolationException();
	}

	@Override
	public List<IncassationObjectModel> findByCreatorId(Long userId, QueryInfo queryInfo) {
		List<QueryParam> params = new ArrayList<>();
		params.add(new QueryParam("userId", userId));
		queryInfo.setSortedFields("id");
		return findObjectsByNamedQuery("IncassationObjectModel.findByCreatorId", params, queryInfo);
	}

	@Override
	public IncassationObjectModel create(IncassationObjectModel model) {
		return save(model);
	}

	@Override
	public IncassationObjectModel update(IncassationObjectModel model, Long updaterId) throws AccessViolationException {
		IncassationObjectModel oldModel = get(model.getId());
		// Only creator and admin can
		if (updaterId.equals(oldModel.getCreator().getId()) || userDao.isUserAdmin(updaterId)) {
			return update(model);
		}
		throw new AccessViolationException();
	}

}
