package ru.artezio.bank.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.SettlementTypeDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.SettlementTypeDaoBeanRemote;
import ru.artezio.bank.entity.SettlementTypeModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Repository
@Transactional
@Stateless
public class SettlementTypeDaoImpl extends GenericDaoImpl<Long, SettlementTypeModel>
		implements SettlementTypeDaoBeanRemote, SettlementTypeDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<SettlementTypeModel> getAll() {
		return list();
	}

	@Override
	public SettlementTypeModel getById(Long id) {
		return get(id);
	}

	@Override
	public SettlementTypeModel update(SettlementTypeModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}
}
