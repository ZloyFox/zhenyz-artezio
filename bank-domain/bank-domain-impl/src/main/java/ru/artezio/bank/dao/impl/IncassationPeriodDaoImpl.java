package ru.artezio.bank.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.IncassationPeriodDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.IncassationPeriodDaoBeanRemote;
import ru.artezio.bank.entity.IncassationPeriodModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Repository
@Transactional
@Stateless
public class IncassationPeriodDaoImpl extends GenericDaoImpl<Long, IncassationPeriodModel>
		implements IncassationPeriodDaoBeanRemote, IncassationPeriodDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<IncassationPeriodModel> getAll() {
		return list();
	}

	@Override
	public IncassationPeriodModel getById(Long id) {
		return get(id);
	}

	@Override
	public IncassationPeriodModel update(IncassationPeriodModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}
}
