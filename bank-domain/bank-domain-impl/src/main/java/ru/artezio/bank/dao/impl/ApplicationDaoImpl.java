package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.QueryParam;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.local.ApplicationDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.local.UserRoleDaoBeanLocal;
import ru.artezio.bank.dao.remote.ApplicationDaoBeanRemote;
import ru.artezio.bank.entity.ApplicationModel;
import ru.artezio.bank.entity.UserModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ApplicationDaoImpl extends GenericDaoImpl<Long, ApplicationModel>
		implements ApplicationDaoBeanRemote, ApplicationDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;
	@EJB private UserRoleDaoBeanLocal userRoleDao;

	@Override
	public ApplicationModel getById(Long id, Long userId) throws NoResultException {
		ApplicationModel model = get(id);
		UserModel user = userDao.getById(userId);
		if (userId.equals(model.getCreator().getId()) || user.hasRole(userRoleDao.findByName("Banker"))) {
			return model;
		}
		throw new NoResultException();
	}

	@Override
	public List<ApplicationModel> findByCreatorId(Long userId, QueryInfo queryInfo) {
		List<QueryParam> params = new ArrayList<>();
		params.add(new QueryParam("userId", userId));
		queryInfo.setSortedFields("id");
		return findObjectsByNamedQuery("ApplicationModel.findByCreatorId", params, queryInfo);
	}

	@Override
	public ApplicationModel create(ApplicationModel model) {
		return save(model);
	}

	@Override
	public void update(ApplicationModel model, Long updaterId) throws NoResultException {
		List<QueryParam> params = new ArrayList<>();
		params.add(new QueryParam("id", model.getId()));
		params.add(new QueryParam("creatorId", updaterId));
		findObjectByNamedQuery("ApplicationModel.findByIdAndCreator", params);
		update(model);
	}

}
