package ru.artezio.bank.dao.impl;

import ru.artezio.bank.dao.exception.AccessViolationException;
import ru.artezio.bank.dao.local.ApplicationTypeDaoBeanLocal;
import ru.artezio.bank.dao.local.UserDaoBeanLocal;
import ru.artezio.bank.dao.remote.ApplicationTypeDaoBeanRemote;
import ru.artezio.bank.entity.ApplicationTypeModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class ApplicationTypeDaoImpl extends GenericDaoImpl<Long, ApplicationTypeModel>
		implements ApplicationTypeDaoBeanRemote, ApplicationTypeDaoBeanLocal {

	@EJB private UserDaoBeanLocal userDao;

	@Override
	public List<ApplicationTypeModel> getAll() {
		return list();
	}

	@Override
	public ApplicationTypeModel getById(Long id) {
		return get(id);
	}

	@Override
	public ApplicationTypeModel update(ApplicationTypeModel model, Long updaterUserId) throws AccessViolationException {
		if (!userDao.isUserAdmin(updaterUserId)) {
			throw new AccessViolationException();
		}
		return update(model);
	}

}
