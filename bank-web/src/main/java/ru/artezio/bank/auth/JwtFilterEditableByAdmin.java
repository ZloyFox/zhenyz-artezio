package ru.artezio.bank.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.entity.dto.UserRoleDto;
import ru.artezio.bank.service.remote.JwtTokenServiceBeanRemote;
import ru.artezio.bank.service.remote.UserRoleServiceBeanRemote;
import ru.artezio.bank.util.HttpUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter user for entities which are readable by all users but editable only by admin
 */
@Component
public class JwtFilterEditableByAdmin implements Filter {

	@Autowired private JwtTokenServiceBeanRemote jwtTokenService;
	@Autowired private UserRoleServiceBeanRemote userRoleService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		// Proceed without role checks
		if (request.getMethod().equalsIgnoreCase("GET")) {
			chain.doFilter(servletRequest, servletResponse);
		} else {
			try {
				String authToken = HttpUtils.getAuthTokenFromServletRequest(request);
				UserInfo user = jwtTokenService.getUserFromToken(authToken);
				// Check admin role
				UserRoleDto role = userRoleService.getAdminRole();
				if (null != user && user.hasRole(role)) {
					logger.trace("Request is valid, continue filtering");
					chain.doFilter(servletRequest, servletResponse);
				} else {
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				}
			} catch (Exception ex) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			}
		}
	}

	@Override
	public void init(FilterConfig filterConfig) {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
				filterConfig.getServletContext());
	}

	@Override
	public void destroy() {
	}

}
