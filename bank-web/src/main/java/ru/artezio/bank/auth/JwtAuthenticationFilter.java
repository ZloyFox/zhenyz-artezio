package ru.artezio.bank.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.artezio.bank.service.remote.JwtTokenServiceBeanRemote;
import ru.artezio.bank.util.HttpUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationFilter implements Filter {

	@Autowired private JwtTokenServiceBeanRemote jwtTokenService;

	private final Logger log = LoggerFactory.getLogger(getClass());

	public JwtAuthenticationFilter() {
		log.info("JWT Authentication filter init");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String requestUri = request.getRequestURI();
		String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

		if (requestUri.endsWith("/login") || requestUri.endsWith("/register")) {
			chain.doFilter(request, response);
		} else {
			if ((null != authHeader) && (authHeader.startsWith("Bearer "))
					&& jwtTokenService.isTokenValid(HttpUtils.getAuthTokenFromServletRequest(request))) {
				log.trace("Request is valid, continue filtering");
				chain.doFilter(req, res);
			} else {
				log.info("Unauthenticated request {} with Auth header {}", requestUri, authHeader);
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			}
		}
	}

	@Override
	public void init(FilterConfig filterConfig) {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
				filterConfig.getServletContext());
	}

	@Override
	public void destroy() {
	}

}
