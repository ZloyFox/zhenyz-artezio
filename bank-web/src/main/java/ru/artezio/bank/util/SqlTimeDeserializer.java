package ru.artezio.bank.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.sql.Time;

/**
 * Deserialize time values formatted as 'HH:MM"
 */
public class SqlTimeDeserializer extends JsonDeserializer<Time> {

	@Override
	public Time deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException {
		return Time.valueOf(jp.getValueAsString() + ":00");
	}

}