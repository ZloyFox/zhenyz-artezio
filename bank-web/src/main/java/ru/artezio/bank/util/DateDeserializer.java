package ru.artezio.bank.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDeserializer extends StdDeserializer<Date> {

	private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
	private SimpleDateFormat isoFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public DateDeserializer() {
		this(null);
	}

	public DateDeserializer(Class<?> valueClass) {
		super(valueClass);
	}

	@Override
	public Date deserialize(JsonParser jsonparser, DeserializationContext context)
			throws IOException, JsonProcessingException {
		String date = jsonparser.getText();
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			try {
				return isoFormatter.parse(date);
			} catch (ParseException ex) {
				throw new RuntimeException(ex);
			}
		}
	}
}