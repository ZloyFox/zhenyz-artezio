package ru.artezio.bank.util;

/**
 *  List of additional HTTP headers for response
 */
public interface RestHttpHeaderNames {
	/**
	 * Used for REST service authorization
	 */
	public static final String AUTH_TOKEN = "auth_token";
}
