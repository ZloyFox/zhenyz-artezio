package ru.artezio.bank.util;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

public class HttpUtils {

	private static final String AUTH_HEADER_PREFIX = "Bearer ";

	public static String getAuthTokenFromServletRequest(HttpServletRequest request) {
		return request.getHeader(HttpHeaders.AUTHORIZATION).substring(AUTH_HEADER_PREFIX.length());
	}

}
