package ru.artezio.bank.util;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.UUID;

@Component
public class LogRequestFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * Appends UUID to request chain
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// Add UUID to logger
		ThreadContext.push(UUID.randomUUID().toString());
		// Process request
		chain.doFilter(request, response);
		// Remove UUID
		ThreadContext.pop();
	}

}