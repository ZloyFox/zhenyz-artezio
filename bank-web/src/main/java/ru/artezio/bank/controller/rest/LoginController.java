package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.LoginDto;
import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.InvalidCredentialsException;
import ru.artezio.bank.service.exception.UserBlockedException;
import ru.artezio.bank.service.remote.UserServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/*")
public class LoginController extends BaseController {

	@Autowired private UserServiceBeanRemote userService;

	private static final Gson gson = new GsonBuilder().create();

	@RequestMapping(path = "login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> processLogin(HttpServletRequest request, @RequestBody UserDto userDto) {
		ResponseEntity<?> response;
		try {
			LoginDto loginDto = tryLoginUser(userDto);
			response = buildSuccessResponse(gson.toJsonTree(loginDto));
		} catch (final InvalidCredentialsException ex) {
			// Incorrect username/password
			logger.debug("InvalidCredentialsException: {}", ex.getMessage());
			response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		} catch (final UserBlockedException ex) {
			// Account is blocked
			logger.debug("UserBlockedException: {}", ex.getMessage());
			response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		} catch (Exception ex) {
			logger.info("Exception: {}", ex.getMessage());
			ex.printStackTrace();
			response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return response;
	}

	// internal

	private LoginDto tryLoginUser(UserDto userDto) throws InvalidCredentialsException, UserBlockedException {
		// Validate credentials

		logger.warn("Login " + userDto);
		UserInfo user = userService.authenticate(userDto);

		// Generate auth_token
		String authToken = jwtTokenService.issueToken(user);
		logger.debug("Login user {} OK", user.getLogin());

		// Build response
		LoginDto loginDto = new LoginDto();
		loginDto.setUser(mapper.mapModel(user, UserDto.class));
		loginDto.setAuthToken(authToken);
		return loginDto;
	}

}
