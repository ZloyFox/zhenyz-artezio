package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.EncashmentTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.remote.EncashmentTypeServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/*")
public class EncashmentTypeController extends BaseController {

	@Autowired private EncashmentTypeServiceBeanRemote encashmentTypeService;

	private static final Gson gson = new GsonBuilder().create();
	private static final String API_URL = "/encashment_type";
	private static final String API_URL_ID = API_URL + "/{id}";

	@RequestMapping(path=API_URL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getEncashmentTypesList(HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			response = buildSuccessResponse(gson.toJsonTree(encashmentTypeService.getAll()));
		} catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getEncashmentType(@PathVariable("id") Long requestId, HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			response = buildSuccessResponse(gson.toJsonTree(encashmentTypeService.get(requestId)));
		} catch (Exception ex) {
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateEncashmentType(@PathVariable("id") Long requestId, HttpServletRequest request,
										@RequestBody EncashmentTypeDto dto) {
		ResponseEntity<?> response;
		try {
			Long userId = getUserIdFromRequest(request);
			logger.trace("Receive {}", dto);
			EncashmentTypeDto result = encashmentTypeService.update(dto, userId);
			response = buildSuccessResponse(gson.toJsonTree(result));
		} catch (AccessViolationException ex) {
			logger.trace("Access violation (action: update)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.trace("Exception {}", ex);
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

}
