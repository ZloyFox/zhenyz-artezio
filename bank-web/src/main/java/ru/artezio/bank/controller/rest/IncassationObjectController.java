package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.IncassationObjectDto;
import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.remote.IncassationObjectServiceBeanRemote;
import ru.artezio.bank.util.DateSerializer;
import ru.artezio.bank.util.HttpUtils;
import ru.artezio.bank.util.SqlTimeSerializer;

import javax.servlet.http.HttpServletRequest;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/v1/*")
public class IncassationObjectController extends BaseController {

	@Autowired private IncassationObjectServiceBeanRemote incassationObjectService;

	private static final String API_URL_ROOT = "/incassation_object";
	private static final String API_URL_ID = API_URL_ROOT + "/{id}";

	private static final Gson gson = new GsonBuilder()
			.registerTypeAdapter(Time.class, new SqlTimeSerializer())
			.registerTypeAdapter(Date.class, new DateSerializer())
			.create();

	@RequestMapping(path=API_URL_ROOT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getIncassationObjectsList(
				@RequestParam(value=REQUEST_PARAM_OFFSET, defaultValue = REQUEST_PARAM_OFFSET_DEAFULT, required = false) Integer offset,
				@RequestParam(value=REQUEST_PARAM_COUNT, defaultValue = REQUEST_PARAM_COUNT_DEFAULT, required = false) Integer count,
				HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			UserInfo user = getUserFromRequest(request);
			List<IncassationObjectDto> dtoList = incassationObjectService.findAllForUser(user, offset, count);
			response = buildSuccessResponse(gson.toJsonTree(dtoList));
		} catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getIncassationObject(@PathVariable("id") Long id, HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			IncassationObjectDto dto = incassationObjectService.get(id, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(dto));
		} catch (AccessViolationException ex) {
			String authToken = HttpUtils.getAuthTokenFromServletRequest(request);
			logger.info("Access violation (action: get)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateIncassationObject(@PathVariable("id") Long id, HttpServletRequest request,
													 @RequestBody IncassationObjectDto dto) {
		ResponseEntity<?> response;
		try {
			logger.trace("Receive {}", dto);
			IncassationObjectDto result = incassationObjectService.update(dto, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(result));
		}
		catch (AccessViolationException ex) {
			logger.info("Access violation (action: update)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.trace("Exception {}", ex);
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ROOT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(HttpServletRequest request, @RequestBody IncassationObjectDto dto) {
		ResponseEntity<?> response;
		try {
			// Set creator
			UserInfo user = getUserFromRequest(request);
			dto.setCreator(mapper.mapModel(user, UserDto.class));
			dto = incassationObjectService.add(dto);
			response = buildSuccessResponse(gson.toJsonTree(dto));
		} catch (Exception ex) {
			logger.info("Catched exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}


}
