package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.remote.UserServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1/*")
public class UserController extends BaseController {

	private static final String API_URL = "/user";

	private static final String API_URL_ID = API_URL + "/{id}";
	private static final Gson gson = new GsonBuilder().create();

	@Autowired private UserServiceBeanRemote userService;

	@RequestMapping(path=API_URL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getUsersList(
			@RequestParam(value=REQUEST_PARAM_OFFSET, defaultValue = REQUEST_PARAM_OFFSET_DEAFULT, required = false) Integer offset,
			@RequestParam(value=REQUEST_PARAM_COUNT, defaultValue = REQUEST_PARAM_COUNT_DEFAULT, required = false) Integer count,
			HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			List<UserInfo> resultList = userService.getAll(getUserIdFromRequest(request), offset, count);
			logger.info("GetAll done");
			response = buildSuccessResponse(gson.toJsonTree(resultList));
		}
		catch (AccessViolationException ex) {
			logger.info("Access violation (action: getAll)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getUser(@PathVariable("id") Long id, HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			UserInfo user = userService.getUserInfo(id, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(user));
		} catch (Exception ex) {
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateUser(@PathVariable("id") Long id, HttpServletRequest request,
										@RequestBody UserDto userDto) {
		ResponseEntity<?> response;
		try {
			logger.trace("Receive userDto = {}", userDto);
			UserInfo userInfo = userService.update(userDto, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(userInfo));
		}
		catch (AccessViolationException ex){
			logger.info("Access violation (action: update)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.trace("Exception {}", ex);
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

}
