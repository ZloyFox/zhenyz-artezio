package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.service.remote.UserRoleServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/*")
public class UserRoleController extends BaseController {

	private static final Gson gson = new GsonBuilder().create();

	@Autowired private UserRoleServiceBeanRemote userRoleService;

	@RequestMapping(path="/user_role", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getUserRolesList(HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			response = buildSuccessResponse(gson.toJsonTree(userRoleService.getAll()));
		} catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}
}
