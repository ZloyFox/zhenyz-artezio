package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.RegisterUserAccountException;
import ru.artezio.bank.service.remote.UserServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/*")
public class RegistrationController extends BaseController {

	private static final Gson gson = new GsonBuilder().create();

	@Autowired private UserServiceBeanRemote userService;

	@RequestMapping(path="/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> register(HttpServletRequest request, @RequestBody UserDto userDto) {
		ResponseEntity<?> response;
		try {
			UserInfo resultUser = userService.createUser(userDto);
			response = buildSuccessResponse(gson.toJsonTree(resultUser));
		} catch (RegisterUserAccountException ex) {
			logger.info("Catched RegisterUserAccount exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		} catch (Exception ex) {
			logger.info("Catched exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}
}
