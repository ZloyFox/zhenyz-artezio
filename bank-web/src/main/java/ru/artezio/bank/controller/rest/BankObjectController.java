package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.BankObjectDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.remote.BankObjectServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1/*")
public class BankObjectController extends BaseController {

	@Autowired private BankObjectServiceBeanRemote bankObjectService;

	private static Gson gson = new GsonBuilder().create();
	private static final String API_URL = "/bank_object";
	private static final String API_URL_ID = API_URL + "/{id}";
	private static final String REQUEST_PARAM_ADDRESS = "address";

	@RequestMapping(path=API_URL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getBankObjectsList(
			@RequestParam(value=REQUEST_PARAM_OFFSET, defaultValue = REQUEST_PARAM_OFFSET_DEAFULT, required = false) Integer offset,
			@RequestParam(value=REQUEST_PARAM_COUNT, defaultValue = REQUEST_PARAM_COUNT_DEFAULT, required = false) Integer count,
			@RequestParam(value=REQUEST_PARAM_ADDRESS, defaultValue = "", required = false) String address,
			HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			List<BankObjectDto> dtos;
			Long demanderId = getUserIdFromRequest(request);

			if (!address.isEmpty()) {
				dtos = bankObjectService.findByAddress(address, offset, count);
			} else {
				dtos = bankObjectService.getAll(offset, count, demanderId);
			}
			response = buildSuccessResponse(gson.toJsonTree(dtos));
		} catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getBankObject(@PathVariable("id") Long id, HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			response = buildSuccessResponse(gson.toJsonTree(bankObjectService.getById(id)));
		} catch (Exception ex) {
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateBankObject(@PathVariable("id") Long id, HttpServletRequest request,
											  @RequestBody BankObjectDto dto) {
		ResponseEntity<?> response;
		try {
			logger.trace("Receive {}", dto);
			BankObjectDto result = bankObjectService.update(dto, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(result));
		} catch (AccessViolationException ex) {
			logger.info("Access violation (action: update)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.trace("Exception {}", ex);
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> createBankObject(HttpServletRequest request, @RequestBody BankObjectDto dto) {
		ResponseEntity<?> response;
		try {
			logger.trace("Receive {}", dto);
			UserInfo creator = getUserFromRequest(request);
			dto.setCreator(creator);
			BankObjectDto result = bankObjectService.create(dto, creator.getId());
			response = buildSuccessResponse(gson.toJsonTree(result));
		} catch (AccessViolationException ex) {
			logger.info("Access violation (action: update)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.trace("Exception {}", ex);
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

}
