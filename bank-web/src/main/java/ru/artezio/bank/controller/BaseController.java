package ru.artezio.bank.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.remote.JwtTokenServiceBeanRemote;
import ru.artezio.bank.util.HttpUtils;
import ru.artezio.bank.util.Mapper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

public abstract class BaseController {

	protected static final String REQUEST_PARAM_OFFSET = "offset";
	protected static final String REQUEST_PARAM_OFFSET_DEAFULT = "0";
	protected static final String REQUEST_PARAM_COUNT = "count";
	protected static final String REQUEST_PARAM_COUNT_DEFAULT = "10";

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired protected Mapper mapper;
	@Autowired protected JwtTokenServiceBeanRemote jwtTokenService;

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseBody
	public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex)
	{
		final Throwable cause = ex.getCause();
		String message;
		if (cause == null) {
			message = "Could not process JSON input";
		} else {
			message = cause.getMessage();
		}

		logger.trace("Could not process JSON object; exception is " + message);
		return buildFailureResponse(message);
	}

	protected <T, S> S mapModelToDto(T model, Class<S> dtoClass) {
		return model == null ? null : mapper.mapModel(model, dtoClass);
	}

	protected <T, S> T mapDtoToModel(S dto, Class<T> modelClass) {
		return dto == null ? null : mapper.mapModel(dto, modelClass);
	}

	protected <T, S> List<S> mapModelListToDto(List<T> modelList, Class<S> dtoClass) {
		return mapper.mapList(modelList, dtoClass);
	}

	protected <T, S> Set<S> mapModelSetToDto(Set<T> modelSet, Class<S> dtoClass) {
		return mapper.mapSet(modelSet, dtoClass);
	}

	/**
	 * Compose response json with success result and specified body
	 * @return json: {'success': true, 'result': obj}
	 */
	protected ResponseEntity<?> buildSuccessResponse(JsonElement obj, HttpStatus status) {
		JsonObject body = new JsonObject();
		body.addProperty("success", true);
		body.add("result", obj);
		if (obj.isJsonArray()) {
			body.addProperty("count", obj.getAsJsonArray().size());
		}
		return ResponseEntity.status(status).body(body.toString());
	}

	protected ResponseEntity<?> buildSuccessResponse(JsonElement obj) {
		return buildSuccessResponse(obj, HttpStatus.OK);
	}

	/**
	 * Compose response json with fail result and specified body
	 * @return json: {'success': false, 'error': error}
	 */
	protected ResponseEntity<?> buildFailureResponse(String error, HttpStatus status) {
		JsonObject body = new JsonObject();
		body.addProperty("success", false);
		body.addProperty("error", error);
		return ResponseEntity.status(status).body(body.toString());
	}

	protected ResponseEntity<?> buildFailureResponse(String error) {
		return buildFailureResponse(error, HttpStatus.BAD_REQUEST);
	}

	protected Long getUserIdFromRequest(HttpServletRequest request) throws Exception {
		String headerAuth = HttpUtils.getAuthTokenFromServletRequest(request);
		return jwtTokenService.getUserIdFromToken(headerAuth);
	}

	protected UserInfo getUserFromRequest(HttpServletRequest request) throws Exception {
		String headerAuth = HttpUtils.getAuthTokenFromServletRequest(request);
		return jwtTokenService.getUserFromToken(headerAuth);
	}

}
