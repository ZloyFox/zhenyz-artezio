package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.ApplicationDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.NotFoundException;
import ru.artezio.bank.service.remote.ApplicationServiceBeanRemote;
import ru.artezio.bank.util.DatetimeSerializer;
import ru.artezio.bank.util.SqlTimeSerializer;

import javax.servlet.http.HttpServletRequest;
import java.sql.Time;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/*")
public class ApplicationController extends BaseController {

	@Autowired private ApplicationServiceBeanRemote applicationService;

	private static final String API_URL_ROOT = "/application";
	private static final String API_URL_ID = API_URL_ROOT + "/{id}";

	private static final Gson gson = new GsonBuilder()
			.registerTypeAdapter(Time.class, new SqlTimeSerializer())
			.registerTypeAdapter(Date.class, new DatetimeSerializer())
			.create();

	@RequestMapping(path=API_URL_ROOT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getApplicationsList(
				@RequestParam(value=REQUEST_PARAM_OFFSET, defaultValue = REQUEST_PARAM_OFFSET_DEAFULT, required = false) Integer offset,
				@RequestParam(value=REQUEST_PARAM_COUNT, defaultValue = REQUEST_PARAM_COUNT_DEFAULT, required = false) Integer count,
				HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			Long userId = getUserIdFromRequest(request);
			response = buildSuccessResponse(gson.toJsonTree(applicationService.findByCreatorId(userId, offset, count)));
		} catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getApplication(@PathVariable("id") Long id, HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			Long userId = getUserIdFromRequest(request);
			response = buildSuccessResponse(gson.toJsonTree(applicationService.get(id, userId)));
		} catch (NotFoundException ex) {
			response = buildFailureResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
		catch (Exception ex) {
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateApplication(@PathVariable("id") Long id, HttpServletRequest request,
													 @RequestBody ApplicationDto dto) {
		ResponseEntity<?> response;
		try {
			logger.trace("Receive {}", dto);
			applicationService.update(dto, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(dto));
		}
		catch (NotFoundException ex) {
			response = buildFailureResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
		catch (Exception ex) {
			logger.info("Exception {}", ex);
			response = buildFailureResponse("Server error");
		}
		return response;
	}

	@RequestMapping(path=API_URL_ROOT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(HttpServletRequest request, @RequestBody ApplicationDto dto) {
		ResponseEntity<?> response;
		try {
			// Set creator
			UserInfo user = getUserFromRequest(request);
			dto.setCreator(user);
			dto = applicationService.add(dto);
			response = buildSuccessResponse(gson.toJsonTree(dto));
		} catch (Exception ex) {
			logger.info("Catched exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

}
