package ru.artezio.bank.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = {"/angular/*", "/angular/**", "/@angular/**", "/"})
public class AngularController extends BaseController {

	@RequestMapping(path = {"*", "/"}, method = RequestMethod.GET)
	public String processAngularRequest(HttpServletRequest request) {
		return "/resources/angular/ang-app.html";
	}

}
