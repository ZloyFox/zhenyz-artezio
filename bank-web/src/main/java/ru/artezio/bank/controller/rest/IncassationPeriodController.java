package ru.artezio.bank.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.bank.controller.BaseController;
import ru.artezio.bank.entity.dto.IncassationPeriodDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.remote.IncassationPeriodServiceBeanRemote;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/*")
public class IncassationPeriodController extends BaseController {

	@Autowired private IncassationPeriodServiceBeanRemote incassationPeriodService;

	private static final String API_URL = "/incassation_period";
	private static final String API_URL_ID = API_URL + "/{id}";
	private static final Gson gson = new GsonBuilder().create();

	@RequestMapping(path=API_URL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getIncassationPeriodsList(HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			response = buildSuccessResponse(gson.toJsonTree(incassationPeriodService.getAll()));
		} catch (Exception ex) {
			logger.info("Catch exception: {}", ex.getMessage());
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getIncassationPeriod(@PathVariable("id") Long id, HttpServletRequest request) {
		ResponseEntity<?> response;
		try {
			response = buildSuccessResponse(gson.toJsonTree(incassationPeriodService.get(id)));
		} catch (Exception ex) {
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

	@RequestMapping(path=API_URL_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateIncassationPeriod(@PathVariable("id") Long id, HttpServletRequest request,
													 @RequestBody IncassationPeriodDto dto) {
		ResponseEntity<?> response;
		try {
			logger.trace("Receive {}", dto);
			IncassationPeriodDto result = incassationPeriodService.update(dto, getUserIdFromRequest(request));
			response = buildSuccessResponse(gson.toJsonTree(result));
		} catch (AccessViolationException ex) {
			logger.info("Access violation (action: update)");
			response = buildFailureResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		catch (Exception ex) {
			logger.trace("Exception {}", ex);
			response = buildFailureResponse(ex.getMessage());
		}
		return response;
	}

}
