import {Component} from "@angular/core";
import {AuthenticationService} from "./components/auth/auth.service";

@Component({
    selector: 'bankApp',
    templateUrl: '/resources/angular/app/app-template.html',
    styles: ['']
})

export class AppComponent {
    constructor(private auth: AuthenticationService) {
    }
}
