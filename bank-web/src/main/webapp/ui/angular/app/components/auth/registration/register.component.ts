import {Component, OnInit} from "@angular/core";
import {Credentials} from "../credentials";
import {RegistrationService} from "./registration.service";
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
    templateUrl: '/resources/angular/app/components/auth/registration/registration-template.html',
    styleUrls: ['/resources/css/login.css','/resources/css/main.css'],
})

export class RegisterComponent implements OnInit {

    private credentials : Credentials;
    private errorMessage: string;
    private regForm : FormGroup;
    private password2: FormControl;
    private serverError: string;
    private isSubmitting: boolean;


    constructor(private formBuilder: FormBuilder,private registrationService: RegistrationService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.buildForm();
    }

    buildForm(): void {
        this.isSubmitting = false;
        this.credentials = new Credentials();
        this.errorMessage = null;
        this.regForm = this.formBuilder.group({
            "login": [
                this.credentials.login,
                Validators.required
            ],
            "password": [
                this.credentials.password,
                Validators.required
            ],
            "password2": [
                this.password2,
                Validators.required
            ]
        }, {validator: this.matchingPasswords('password', 'password2')});
    }

    submitForm(value: any) {
        this.isSubmitting = true;
        let cred = new Credentials();
        cred.login = value.login;
        cred.password = value.password;
        this.errorMessage = null;
        this.registrationService.postData(cred)
            .subscribe(
                data => {
                    this.router.navigate(['/']);
                },
                error => {
                    this.errorMessage = error;
                    this.isSubmitting = false;
                }
            );
    }

    matchingPasswords(password: string, password2: string) {
        return (group: FormGroup) => {
            let passwordInput = group.controls[password];
            let passwordConfirmationInput = group.controls[password2];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({notEquivalent: true})
            }
        }
    }

    navigateLogin() {
        this.router.navigate(['/signin']);
    }
}
