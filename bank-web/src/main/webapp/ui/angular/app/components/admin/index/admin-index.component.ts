import {Component} from "@angular/core";
import {Router} from "@angular/router";
import "@angular/router";
import "rxjs/add/observable/throw";

@Component({
    templateUrl: '/resources/angular/app/components/admin/index/admin-index-template.html',
    styleUrls: ['/resources/css/main.css', '/resources/css/admin.css'],
})

export class AdminIndexComponent {

    private linkUsers = 'admin/users';

    constructor(private router: Router) {
    }

    private navigateUsers() : void {
        this.router.navigate([this.linkUsers]);
    }

}
