import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AdminIndexComponent} from "./index/admin-index.component";
import {AdminUsersComponent} from "./users/admin-users.component";
import {AdminRouteGuard} from "../auth/guard/admin-route.guard";
import {ProtectedRouteGuard} from "../auth/guard/protected-route.guard";
import {AdminComponent} from "./admin.component";
import {AdminEditUserComponent} from "./users/admin-edit-user.component";
import {BankObjectListComponent} from "./bank-objects/bank-object-list.component";
import {BankObjectComponent} from "./bank-objects/bank-object.component";

const adminRoutes : Routes = [

    { path: 'admin', component: AdminComponent, canActivate: [ProtectedRouteGuard, AdminRouteGuard],
        children: [
            { path: 'users', component: AdminUsersComponent },
            { path: 'edit-user/:id', component: AdminEditUserComponent },
            { path: 'bank-objects', component: BankObjectListComponent },
            { path: 'bank-objects/edit/:id', component: BankObjectComponent },
            { path: 'bank-objects/add', component: BankObjectComponent },
            { path: '', component: AdminIndexComponent },
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(adminRoutes) ],
    exports: [ RouterModule ]
})

export class AdminRoutingModule {}