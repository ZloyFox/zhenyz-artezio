import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {SettlementType} from "../data/settlement-type";

@Injectable()
export class SettlementTypeService {

    private apiUrl = "/api/v1/settlement_type";
    private bhSettlementTypes: ReplaySubject<SettlementType[]> = null;
    private bhSettlementTypesUpdater: ReplaySubject<SettlementType> = null;
    private storage : {
        settlementTypes: SettlementType[];
    };

    constructor(private http: HttpService) {
        this.storage = { settlementTypes: [] };
        this.bhSettlementTypes = new ReplaySubject(1);
        this.bhSettlementTypesUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getSettlementTypes() {
        return this.bhSettlementTypes;
    }

    getSettlementType(id: number) {
        return this.getSettlementTypes().map( lst => lst.filter(t => t.id == id) );
    }

/*    update(at: SettlementType) {
        let obs = this.http.put(`${this.apiUrl}/${user.id}`, JSON.stringify(user))
            .map(response => response.result.user);
        obs.subscribe(
            user => {
                let u: User = this.storage.users.find(iterUser => iterUser.id == user.id);
                u = user;
                this.bhSettlementTypesUpdater.next(u);
                this.updateStorage(this.storage.users);
            },
            error => {
                this.bhSettlementTypesUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhSettlementTypesUpdater.asObservable();
    }*/

    private updateStorage(data: any) {
        this.storage.settlementTypes = data;
        this.bhSettlementTypes.next(data);
    }

}