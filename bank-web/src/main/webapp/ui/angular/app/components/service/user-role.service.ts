import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {UserRole} from "../data/user.role";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class UserRoleService {

    private rolesGetUrl = "/api/v1/user_role";

    private bhRoles: ReplaySubject<UserRole[]> = null;

    private storage: {
        userRoles: UserRole[];
    };

    constructor(private http: HttpService) {
        this.storage = { userRoles: [] };
        this.bhRoles = new ReplaySubject(null);
        this.http.get(this.rolesGetUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    private updateStorage(data: any) {
        this.storage.userRoles = data;
        this.bhRoles.next(Object.assign({userRoles: []}, this.storage).userRoles);
    }

    getUserRoles() {
        return this.bhRoles;
    }

    getGuestRoles() {
        return this.getUserRoles().map( roles => roles.filter(role => role.guest));
    }

    getAdminRoles() {
        return this.getUserRoles().map( roles => roles.find(role => role.name == "Admin"));
    }

    private handleError(error: any) {
        console.error("Roles HandleError " + error);
    }

}
