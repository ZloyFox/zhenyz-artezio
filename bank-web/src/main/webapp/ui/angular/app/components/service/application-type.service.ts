import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {ApplicationType} from "../data/application-type";

@Injectable()
export class ApplicationTypeService {

    private apiUrl = "/api/v1/application_type";
    private bhApplicationTypes: ReplaySubject<ApplicationType[]> = null;
    private bhApplicationTypesUpdater: ReplaySubject<ApplicationType> = null;
    private storage : {
        applicationTypes: ApplicationType[];
    };

    constructor(private http: HttpService) {
        this.storage = { applicationTypes: [] };
        this.bhApplicationTypes = new ReplaySubject(1);
        this.bhApplicationTypesUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getApplicationTypes() {
        return this.bhApplicationTypes;
    }

    getApplicationType(id: number) {
        return this.getApplicationTypes().map( lst => lst.filter(t => t.id == id) );
    }

/*    update(at: AddressType) {
        let obs = this.http.put(`${this.apiUrl}/${user.id}`, JSON.stringify(user))
            .map(response => response.result);
        obs.subscribe(
            user => {
                let u: User = this.storage.users.find(iterUser => iterUser.id == user.id);
                u = user;
                this.bhAddressTypesUpdater.next(u);
                this.updateStorage(this.storage.users);
            },
            error => {
                this.bhAddressTypesUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhAddressTypesUpdater.asObservable();
    }*/

    private updateStorage(data: any) {
        this.storage.applicationTypes = data;
        this.bhApplicationTypes.next(data);
    }

}