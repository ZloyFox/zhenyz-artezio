import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/publishReplay";
import {BankObjectService} from "../../service/bank-object.service";
import {BankObject} from "../../data/bank-object";

@Component({
    templateUrl: '/resources/angular/app/components/admin/bank-objects/bank-object-list_template.html',
    styles: [
        ".table tbody tr.selected {background-color: rgba(80,36,255,0.18);}",
        ".table tbody tr { cursor: pointer; }",
    ],
})

export class BankObjectListComponent implements OnInit {

    private error: any = null;

    private obsBankObjects: Observable<BankObject[]> = null;

    private currentRow: number = -1;
    private currentBankObject: BankObject = null;
    private loaded: boolean;
    private noMoreItems: boolean;

    constructor(private router: Router, private bankObjectService: BankObjectService) {
    }

    ngOnInit(): void {
        this.loaded = false;
        this.noMoreItems = false;
        this.currentRow = -1;
        this.currentBankObject = null;
        this.loadData();
    }

    loadData(): void {
        this.obsBankObjects = this.bankObjectService.getBankObjects();
        this.obsBankObjects.subscribe( r => {
            this.noMoreItems = !this.bankObjectService.hasMoreItems();
            this.loaded = true;
        });
    }

    private fetchNext(): void {
        this.bankObjectService.fetch();
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private selectRow(row: number, obj: BankObject) {
        this.currentRow = row;
        this.currentBankObject = obj;
    }

    private editSelected() {
        this.router.navigate(['/admin/bank-objects/edit', this.currentBankObject.id]);
    }

    static pad(num: number, size: number): string {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    static formatId(id: number): string {
        let s = "2-";
        s = s + BankObjectListComponent.pad(id, 7);
        return s;
    }

}
