import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    templateUrl: '/resources/angular/app/components/session-expired-template.html',
    styleUrls: ['/resources/css/main.css']
})

export class SessionExpiredComponent {

    constructor(private router: Router) {
        setTimeout(() => {
            this.router.navigate(['/signin']);
        }, 2000);
    }

}
