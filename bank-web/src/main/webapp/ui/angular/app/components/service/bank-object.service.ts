import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {Observable} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {BankObject} from "../data/bank-object";

@Injectable()
export class BankObjectService {

    private apiUrl = "/api/v1/bank_object";
    private bhBankObjects: ReplaySubject<BankObject[]> = null;
    private bhBankObjectsUpdater: ReplaySubject<BankObject> = null;
    private storage : {
        bankObjects: BankObject[];
    };
    private offset: number = 0;
    private hasNoMore: boolean = false;
    public resultsCountPerFetch = 10;

    constructor(private http: HttpService) {
        this.storage = { bankObjects: [] };
        this.bhBankObjects = new ReplaySubject(1);
        this.bhBankObjectsUpdater = new ReplaySubject(1);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    fetch() {
        if (this.hasNoMore) {
            return;
        }
        let url = this.apiUrl + "?offset=" + this.offset + "&count=" + this.resultsCountPerFetch;
        this.http.get(url)
            .map( response => response.result )
            .subscribe(data => {
                if (data.length < this.resultsCountPerFetch) {
                    this.hasNoMore = true;
                }
                this.offset += data.length;
                this.updateStorage(data);
            });
    }

    getBankObjects() {
        return this.bhBankObjects;
    }

    getBankObject(id: number) {
        if (~this.storage.bankObjects.findIndex( x => x.id == id )) {
            return this.getBankObjects().map( lst => lst.filter(obj => obj.id == id) );
        } else {
            let obs1 = this.http.get(this.apiUrl + "/" + id).map( response => response.result );
            obs1.subscribe( x => {
                this.storage.bankObjects.push(x);
            });
            return obs1;
        }
    }

    hasMoreItems() {
        return !this.hasNoMore;
    }

    refetch() {
        this.storage = {bankObjects: []};
        this.updateStorage(this.storage.bankObjects);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    create(bankObject: BankObject) {
        let obs = this.http.post(this.apiUrl, JSON.stringify(bankObject))
            .map(response => response.result);
        obs.subscribe(
            result => {
                this.storage.bankObjects.push(result);
                this.bhBankObjectsUpdater.next(result);
                this.updateStorage(this.storage.bankObjects);
            },
            error => {
                this.bhBankObjectsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhBankObjectsUpdater;
    }

    update(bankObject: BankObject) {
        let obs = this.http.put(this.apiUrl + "/" + bankObject.id, JSON.stringify(bankObject))
            .map(response => response.result);
        obs.subscribe(
            result => {
                let index = this.storage.bankObjects.findIndex(iter => iter.id == result.id);
                if (~index) {
                    this.storage.bankObjects.splice(index, 1, result);
                    this.bhBankObjectsUpdater.next(result);
                } else {
                    this.storage.bankObjects.push(result);
                }
                this.updateStorage(this.storage.bankObjects);
            },
            error => {
                this.bhBankObjectsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhBankObjectsUpdater;
    }

    private updateStorage(data: any) {
        for (let i = 0; i < data.length; ++i) {
            let obj = data[i];
            let oldObjIndex = this.storage.bankObjects.findIndex(r=>r.id == obj.id);
            if (~oldObjIndex) {
                this.storage.bankObjects[oldObjIndex] = obj;
            } else {
                this.storage.bankObjects.push(obj);
            }
        }
        this.bhBankObjects.next(Object.assign([], this.storage.bankObjects));
    }

}