import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../../data/user";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {UserService} from "../../service/user.service";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/publishReplay";

@Component({
    templateUrl: '/resources/angular/app/components/admin/users/admin-users-template.html',
    styleUrls: ['/resources/css/main.css', '/resources/css/admin.css'],
})

export class AdminUsersComponent implements OnInit {

    private error: any = null;
    private userObs: Observable<User[]> = null;
    private listReady: boolean;
    private noMoreItems: boolean;

    constructor(private userService: UserService, private router: Router) {
    }

    ngOnInit() : void {
        this.listReady = false;
        this.noMoreItems = false;
        this.userObs = this.userService.getUsers();
        this.userObs.subscribe( r => {
            this.listReady = true;
            this.noMoreItems = !this.userService.hasMoreItems();
        });
    }

    private fetchNext(): void {
        this.userService.fetch();
    }

    private refetch(): void {
        this.listReady = false;
        this.noMoreItems = false;
        this.userService.refetch();
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private editUser(user: User) {
        this.router.navigate(['/admin/edit-user', user.id]);
    }

}
