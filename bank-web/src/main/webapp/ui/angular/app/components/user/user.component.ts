import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    templateUrl: '/resources/angular/app/components/user/user-template.html',
    styles: [
        "",
    ],
})

export class UserComponent {

    constructor(private router: Router) {
    }

}
