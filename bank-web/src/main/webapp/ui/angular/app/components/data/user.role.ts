export class UserRole {
    id: number;
    name: string;
    guest: boolean;
}