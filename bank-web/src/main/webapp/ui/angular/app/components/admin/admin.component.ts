import {Component} from "@angular/core";
import "@angular/router";

@Component({
    templateUrl: '/resources/angular/app/components/admin/admin-template.html',
    styleUrls: ['/resources/css/main.css', '/resources/css/admin.css'],
})

export class AdminComponent {

}
