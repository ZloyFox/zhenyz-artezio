import {AbstractControl, ValidatorFn} from "@angular/forms";

export function futureDateValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const date = control.value;
        let result: boolean = true;
        try {
            const no = Date.parse(date);
            if (typeof no === 'undefined' || no == null)
                    result = false;
        } catch (e) {
            result = false;
        }
        return result ? null : {'dateLessThanNow': {date}};
    };
}

