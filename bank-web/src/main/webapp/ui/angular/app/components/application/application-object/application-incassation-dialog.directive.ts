import {ViewContainerRef, Directive, ComponentRef} from "@angular/core";
import {IncassationObject} from "../../data/incassation-object";
import {IncassationSelectDialogComponent} from "./application-incassation-dialog.component";

@Directive({ selector: '[applicationAddIncassDialog]' })
export class ApplicationAddIncassationDialog {

    constructor(private viewContainer: ViewContainerRef, excluded?: IncassationObject[]) {

    }

    createDialog(dialogComponent: { new(): IncassationSelectDialogComponent }): ComponentRef<IncassationSelectDialogComponent> {
        // this.viewContainer.clear();
        //
        // let dialogComponentFactory =
        //     this.componentFactoryResolver.resolveComponentFactory(dialogComponent);
        // let dialogComponentRef = this.viewContainer.createComponent(dialogComponentFactory);
        //
        // dialogComponentRef.instance.close.subscribe(() => {
        //     dialogComponentRef.destroy();
        // });
        //
        // return dialogComponentRef;
        return null;
    }
}