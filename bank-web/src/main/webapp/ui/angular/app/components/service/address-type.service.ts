import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {AddressType} from "../data/address-type";

@Injectable()
export class AddressTypeService {

    private apiUrl = "/api/v1/address_type";
    private bhAddressTypes: ReplaySubject<AddressType[]> = null;
    private bhAddressTypesUpdater: ReplaySubject<AddressType> = null;
    private storage : {
        addressTypes: AddressType[];
    };

    constructor(private http: HttpService) {
        this.storage = { addressTypes: [] };
        this.bhAddressTypes = new ReplaySubject(1);
        this.bhAddressTypesUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getAddressTypes() {
        return this.bhAddressTypes;
    }

    getAddressType(id: number) {
        return this.getAddressTypes().map( lst => lst.filter(t => t.id == id) );
    }

/*    update(at: AddressType) {
        let obs = this.http.put(`${this.apiUrl}/${user.id}`, JSON.stringify(user))
            .map(response => response.result);
        obs.subscribe(
            user => {
                let u: User = this.storage.users.find(iterUser => iterUser.id == user.id);
                u = user;
                this.bhAddressTypesUpdater.next(u);
                this.updateStorage(this.storage.users);
            },
            error => {
                this.bhAddressTypesUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhAddressTypesUpdater.asObservable();
    }*/

    private updateStorage(data: any) {
        this.storage.addressTypes = data;
        this.bhAddressTypes.next(data);
    }

}