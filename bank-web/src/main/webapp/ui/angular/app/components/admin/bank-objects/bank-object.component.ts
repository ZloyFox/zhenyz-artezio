import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/publishReplay";
import {BankObject} from "../../data/bank-object";
import {BankObjectService} from "../../service/bank-object.service";
import {Location} from "@angular/common";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

@Component({
    templateUrl: '/resources/angular/app/components/admin/bank-objects/bank-object_template.html',
    styles: [
        ".req:after{ color: #af2600; content: '*'; padding-left: 4px; }",
        ".container { padding-bottom: 200px; }",
        ".alert { padding: 8px; margin-bottom: 4px;}",
        ".sep { text-align: left; border-top: 1px solid #dadada; padding: 4px 17px 0; margin-top: 15px;}",
        ".buttons-default { padding-left: 8px; margin-top: 16px }",
    ],
    selector: 'bank-object-form',
})

export class BankObjectComponent implements OnInit {

    private error: any = null;
    private bankObject: BankObject = null;
    private originalBankObject: BankObject = null;

    private obsBankObject: Observable<BankObject[]> = null;

    private formSubmitting: boolean = false;
    private readinessLevel: number = 0;
    private commonErrorString: string;

    private form: FormGroup = null;

    private bNew: boolean;
    private bSubmitting: boolean;

    constructor(private router: Router,
                private bankObjectService: BankObjectService,
                private location: Location,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.readinessLevel = 0;
        this.commonErrorString = null;
        this.loadFormData();
    }

    buildForm(): void {
        this.form = this.formBuilder.group({
                'name': [
                    this.bankObject.name,
                    [
                        Validators.required
                    ]
                ],
                'address': [
                    this.bankObject.address,
                    [
                        Validators.required
                    ]
                ],
            }
        );
        this.form.valueChanges
            .subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // (re)set validation messages now

        this.bSubmitting = false;
    }

    onValueChanged(data?: any, forceErrors?: boolean) {
        if (!this.form) { return; }
        this.commonErrorString = null;
        const form = this.form;
        this.bankObject = form.value;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && ((forceErrors && forceErrors == true && !control.valid) ||
                (control.touched && control.dirty && !control.valid))) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    console.log("Push err " + field + ":" + key);
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private loadFormData(): void {
        let id = +this.route.snapshot.params['id'];
        if (typeof id !== 'undefined' && id != null && !isNaN(id)) {
            this.obsBankObject = this.bankObjectService.getBankObject(id);
            this.bNew = false;
            this.obsBankObject.subscribe( objects => {
                if (typeof objects[0] !== 'undefined') {
                    this.bankObject = objects[0];

                    this.bankObject.id = objects[0].id;
                    this.originalBankObject = JSON.parse(JSON.stringify(this.bankObject));

                    this.buildForm();
                }
            } );
        } else {
            this.bNew = true;
            this.bankObject = new BankObject();
            this.buildForm();
        }
    }

    private submit() {
        // Make sure all errors will appear (if any)
        this.onValueChanged(null, true);

        // Show error or submit
        if (!this.form.valid) {
            this.commonErrorString = "Форма заполнена неверно";
        } else {
            this.commonErrorString = null;
            this.bSubmitting = true;
            if (this.bNew) {
                this.bankObjectService.create(this.bankObject).subscribe(
                    data => {
                        this.router.navigate(['admin/bank-objects']);
                    },
                    error => {
                        this.error = error;
                        this.bSubmitting = false;
                    }
                );
            } else {
                this.bankObject.id = this.originalBankObject.id;
                this.bankObjectService.update(this.bankObject).subscribe(
                    data => {
                        this.bankObject = data;
                        this.router.navigate(['admin/bank-objects']);
                    },
                    error => {
                        this.error = error;
                        this.bSubmitting = false;
                    }
                );
            }
        }
    }

    private cancel() {
        if (this.originalBankObject && typeof this.originalBankObject.id !== 'undefined') {
            this.bankObject = this.originalBankObject;
        }
        this.router.navigate(['admin/bank-objects']);
    }

    formErrors = {
        'address': '',
        'name': '',
    };
    validationMessages = {
        'address': {
            'required' : 'Заполните поле'
        },
        'name': {
            'required': 'Заполните поле'
        },
    };

}
