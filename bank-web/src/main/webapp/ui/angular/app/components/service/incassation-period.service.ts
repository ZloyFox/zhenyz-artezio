import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {Observable} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {IncassationPeriod} from "../data/incassation-period";

@Injectable()
export class IncassationPeriodService {

    private apiUrl = "/api/v1/incassation_period";
    private bhIncassationPeriods: ReplaySubject<IncassationPeriod[]> = null;
    private bhIncassationPeriodsUpdater: ReplaySubject<IncassationPeriod> = null;
    private storage : {
        incassationPeriods: IncassationPeriod[];
    };

    constructor(private http: HttpService) {
        this.storage = { incassationPeriods: [] };
        this.bhIncassationPeriods = new ReplaySubject(1);
        this.bhIncassationPeriodsUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getIncassationPeriods() {
        return this.bhIncassationPeriods;
    }

    getIncassationPeriod(id: number) {
        return this.getIncassationPeriods().map( lst => lst.filter(t => t.id == id) );
    }

    create(incassationPeriod: IncassationPeriod) {
        let obs = this.http.post(this.apiUrl, JSON.stringify(incassationPeriod))
            .map(response => response.result);
        obs.subscribe(
            result => {
                this.storage.incassationPeriods.push(result);
                this.bhIncassationPeriodsUpdater.next(result);
                this.updateStorage(this.storage.incassationPeriods);
            },
            error => {
                this.bhIncassationPeriodsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhIncassationPeriodsUpdater.asObservable();
    }

    update(incassationPeriod: IncassationPeriod) {
        let obs = this.http.put(`${this.apiUrl}/${incassationPeriod.id}`, JSON.stringify(incassationPeriod))
            .map(response => response.result);
        obs.subscribe(
            result => {
                let i: IncassationPeriod = this.storage.incassationPeriods.find(iter => iter.id == incassationPeriod.id);
                i = result;
                this.bhIncassationPeriodsUpdater.next(i);
                this.updateStorage(this.storage.incassationPeriods);
            },
            error => {
                this.bhIncassationPeriodsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhIncassationPeriodsUpdater.asObservable();
    }

    private updateStorage(data: any) {
        this.storage.incassationPeriods = data;
        this.bhIncassationPeriods.next(data);
    }

}