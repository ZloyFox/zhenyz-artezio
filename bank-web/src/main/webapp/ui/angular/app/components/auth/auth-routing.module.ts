import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./registration/register.component";
import {LogoutComponent} from "./logout/logout.component";

const authRoutes : Routes = [
    { path: 'signin', component: LoginComponent },
    { path: 'signup', component: RegisterComponent },
    { path: 'logout', component: LogoutComponent },
];

@NgModule({
    imports: [ RouterModule.forChild(authRoutes) ],
    exports: [ RouterModule ]
})

export class AuthRoutingModule {}