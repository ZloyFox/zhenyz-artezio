import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {IncassationObjectComponent} from "./incassation-object/incassation-object.component";
import {IncassationRoutingModule} from "./incassation-routing.module";
import {IncassationComponent} from "./incassation.component";
import {IncassationObjectListComponent} from "./incassation-object/incassation-object-list.component";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IncassationRoutingModule,
    ],
    declarations:   [
        IncassationComponent,
        IncassationObjectComponent,
        IncassationObjectListComponent,
    ],
    exports: [
    ]
})

export class IncassationModule { }