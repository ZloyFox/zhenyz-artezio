import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {Observable} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {IncassationObject} from "../data/incassation-object";

@Injectable()
export class IncassationObjectService {

    private apiUrl = "/api/v1/incassation_object";
    private bhIncassationObjects: ReplaySubject<IncassationObject[]> = null;
    private bhIncassationObjectsUpdater: ReplaySubject<IncassationObject> = null;
    private storage : {
        incassationObjects: IncassationObject[];
    };
    private offset: number = 0;
    private hasNoMore: boolean = false;
    public resultsCountPerFetch = 10;

    constructor(private http: HttpService) {
        this.storage = { incassationObjects: [] };
        this.bhIncassationObjects = new ReplaySubject(1);
        this.bhIncassationObjectsUpdater = new ReplaySubject(1);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    fetch() {
        if (this.hasNoMore) {
            return;
        }
        let url = this.apiUrl + "?offset=" + this.offset + "&count=" + this.resultsCountPerFetch;
        this.http.get(url)
            .map( response => response.result )
            .subscribe(data => {
                if (data.length < this.resultsCountPerFetch) {
                    this.hasNoMore = true;
                }
                this.offset += data.length;
                this.updateStorage(data);
            });
    }

    getIncassationObjects() {
        return this.bhIncassationObjects;
    }

    getIncassationObject(id: number) {
        if (~this.storage.incassationObjects.findIndex( x => x.id == id )) {
            return this.getIncassationObjects().map( lst => lst.filter(obj => obj.id == id) );
        } else {
            let obs1 = this.http.get(this.apiUrl + "/" + id).map( response => response.result );
            obs1.subscribe( x => {
                this.storage.incassationObjects.push(x);
            });
            return obs1;
        }
    }

    hasMoreItems() {
        return !this.hasNoMore;
    }

    refetch() {
        this.storage = {incassationObjects: []};
        this.updateStorage(this.storage.incassationObjects);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    create(incassationObject: IncassationObject) {
        let obs = this.http.post(this.apiUrl, JSON.stringify(incassationObject, IncassationObjectService.jsonPrepare))
            .map(response => response.result);
        obs.subscribe(
            result => {
                this.storage.incassationObjects.push(result);
                this.bhIncassationObjectsUpdater.next(result);
                this.updateStorage(this.storage.incassationObjects);
            },
            error => {
                this.bhIncassationObjectsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhIncassationObjectsUpdater;
    }

    static jsonPrepare(key: string, value: any) {
        if (value == null)
            return undefined;
        if (typeof value === 'string') {
            if (value.length == 0)
                return undefined;
        }
        if ((key == 'addressType' || key == 'settlementType') && !value.hasOwnProperty("id"))
            return undefined;
        return value;
    }

    update(incassationObject: IncassationObject) {
        let obs = this.http.put(this.apiUrl + "/" + incassationObject.id, JSON.stringify(incassationObject, IncassationObjectService.jsonPrepare))
            .map(response => response.result);
        obs.subscribe(
            result => {
                let index = this.storage.incassationObjects.findIndex(iter => iter.id == result.id);
                if (~index) {
                    this.storage.incassationObjects.splice(index, 1, result);
                    this.bhIncassationObjectsUpdater.next(result);
                } else {
                    this.storage.incassationObjects.push(result);
                }
                this.updateStorage(this.storage.incassationObjects);
            },
            error => {
                this.bhIncassationObjectsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhIncassationObjectsUpdater;
    }

    private updateStorage(data: any) {
        for (let i = 0; i < data.length; ++i) {
            let obj = data[i];
            let oldObjIndex = this.storage.incassationObjects.findIndex(r=>r.id == obj.id);
            if (~oldObjIndex) {
                this.storage.incassationObjects[oldObjIndex] = obj;
            } else {
                this.storage.incassationObjects.push(obj);
            }
        }
        this.bhIncassationObjects.next(Object.assign([], this.storage.incassationObjects));
    }

}