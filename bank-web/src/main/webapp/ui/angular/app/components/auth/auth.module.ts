import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RegisterComponent} from "./registration/register.component";
import {RegistrationService} from "./registration/registration.service";
import {LoginComponent} from "./login/login.component";
import {LoginService} from "./login/login.service";
import {LogoutComponent} from "./logout/logout.component";
import {AuthenticationService} from "./auth.service";
import {AuthRoutingModule} from "./auth-routing.module";
import {AdminRouteGuard} from "./guard/admin-route.guard";
import {ProtectedRouteGuard} from "./guard/protected-route.guard";
// import {HttpService} from "../http/http.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
        LogoutComponent,
    ],
    providers: [
        LoginService,
        RegistrationService,
        AuthenticationService,
        AdminRouteGuard,
        ProtectedRouteGuard,
    ],
    exports: [
        AuthRoutingModule
    ]
})

export class AuthModule { }