import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {AuthenticationService} from "../auth/auth.service";

@Injectable()
export class HttpService {

    constructor(private http: Http, private auth: AuthenticationService) {
    }

    post(url: string, data: string): Observable<any> {
        let headers: any = null;
        // Get authentication headers
        try {
            headers = this.buildHeaders();
            let options = new RequestOptions({ headers: headers });
            return this.http.post(url, data, options)
                .map( response => response.json() )
                .catch( (error: any) => Observable.throw(error.json() || "Server error") );
        } catch (e) {
            return Observable.throw(e);
        }
    }

    get(url: string): Observable<any> {
        let headers: any = null;
        // Get authentication headers
        try {
            headers = this.buildHeaders();
            let options = new RequestOptions({ headers: headers });
            return this.http.get(url, options)
                .map( response => response.json() )
                .catch( (error: any) => Observable.throw(error.json() || "Server error") );
        } catch (e) {
            return Observable.throw(e);
        }
    }

    put(url: string, data: string): Observable<any> {
        let headers: any = null;
        // Get authentication headers
        try {
            headers = this.buildHeaders();
            let options = new RequestOptions({ headers: headers });
            return this.http.put(url, data, options)
                .map( response => response.json() )
                .catch( (error: any) => Observable.throw(error.json() || "Server error") );
        } catch (e) {
            return Observable.throw(e);
        }
    }

    private buildHeaders(): Headers {
        // Check authorization
        if (!this.auth.isLoggedIn()) {
            throw "Authorization error";
        }
        return new Headers( {
            'Content-Type'  : 'application/json',
            'Authorization' : 'Bearer '+this.auth.getAuthToken()
        });
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return errMsg;
    }
}
