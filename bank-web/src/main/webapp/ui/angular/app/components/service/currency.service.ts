import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {Currency} from "../data/currency";

@Injectable()
export class CurrencyService {

    private apiUrl = "/api/v1/currency";
    private bhCurrencies: ReplaySubject<Currency[]> = null;
    private bhCurrencyUpdater: ReplaySubject<Currency> = null;
    private storage : {
        currencies: Currency[];
    };

    constructor(private http: HttpService) {
        this.storage = { currencies: [] };
        this.bhCurrencies = new ReplaySubject(1);
        this.bhCurrencyUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getCurrencies() {
        return this.bhCurrencies;
    }

    getCurrency(id: number) {
        return this.getCurrencies().map( lst => lst.filter(c => c.id == id) );
    }

    private updateStorage(data: any) {
        this.storage.currencies = data;
        this.bhCurrencies.next(data);
    }

}