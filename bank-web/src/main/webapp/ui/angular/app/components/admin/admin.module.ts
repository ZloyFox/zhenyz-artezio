import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AdminUsersComponent} from "./users/admin-users.component";
import {AdminIndexComponent} from "./index/admin-index.component";
import {CommonModule} from "@angular/common";
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminComponent} from "./admin.component";
import {AdminEditUserComponent} from "./users/admin-edit-user.component";
import {BankObjectComponent} from "./bank-objects/bank-object.component";
import {BankObjectListComponent} from "./bank-objects/bank-object-list.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AdminRoutingModule,
    ],
    declarations:   [
        AdminIndexComponent,
        AdminUsersComponent,
        AdminEditUserComponent,
        AdminComponent,
        BankObjectComponent,
        BankObjectListComponent
    ],
    exports: [
    ]
})

export class AdminModule { }