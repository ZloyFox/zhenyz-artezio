import {NgModule} from "@angular/core";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {ApplicationRoutingModule} from "./application-routing.module";
import {ApplicationMainComponent} from "./application-main.component";
import {ApplicationComponent} from "./application-object/application.component";
import {ApplicationListComponent} from "./application-object/application-list.component";
import {BrowserModule} from "@angular/platform-browser";
import {DateValueAccessor} from "../shared/date-value-accessor";
import {ApplicationIncassationListComponent} from "./application-object/application-incassation-list.component";
import {ApplicationAddIncassationDialog} from "./application-object/application-incassation-dialog.directive";

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        ApplicationRoutingModule,
    ],
    declarations:   [
        ApplicationMainComponent,
        ApplicationComponent,
        ApplicationListComponent,
        ApplicationIncassationListComponent,
        ApplicationAddIncassationDialog,
        DateValueAccessor
    ],
    exports: [
    ]
})

export class ApplicationModule { }