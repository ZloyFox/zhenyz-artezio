import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {Observable} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {IncassationType} from "../data/incassation-type";

@Injectable()
export class IncassationTypeService {

    private apiUrl = "/api/v1/incassation_type";
    private bhIncassationTypes: ReplaySubject<IncassationType[]> = null;
    private bhIncassationTypesUpdater: ReplaySubject<IncassationType> = null;
    private storage : {
        incassationTypes: IncassationType[];
    };

    constructor(private http: HttpService) {
        this.storage = { incassationTypes: [] };
        this.bhIncassationTypes = new ReplaySubject(1);
        this.bhIncassationTypesUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getIncassationTypes() {
        return this.bhIncassationTypes;
    }

    getIncassationType(id: number) {
        return this.getIncassationTypes().map( lst => lst.filter(t => t.id == id) );
    }

    create(incassationType: IncassationType) {
        let obs = this.http.post(this.apiUrl, JSON.stringify(incassationType))
            .map(response => response.result);
        obs.subscribe(
            result => {
                this.storage.incassationTypes.push(result);
                this.bhIncassationTypesUpdater.next(result);
                this.updateStorage(this.storage.incassationTypes);
            },
            error => {
                this.bhIncassationTypesUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhIncassationTypesUpdater.asObservable();
    }

    update(incassationType: IncassationType) {
        let obs = this.http.put(`${this.apiUrl}/${incassationType.id}`, JSON.stringify(incassationType))
            .map(response => response.result);
        obs.subscribe(
            result => {
                let i: IncassationType = this.storage.incassationTypes.find(iter => iter.id == incassationType.id);
                i = result;
                this.bhIncassationTypesUpdater.next(i);
                this.updateStorage(this.storage.incassationTypes);
            },
            error => {
                this.bhIncassationTypesUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhIncassationTypesUpdater.asObservable();
    }

    private updateStorage(data: any) {
        this.storage.incassationTypes = data;
        this.bhIncassationTypes.next(data);
    }

}