import {Component} from "@angular/core";
import {AuthenticationService} from "./auth/auth.service";
import {Router} from "@angular/router";
import {User} from "./data/user";

@Component({
    templateUrl: '/resources/angular/app/components/index-template.html',
    styleUrls: ['/resources/css/main.css']
})

export class IndexComponent {

    username: string = null;
    loggedIn: boolean;
    user: User = null;

    constructor(private auth: AuthenticationService, private router: Router) {
        this.loggedIn = auth.isLoggedIn();
        if (this.loggedIn) {
            this.username = auth.login;
            this.user = auth.user;
        } else {
            this.router.navigate(['/signin']);
        }
    }

    private isAdmin() {
        return this.auth.isAdmin();
    }

    private isUser() {
        return this.auth.isUser();
    }

}
