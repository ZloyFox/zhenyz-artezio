import {User} from "./user";
import {BankObject} from "./bank-object";
import {ApplicationType} from "./application-type";
import {IncassationObject} from "./incassation-object";

export class Application {
    id: number;
    creator: User;
    bankObject: BankObject;
    applicationType: ApplicationType;
    date: Date;
    inn: string;
    kpp: string;
    organizationName: string;
    ogrn: string;
    contactName: string;
    contactPhone: string;
    clientAccount: string;
    bankCode: string;
    bankAccountNumber: string;
    bankName: string;
    bankSwift: string;
    bankOtherInfo: string;
    incassationObjects: IncassationObject[];

    constructor() {
        this.creator = new User();
        this.id = 0;
        this.bankObject = new BankObject();
        this.applicationType = new ApplicationType();
        this.date = new Date();
        this.inn = "";
        this.kpp = "";
        this.organizationName = "";
        this.ogrn = "";
        this.contactName = "";
        this.contactPhone = "";
        this.clientAccount = "";
        this.bankCode = "";
        this.bankAccountNumber = "";
        this.bankSwift = "";
        this.bankName = "";
        this.bankOtherInfo = "";
        this.incassationObjects = [];
    }
}
