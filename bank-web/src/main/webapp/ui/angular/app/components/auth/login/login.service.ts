import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import {Credentials} from "../credentials";
import {AuthenticationService} from "../auth.service";

@Injectable()
export class LoginService {

    private loginUrl = '/api/v1/login';

    constructor(private http: Http, private auth:AuthenticationService) {
    }

    postLogin(cred: Credentials): Observable<string> {
        let headers = new Headers({ 'Content-Type': 'application/json' });

        let options = new RequestOptions({ headers: headers });
        let body = { login: cred.login, password: cred.password };
        return this.http.post(this.loginUrl, body, options)
            .map(this.extractData.bind(this))
            .catch(this.handleError.bind(this));
    }

    public authenticate(data: any) {
        // this.auth.authenticate(data);
    }

    private extractData(res: Response) {
        let body = res.json();
        this.auth.authenticate(body.result);
        return body;
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
