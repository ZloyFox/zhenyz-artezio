import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/publishReplay";
import {IncassationObject} from "../../data/incassation-object";
import {IncassationType} from "../../data/incassation-type";
import {IncassationObjectService} from "../../service/incassation-object.service";
import {IncassationTypeService} from "../../service/incassation-type.service";

@Component({
    templateUrl: '/resources/angular/app/components/incassation/incassation-object/incassation-object-list-template.html',
    styles: [
        ".table tbody tr.selected {background-color: rgba(80,36,255,0.18);}",
        ".table tbody tr { cursor: pointer; }",
    ],
})

export class IncassationObjectListComponent implements OnInit {

    private error: any = null;

    private obsIncassationObjects: Observable<IncassationObject[]> = null;
    private obsIncassationTypes: Observable<IncassationType[]>;

    private currentRow: number = -1;
    private currentIncassationObject: IncassationObject = null;
    private loaded: boolean;
    private noMoreItems: boolean;

    private days = [
        {id: 0, text: 'Понедельник'},
        {id: 1, text: 'Вторник'},
        {id: 2, text: 'Среда'},
        {id: 3, text: 'Четверг'},
        {id: 4, text: 'Пятница'},
        {id: 5, text: 'Суббота'},
        {id: 6, text: 'Воскресенье'},
    ];

    constructor(private router: Router, private incassationObjectService: IncassationObjectService,
                private incassationTypeService: IncassationTypeService) {
    }

    ngOnInit(): void {
        this.loaded = false;
        this.noMoreItems = false;
        this.currentRow = -1;
        this.currentIncassationObject = null;
        this.loadData();
    }

    loadData(): void {
        this.obsIncassationTypes = this.incassationTypeService.getIncassationTypes();
        this.obsIncassationTypes.subscribe( data => {
            this.obsIncassationObjects = this.incassationObjectService.getIncassationObjects();
            this.obsIncassationObjects.subscribe( r => {
                this.noMoreItems = !this.incassationObjectService.hasMoreItems();
                this.loaded = true;
            });
        });
    }

    private fetchNext(): void {
        this.incassationObjectService.fetch();
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private selectRow(row: number, incass: IncassationObject) {
        this.currentRow = row;
        this.currentIncassationObject = incass;
    }

    private editSelected() {
        this.router.navigate(['/incassation_object/edit', this.currentIncassationObject.id]);
    }

    static pad(num: number, size: number): string {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    static formatId(id: number): string {
        let s = "2-";
        s = s + IncassationObjectListComponent.pad(id, 7);
        return s;
    }

}
