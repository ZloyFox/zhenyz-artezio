import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ProtectedRouteGuard} from "../auth/guard/protected-route.guard";
import {ApplicationMainComponent} from "./application-main.component";
import {ApplicationListComponent} from "./application-object/application-list.component";
import {ApplicationComponent} from "./application-object/application.component";

const applicationRoutes : Routes = [

    { path: 'application', component: ApplicationMainComponent, canActivate: [ProtectedRouteGuard],
        children: [
            { path: '', component: ApplicationListComponent, pathMatch: 'full' },
            { path: 'add', component: ApplicationComponent },
            { path: 'edit/:id', component: ApplicationComponent },
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(applicationRoutes) ],
    exports: [ RouterModule ]
})

export class ApplicationRoutingModule {}