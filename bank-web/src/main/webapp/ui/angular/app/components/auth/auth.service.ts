import {Injectable} from "@angular/core";
import {User} from "../data/user";
import {UserRole} from "../data/user.role";
import {Router} from "@angular/router";

@Injectable()
export class AuthenticationService {
    public token: string = null;
    public tokenObject: any = null;
    public roles: UserRole[] = null;
    public login: string;
    public user: User;

    constructor(private router: Router) {
        if (!!localStorage.getItem("auth_token")) {
            this.token = localStorage.getItem("auth_token");
            let base64 = encodeURIComponent(this.token.split('.')[1]);
            let token = JSON.parse(window.atob(base64));
            this.tokenObject = token;
            this.roles = token.roles;
            this.login = token.username;
            this.checkSessionExpiredAndNavigate();
        }
    }

    public isSessionExpired(): boolean {
        // Divide by 1k to convert millis -> seconds
        return (this.tokenObject == null) || (this.tokenObject.exp < Math.round(+new Date()/1000));
    }

    public checkSessionExpiredAndNavigate(): boolean {
        let exp = this.isSessionExpired();
        if (exp) {
            this.logout();
            this.router.navigate(['/session_expired']);
        }
        return exp;
    }

    public authenticate(body: any) : void {
        let token = body.authToken || {};
        this.user = body.user || new User();
        let login = this.user.login || "";
        let roles = this.user.userRoles || [];

        if (!!this.token || !!localStorage.getItem("auth_token")) {
            this.logout();
        }
        this.token = token;
        let base64 = encodeURIComponent(this.token.split('.')[1]);
        this.tokenObject = JSON.parse(window.atob(base64));
        this.login = login;
        this.roles = roles;
        localStorage.setItem("auth_token", this.token);
    }

    public isLoggedIn() : boolean {
        return !!this.token;
    }

    public isAdmin(): boolean {
        return (-1 != this.roles.findIndex( role => {return role.name.localeCompare('Admin') == 0;}));
    }

    public isUser(): boolean {
        return (-1 != this.roles.findIndex( role => {return role.name.localeCompare('User') == 0;}));
    }

    public getAuthToken() : string {
        return this.token;
    }

    public logout(): void {
        // Just erase auth_token, as it's REST stateless architecture
        localStorage.removeItem("auth_token");
        this.token = null;
        this.login = null;
        this.roles = null;
    }

}