import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {User} from "../../data/user";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/publishReplay";
import {AuthenticationService} from "../../auth/auth.service";
import {UserRole} from "../../data/user.role";
import {UserService} from "../../service/user.service";
import {UserRoleService} from "../../service/user-role.service";
import {Observable} from "rxjs/Rx";

@Component({
    templateUrl: '/resources/angular/app/components/admin/users/admin-edit-user-template.html',
    styleUrls: ['/resources/css/main.css', '/resources/css/admin.css'],
})

export class AdminEditUserComponent implements OnInit {

    private user: User = null;
    private originalUser: User = null;
    private error: any = null;
    private obsAllRoles$: Observable<UserRole[]> = null;
    private userId: number = null;
    private obsUser$: Observable<User[]> = null;

    private formSubmitting: boolean = false;

    constructor(private auth: AuthenticationService, private router: Router,
                private route: ActivatedRoute, private userService: UserService, private roleService: UserRoleService) {
    }

    ngOnInit() : void {
        this.userId = +this.route.snapshot.params['id'];
        this.obsUser$ = this.userService.getUser(this.userId);
        this.obsUser$.subscribe( users => {
            if (users[0] !== undefined) {
                this.user = users[0];
                this.originalUser = JSON.parse(JSON.stringify(this.user));
            }
        } );
        this.obsAllRoles$ = this.roleService.getUserRoles();
        this.obsAllRoles$.subscribe();

    }

    private handleError (error: any) {
        let errMsg: string;
        errMsg = error.message ? error.message : error.toString();
        console.error(errMsg);
        this.error = errMsg;
        return errMsg;
    }

    private checkRole(role: UserRole) {
        if (role.name == "Guest")
            return;
        let index = this.user.userRoles.findIndex(function(r) { return r.id == role.id; });
        if (index!= -1) {
            this.user.userRoles.splice(index, 1);
        } else {
            this.user.userRoles.push(role);
        }
    }

    private isRolePresent(role: UserRole): number {
        if (this.user.userRoles.find(function(r){ return r.id == role.id; }) !== undefined) return 1;
        return 0;
    }

    // Submit form
    private submit() {
        this.formSubmitting = true;
        // Skip admin saving
        if (this.isAdmin(this.originalUser)) {
            this.router.navigate(['/admin/users']);
        } else {
            this.userService.updateUser(this.user).subscribe(
                data => {
                    this.user = data;
                    this.router.navigate(['/admin/users']);
                },
                error => {
                    this.handleError(error);
                    this.error = error;
                    this.formSubmitting = false;
                }
            );
        }
    }

    // Restore model and go back
    private cancel() {
        this.user.userRoles = this.originalUser.userRoles;
        this.user.blocked = this.originalUser.blocked;
        this.router.navigate(['/admin/users']);
    }

    private isAdmin(u: User): boolean {
        return UserService.isAdmin(u)
    }

    private getAuthLogin() {
        return this.auth.login;
    }

}
