import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[incass-list]',
})
export class ApplicationIncassationListDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}