import {OnInit, Component} from "@angular/core";
import {IncassationObjectService} from "../../service/incassation-object.service";
import {IncassationObject} from "../../data/incassation-object";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Component({
    templateUrl: '/resources/angular/app/components/application/application-object/application_incassation-dialog_template.html',
    selector: 'incass-dialog',
})

export class IncassationSelectDialogComponent implements OnInit {

    private obsIncassationObjects: Observable<IncassationObject[]> = null;

    private isLoaded: boolean;
    private exclude: IncassationObject[];
    private objList: IncassationObject[];

    private currentObject: IncassationObject;

    private obsResult: ReplaySubject<IncassationObject>;

    constructor(private incassationObjectService: IncassationObjectService, exclude: IncassationObject[]) {
        this.currentObject = null;
        this.obsResult = new ReplaySubject<IncassationObject>(1);
        if (!exclude) {
            this.exclude = [];
        } else {
            this.exclude = exclude;
        }
    }

    ngOnInit() {
        this.obsIncassationObjects = this.incassationObjectService.getIncassationObjects();
        this.obsIncassationObjects.subscribe( data => {
            for (let i = 0; i < data.length; ++i) {
                let obj: IncassationObject = data[i];
                if (-1 == this.exclude.findIndex( p => p.id == obj.id )) {
                    this.objList.push(obj);
                }
            }
            this.isLoaded = true;
        });
    }

    selectObject(obj: IncassationObject) {
        this.currentObject = obj;
    }

    submit() {
        this.obsResult.next(this.currentObject);
    }
}