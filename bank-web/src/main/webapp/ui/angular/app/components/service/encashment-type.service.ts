import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {EncashmentType} from "../data/encashment-type";

@Injectable()
export class EncashmentTypeService {

    private apiUrl = "/api/v1/encashment_type";
    private bhEncashmentTypes: ReplaySubject<EncashmentType[]> = null;
    private bhEncashmentTypesUpdater: ReplaySubject<EncashmentType> = null;
    private storage : {
        encashmentTypes: EncashmentType[];
    };

    constructor(private http: HttpService) {
        this.storage = { encashmentTypes: [] };
        this.bhEncashmentTypes = new ReplaySubject(1);
        this.bhEncashmentTypesUpdater = new ReplaySubject(1);
        this.http.get(this.apiUrl)
            .map( response => response.result )
            .subscribe( data => this.updateStorage(data) );
    }

    getEncashmentTypes() {
        return this.bhEncashmentTypes;
    }

    getEncashmentType(id: number) {
        return this.getEncashmentTypes().map( lst => lst.filter(t => t.id == id) );
    }

/*    update(at: EncashmentType) {
        let obs = this.http.put(`${this.apiUrl}/${user.id}`, JSON.stringify(user))
            .map(response => response.result.user);
        obs.subscribe(
            user => {
                let u: User = this.storage.users.find(iterUser => iterUser.id == user.id);
                u = user;
                this.bhEncashmentTypesUpdater.next(u);
                this.updateStorage(this.storage.users);
            },
            error => {
                this.bhEncashmentTypesUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhEncashmentTypesUpdater.asObservable();
    }*/

    private updateStorage(data: any) {
        this.storage.encashmentTypes = data;
        this.bhEncashmentTypes.next(data);
    }

}