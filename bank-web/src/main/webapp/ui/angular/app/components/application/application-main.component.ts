import {Component} from "@angular/core";
import "@angular/router";

@Component({
    templateUrl: '/resources/angular/app/components/application/application-main-template.html',
    styleUrls: ['/resources/css/main.css', '/resources/css/forms.css'],
})

export class ApplicationMainComponent {

}
