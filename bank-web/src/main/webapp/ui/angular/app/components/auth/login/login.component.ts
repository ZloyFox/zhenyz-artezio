import {Component} from "@angular/core";
import {Credentials} from "../credentials";
import {LoginService} from "./login.service";
import {Router} from "@angular/router";
import {AuthenticationService} from "../auth.service";

@Component({
    templateUrl: '/resources/angular/app/components/auth/login/login-template.html',
    styleUrls: ['/resources/css/login.css','/resources/css/main.css']
})

export class LoginComponent {

    credentials : Credentials;
    authTokenReceived : boolean;
    errorMessage: string;
    private isSubmitting: boolean;

    constructor(private loginService: LoginService, private auth: AuthenticationService, private router: Router) {
        this.authTokenReceived = false;
        this.credentials = new Credentials();
        this.errorMessage = null;
        this.isSubmitting = false;
    }

    submit(){
        this.errorMessage = null;
        this.isSubmitting = true;
        this.loginService.postLogin(this.credentials)
            .subscribe(
                (data) => {
                    // this.loginService.authenticate(data);
                    this.authTokenReceived = true;
                    this.router.navigate(['/']);
                },
                error => {
                    this.isSubmitting = false;
                    this.errorMessage = "Ошибка входа, проверьте логин и пароль";
                }
            );
    }

    navigateRegister() {
        this.router.navigate(['/signup']);
    }
}
