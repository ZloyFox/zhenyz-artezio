import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ProtectedRouteGuard} from "../auth/guard/protected-route.guard";
import {IncassationObjectComponent} from "./incassation-object/incassation-object.component";
import {IncassationComponent} from "./incassation.component";
import {IncassationObjectListComponent} from "./incassation-object/incassation-object-list.component";

const incassationRoutes : Routes = [

    { path: 'incassation_object', component: IncassationComponent, canActivate: [ProtectedRouteGuard],
        children: [
        //     { path: 'users', component: AdminUsersComponent },
        //     { path: 'edit-user/:id', component: AdminEditUserComponent }
            { path: '', component: IncassationObjectListComponent, pathMatch: 'full' },
            { path: 'add', component: IncassationObjectComponent },
            { path: 'edit/:id', component: IncassationObjectComponent },
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(incassationRoutes) ],
    exports: [ RouterModule ]
})

export class IncassationRoutingModule {}