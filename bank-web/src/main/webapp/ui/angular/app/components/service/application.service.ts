import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {Observable} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";
import {Application} from "../data/application";

@Injectable()
export class ApplicationService {

    private apiUrl = "/api/v1/application";
    private bhApplications: ReplaySubject<Application[]> = null;
    private bhApplicationsUpdater: ReplaySubject<Application> = null;
    private storage : {
        applications: Application[];
    };
    private offset: number = 0;
    private hasNoMore: boolean = false;
    public resultsCountPerFetch = 10;

    constructor(private http: HttpService) {
        this.storage = { applications: [] };
        this.bhApplications = new ReplaySubject(1);
        this.bhApplicationsUpdater = new ReplaySubject(1);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    fetch() {
        if (this.hasNoMore) {
            return;
        }
        let url = this.apiUrl + "?offset=" + this.offset + "&count=" + this.resultsCountPerFetch;
        this.http.get(url)
            .map( response => response.result )
            .subscribe(data => {
                if (data.length < this.resultsCountPerFetch) {
                    this.hasNoMore = true;
                }
                this.offset += data.length;
                this.updateStorage(data);
            });
    }

    getApplications() {
        return this.bhApplications;
    }

    getApplication(id: number) {
        if (~this.storage.applications.findIndex( x => x.id == id )) {
            return this.getApplications().map( lst => lst.filter(obj => obj.id == id) );
        } else {
            let obs1 = this.http.get(this.apiUrl + "/" + id).map( response => response.result );
            obs1.subscribe( x => {
                this.storage.applications.push(x);
            });
            return obs1;
        }
    }

    hasMoreItems() {
        return !this.hasNoMore;
    }

    refetch() {
        this.storage = {applications: []};
        this.updateStorage(this.storage.applications);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    create(application: Application) {
        let obs = this.http.post(this.apiUrl, JSON.stringify(application, ApplicationService.jsonPrepare))
            .map(response => response.result);
        obs.subscribe(
            result => {
                this.storage.applications.push(result);
                this.bhApplicationsUpdater.next(result);
                this.updateStorage(this.storage.applications);
            },
            error => {
                this.bhApplicationsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhApplicationsUpdater;
    }

    static jsonPrepare(key: string, value: any) {
        if (value == null)
            return undefined;
        if (typeof value === 'string') {
            if (value.length == 0)
                return undefined;
        }
        return value;
    }

    update(application: Application) {
        let obs = this.http.put(this.apiUrl + "/" + application.id, JSON.stringify(application, ApplicationService.jsonPrepare))
            .map(response => response.result);
        obs.subscribe(
            result => {
                let index = this.storage.applications.findIndex(iter => iter.id == result.id);
                if (~index) {
                    this.storage.applications.splice(index, 1, result);
                    this.bhApplicationsUpdater.next(result);
                } else {
                    this.storage.applications.push(result);
                }
                this.updateStorage(this.storage.applications);
            },
            error => {
                this.bhApplicationsUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhApplicationsUpdater;
    }

    private updateStorage(data: any) {
        for (let i = 0; i < data.length; ++i) {
            let obj = data[i];
            if (!(obj.date instanceof Date)) {
                obj.date = new Date(obj.date);
            }            let oldObjIndex = this.storage.applications.findIndex(r=>r.id == obj.id);
            if (~oldObjIndex) {
                this.storage.applications[oldObjIndex] = obj;
            } else {
                this.storage.applications.push(obj);
            }
        }
        this.bhApplications.next(Object.assign([], this.storage.applications));
    }

}