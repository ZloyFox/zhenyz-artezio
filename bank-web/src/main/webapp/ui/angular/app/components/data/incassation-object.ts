import {EncashmentType} from "./encashment-type";
import {IncassationPeriod} from "./incassation-period";
import {Currency} from "./currency";
import {AddressType} from "./address-type";
import {SettlementType} from "./settlement-type";
import {IncassationType} from "./incassation-type";
import {User} from "./user";

export class IncassationObject {
    id: number;
    time: string;
    encashmentType: EncashmentType;
    incassationType: IncassationType;
    incassationPeriod: IncassationPeriod;
    dayOfWeek: number;
    approxCash: number;
    currency: Currency;
    directorContactPhone: string;
    openingDate: string;
    shared: boolean;
    openingTimeWeekdays: string;
    openingTimeSaturday: string;
    openingTimeSunday: string;
    closingTimeWeekdays: string;
    closingTimeSaturday: string;
    closingTimeSunday: string;
    addressType: AddressType;
    settlementType: SettlementType;
    settlementName: string;
    streetName: string;
    buildingNumber: string;
    buildingBlockNumber: string;
    creator: User;

    constructor() {
        this.time = "";
        this.dayOfWeek = 0;
        this.approxCash = 0.0;
        this.directorContactPhone = "";
        this.shared = false;
        this.openingDate = "";
        this.openingTimeSaturday = "";
        this.openingTimeSunday = "";
        this.openingTimeWeekdays = "";
        this.closingTimeSaturday = "";
        this.closingTimeSunday = "";
        this.closingTimeWeekdays = "";
        this.settlementName = "";
        this.streetName = "";
        this.buildingBlockNumber = "";
        this.buildingBlockNumber = "";
        this.addressType = new AddressType();
        this.settlementType = new SettlementType();
        this.encashmentType = new EncashmentType();
        this.incassationType = new IncassationType();
        this.incassationPeriod = new IncassationPeriod();
        this.creator = new User();
    }
}
