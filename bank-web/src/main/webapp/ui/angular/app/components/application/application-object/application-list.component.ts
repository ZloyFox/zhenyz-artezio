import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/publishReplay";
import {Application} from "../../data/application";
import {ApplicationType} from "../../data/application-type";
import {ApplicationTypeService} from "../../service/application-type.service";
import {BankObjectService} from "../../service/bank-object.service";
import {ApplicationService} from "../../service/application.service";

@Component({
    templateUrl: '/resources/angular/app/components/application/application-object/application-list-template.html',
    styles: [
        ".table tbody tr.selected {background-color: rgba(80,36,255,0.18);}",
        ".table tbody tr { cursor: pointer; }",
    ],
})

export class ApplicationListComponent implements OnInit {

    private error: any = null;

    private obsApplications: Observable<Application[]> = null;
    private obsApplicationTypes: Observable<ApplicationType[]>;

    private currentRow: number = -1;
    private currentApplication: Application = null;
    private loaded: boolean;
    private noMoreItems: boolean;

    constructor(private router: Router, private applicationTypeService: ApplicationTypeService,
                private applicationService: ApplicationService) {
    }

    ngOnInit(): void {
        this.loaded = false;
        this.noMoreItems = false;
        this.currentRow = -1;
        this.currentApplication = null;
        this.loadData();
    }

    loadData(): void {
        this.obsApplicationTypes = this.applicationTypeService.getApplicationTypes();
        this.obsApplicationTypes.subscribe( data => {
            this.obsApplications = this.applicationService.getApplications();
            this.obsApplications.subscribe( r => {
                this.noMoreItems = !this.applicationService.hasMoreItems();
                this.loaded = true;
            });
        });
    }

    private fetchNext(): void {
        this.applicationService.fetch();
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private selectRow(row: number, appl: Application) {
        this.currentRow = row;
        this.currentApplication = appl;
    }

    private editSelected() {
        this.router.navigate(['/application/edit', this.currentApplication.id]);
    }

    static pad(num: number, size: number): string {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    formatId(id: number): string {
        let s = "2-";
        s = s + ApplicationListComponent.pad(id, 7);
        return s;
    }

    formatDate(date: Date): string {
        return date.toLocaleString();
    }

}
