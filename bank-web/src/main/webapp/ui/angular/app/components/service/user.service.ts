import {Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {HttpService} from "../http/http.service";
import {User} from "../data/user";
import {Observable} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/ReplaySubject";
import "rxjs/operator/merge";

@Injectable()
export class UserService {

    private userUrl = "/api/v1/user";
    private bhUsers: ReplaySubject<User[]> = null;
    private bhUserUpdater: ReplaySubject<User> = null;

    private offset: number = 0;
    private hasNoMore: boolean = false;
    private storage : {
        users: User[];
    };
    public resultsCountPerFetch = 20;

    constructor(private http: HttpService) {
        this.storage = { users: [] };
        this.bhUsers = new ReplaySubject(1);
        this.offset = 0;
        this.hasNoMore = false;
        this.bhUserUpdater = new ReplaySubject(1);
        this.fetch();
    }

    public fetch() {
        if (this.hasNoMore) {
            return;
        }
        let url = this.userUrl + "?offset=" + this.offset + "&count=" + this.resultsCountPerFetch;
        this.http.get(url)
            .map(response => response.result)
            .subscribe(data => {
                if (data.length < this.resultsCountPerFetch) {
                    this.hasNoMore = true;
                }
                this.offset += data.length;
                this.updateStorage(data);
            });
    }

    hasMoreItems() {
        return !this.hasNoMore;
    }

    getUsers() {
        return this.bhUsers;
    }

    refetch() {
        this.storage = {users: []};
        this.updateStorage(this.storage.users);
        this.offset = 0;
        this.hasNoMore = false;
        this.fetch();
    }

    getUser(id: number) {
        if (this.storage.users.findIndex( user => user.id == id )) {
            return this.getUsers().map( users => users.filter(user => user.id == id) );
        } else {
            let obs1 = this.http.get(this.userUrl + "/").map( response => response.result );
            obs1.subscribe( user => {
                this.storage.users.push(user);
            });
            return obs1;
        }
    }

    updateUser(user: User) {
        let obs = this.http.put(this.userUrl + "/" + user.id, JSON.stringify(user))
            .map(response => response.result);
        obs.subscribe(
            user => {
                let u = this.storage.users.find(iterUser => iterUser.id == user.id);
                u = user;
                this.bhUserUpdater.next(u);
                this.updateStorage(this.storage.users);
            },
            error => {
                this.bhUserUpdater.error(error);
                Observable.throw(error)
            }
        );
        return this.bhUserUpdater.asObservable();
    }

    static isAdmin(user: User): boolean {
        return user.userRoles.findIndex(role => role.name == "Admin") != -1;
    }

    private updateStorage(data: any) {
        for (let i = 0; i < data.length; ++i) {
            let user = data[i];
            let oldUser = this.storage.users.find(r=>r.id == user.id);
            if (oldUser) {
                oldUser = user;
            } else {
                this.storage.users.push(user);
            }
        }
        this.bhUsers.next(this.storage.users);
    }

}