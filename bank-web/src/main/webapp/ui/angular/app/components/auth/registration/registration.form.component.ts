import {Component, AfterViewChecked, ViewChild} from "@angular/core";
import {NgForm} from "@angular/forms";

@Component({
    selector: 'registration-form-template',
    templateUrl: '/resources/angular/app/components/auth/registration/registration-template.html'
})

export class RegistrationFormTemplateComponent implements AfterViewChecked {

    registrationForm: NgForm;
    @ViewChild('registrationForm') currentForm: NgForm;
    serverError: string = null;

    ngAfterViewChecked() {
        this.formChanged();
    }

    formChanged() {
        if (this.currentForm === this.registrationForm) {
            return;
        }
        this.registrationForm = this.currentForm;
        if (this.registrationForm) {
            this.registrationForm.valueChanges
                .subscribe(data => this.onValueChanged(data));
        }
    }

    onValueChanged(data?: any) {
        if (!this.registrationForm) { return; }
        const form = this.registrationForm.form;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };

    formErrors = {
        'login': '',
        'password': '',
        'password2': '',
    };

    validationMessages = {
        'login': {
            'required':      'Введите логин',
            'minlength':     'Логин должен быть длиной не менее 4 символов',
            'maxlength':     'Логин должен быть длиной не более 20 символов'
        },
        'password': {
            'required': 'Введите пароль'
        },
        'password2': {
            'required': 'Введите пароль повторно',
            'notmatch': 'Введенные пароли не совпадают'
        }
    };

}