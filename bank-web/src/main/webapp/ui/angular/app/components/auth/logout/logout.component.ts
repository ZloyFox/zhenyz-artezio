import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthenticationService} from "../auth.service";
import "rxjs/add/operator/catch";

@Component({
    template: "<h3>Выхожу...</h3>",
    styleUrls: ['/resources/css/main.css']
})

export class LogoutComponent implements OnInit {

    error: string = null;

    constructor(private router: Router, private auth: AuthenticationService) {
    }

    ngOnInit(): void {
        this.auth.logout();
        this.router.navigate(['/']);
    }

}
