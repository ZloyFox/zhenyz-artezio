import {UserRole} from "./user.role";

export class User {
    id: number;
    login: string;
    userRoles: UserRole[];
    blocked: boolean;
}
