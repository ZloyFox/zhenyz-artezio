import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/publishReplay";
import {Location} from "@angular/common";
import {FormGroup, FormBuilder, Validators, FormControl, AbstractControl, ValidatorFn} from "@angular/forms";
import {ApplicationTypeService} from "../../service/application-type.service";
import {BankObjectService} from "../../service/bank-object.service";
import {BankObject} from "../../data/bank-object";
import {ApplicationType} from "../../data/application-type";
import {Application} from "../../data/application";
import {ApplicationService} from "../../service/application.service";
import {IncassationObject} from "../../data/incassation-object";

@Component({
    templateUrl: '/resources/angular/app/components/application/application-object/application-template.html',
    styles: [
        ".req:after{ color: #af2600; content: '*'; padding-left: 4px; }",
        ".container { padding-bottom: 200px; }",
        ".alert { padding: 8px; margin-bottom: 4px;}",
        ".sep { text-align: left; border-top: 1px solid #dadada; padding: 4px 17px 0; margin-top: 15px;}",
        ".buttons-default { padding-left: 8px; margin-top: 16px }",
    ],
    selector: 'application-form',
})

export class ApplicationComponent implements OnInit {

    private error: any = null;
    private application: Application = null;
    private originalApplication: Application = null;

    private obsApplication: Observable<Application[]> = null;
    private obsApplicationTypes: Observable<ApplicationType[]> = null;
    private obsBankObjects: Observable<BankObject[]> = null;

    private formSubmitting: boolean = false;
    private readinessLevel: number = 0;
    private commonErrorString: string;

    private form: FormGroup = null;

    private bNew: boolean;
    private bSubmitting: boolean;

    private computedClientAccount: string;

    private incassList: IncassationObject[];

    private static digitsOnlyRegex: RegExp = /^\d*$/;
    private static clientAccountPattern = [7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1];

    constructor(private router: Router, private applicationService: ApplicationService,
                private applicationTypeService: ApplicationTypeService, private bankObjectService: BankObjectService,
                private route: ActivatedRoute, private location: Location, private formBuilder: FormBuilder) {
    }

    loadData(): void {
        this.incassList = [];
        this.readinessLevel = 0;
        this.obsApplicationTypes = this.applicationTypeService.getApplicationTypes();
        this.obsApplicationTypes.subscribe(r=>this.loadNextAndCheck());
        this.obsBankObjects = this.bankObjectService.getBankObjects();
        this.obsBankObjects.subscribe(r=>this.loadNextAndCheck());
    }

    ngOnInit(): void {
        this.readinessLevel = 0;
        this.commonErrorString = null;
        this.computedClientAccount = "";
        this.loadData();
    }

    buildForm(): void {
        // this.cbAddressType = new FormControl(this.application.addressType);
        this.incassList = this.application.incassationObjects;
        this.form = this.formBuilder.group({
                'applicationType': [
                    this.application.applicationType,
                    [
                        this.cbValidator()
                    ]
                ],
                'date': [
                    this.application.date,
                ],
                'bankObject': [
                    this.application.bankObject,
                    [
                        this.cbValidator()
                    ]
                ],
                'inn': [
                    this.application.inn,
                ],
                'kpp': [
                    this.application.kpp,
                ],
                'ogrn': [
                    this.application.ogrn,
                ],
                'organizationName': [
                    this.application.organizationName,
                ],
                'contactName': [
                    this.application.contactName,
                ],
                'contactPhone': [
                    this.application.contactPhone,
                ],
                'clientAccount': [
                    this.application.clientAccount,
                    [
                        this.clientAccountValidator,
                        Validators.pattern(ApplicationComponent.digitsOnlyRegex),
                    ]
                ],
                'bankCode': [
                    this.application.bankCode,
                    [
                        Validators.pattern(ApplicationComponent.digitsOnlyRegex),
                    ]
                ],
                'bankAccountNumber': [
                    this.application.bankAccountNumber,
                ],
                'bankName': [
                    this.application.bankName,
                ],
                'bankSwift': [
                    this.application.bankSwift,
                ],
                'bankOtherInfo': [
                    this.application.bankOtherInfo,
                ],
            }
        );
        this.form.valueChanges
            .subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // (re)set validation messages now

        this.bSubmitting = false;
    }

    onValueChanged(data?: any, forceErrors?: boolean) {
        if (!this.form) { return; }
        this.commonErrorString = null;
        const form = this.form;
        this.application = form.value;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && ((forceErrors && forceErrors == true && !control.valid) ||
                (control.touched && control.dirty && !control.valid))) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
        // Detailed clientAccountNumber message
        //'invalid' : ,
        let control = form.get('clientAccount');
        if (control && ((forceErrors && forceErrors == true && !control.valid) ||
            (control.touched && control.dirty && !control.valid))) {
            if (control.errors['invalid']) {
                this.formErrors['clientAccount'] += 'Ключ счета ' + this.application.clientAccount + ' не верен. Должен быть ' + this.computedClientAccount;
            }
        }
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private loadNextAndCheck(): void {
        ++this.readinessLevel;
        if (this.readinessLevel == 2) {
            this.loadFormData();
        }
    }

    /**
     * Check is provided client account numbers and BIK are valid
     * @return true if valid, false otherwise
     */
    private clientAccountValidator(): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} => {
            let result: boolean = true;
            try {
                this.getClientAccountKey();
                let bik = control.value;
                let orgn = bik[6] + bik[7] + bik[8];
                let tnum = orgn + this.application.clientAccount;

                let num = 0;
                for (let i = 0; i < tnum.length; ++i) {
                    num = num + ((ApplicationComponent.clientAccountPattern[i]*parseInt(tnum[i], 10)) % 10);
                }
                result = (0 == (num % 10));
            } catch (e) {
                result = false;
            }
            return result ? null : {'invalid': true};
        };
    }

    getClientAccountKey() {
        let bik = this.application.bankCode;
        let orgn = bik[6] + bik[7] + bik[8];
        let tnum = orgn + this.application.clientAccount;
        tnum = tnum.substr(0, 11) + '0' + tnum.substr(12);

        let num = 0;
        for (let i = 0; i < tnum.length; i++ ) {
            num = num + ((ApplicationComponent.clientAccountPattern[i]*parseInt( tnum[i], 10 )) % 10);
        }
        this.computedClientAccount = (((num % 10)*3)%10).toString();
    }

    private loadFormData(): void {
        let id = +this.route.snapshot.params['id'];
        if (typeof id !== 'undefined' && id != null && !isNaN(id)) {
            this.obsApplication = this.applicationService.getApplication(id);
            this.bNew = false;
            this.obsApplication.subscribe( objects => {
                if (typeof objects[0] !== 'undefined') {
                    this.application = objects[0];

                    this.application.id = objects[0].id;
                    this.originalApplication = JSON.parse(JSON.stringify(this.application));

                    this.applicationTypeService.getApplicationType(this.application.applicationType.id).subscribe( r => this.application.applicationType = r[0] );
                    this.bankObjectService.getBankObject(this.application.bankObject.id).subscribe( r => {
                        this.application.bankObject = r[0];
                    });
                    this.buildForm();
                }
            } );
        } else {
            this.bNew = true;
            this.application = new Application();
            this.application.date = new Date();
            console.log("Created new with incassObjs=" + JSON.stringify(this.application.incassationObjects));
            this.buildForm();
        }
    }

    private submit() {
        // Make sure all errors will appear (if any)
        this.onValueChanged(null, true);

        // Show error or submit
        if (!this.form.valid) {
            this.commonErrorString = "Форма заполнена неверно";
        } else {
            this.application.incassationObjects = this.incassList;
            this.commonErrorString = null;
            this.bSubmitting = true;
            if (this.bNew) {
                this.applicationService.create(this.application).subscribe(
                    data => {
                        this.router.navigate(['application']);
                    },
                    error => {
                        this.error = error;
                        this.bSubmitting = false;
                    }
                );
            } else {
                this.application.id = this.originalApplication.id;
                this.applicationService.update(this.application).subscribe(
                    data => {
                        this.application = data;
                        this.router.navigate(['application']);
                    },
                    error => {
                        this.error = error;
                        this.bSubmitting = false;
                    }
                );
            }
        }
    }

    private cancel() {
        if (this.originalApplication && typeof this.originalApplication.id !== 'undefined') {
            this.application = this.originalApplication;
        }
        this.router.navigate(['/application']);
    }

    private onChangedList(list: IncassationObject[]) {
        console.log("Changed list size " + list.length);
        this.incassList = list;
    }

    formErrors = {
        'bankObject': '',
        'applicationType': '',
        'date': '',
        'inn': '',
        'kpp': '',
        'organizationName': '',
        'ogrn': '',
        'contactName': '',
        'contactPhone': '',
        'clientAccount': '',
        'bankCode': '',
        'bankAccountNumber': '',
        'bankName': '',
        'bankSwift': '',
        'bankOtherInfo': '',
    };

    validationMessages = {
        'bankObject': {
            'required' : 'Не указано подразделение банка',
        },
        'applicationType': {
            'required' : 'Не указан тип заявки',
        },
        'inn': {
            'maxlength' : 'Максимальная длина поля - 12 символов',
            'required' : 'Заполните поле',
        },
        'kpp': {
            'maxlength' : 'Максимальная длина поля - 12 символов',
            'required' : 'Заполните поле',
        },
        'organizationName': {
            'required' : 'Заполните поле',
        },
        'ogrn': {
            'maxlength' : 'Максимальная длина поля - 13 символов',
        },
        'contactName': {
            'required' : 'Не указано имя уполномоченного сотрудника организации клиента для решения вопросов организации инкассации',
        },
        'contactPhone': {
            'required' : 'Не указан номер телефона уполномоченного сотрудника организации клиента для решения вопросов организации инкассации',
        },
        'clientAccount': {
            'required' : 'Не указан счет для перечисления инкассированных средств',
            'maxlength' : 'Количество символов в поле Счет не соответствует норме (20)',
            'minlength' : 'Количество символов в поле Счет не соответствует норме (20)',
            'pattern' : 'Номер расчетного счета содержит недопустимые символы',
        },
        'bankCode': {
            'required' : 'Не указан БИК',
            'maxlength' : 'Количество символов в поле БИК не соответствует норме (9)',
            'minlength' : 'Количество символов в поле БИК не соответствует норме (9)',
            'pattern' : 'Введены недопустимые символы в поле БИК'
        },
        'bankAccountNumber': {
            'required' : 'Заполните поле',
            'maxlength' : 'Максимальная длина поля - 20 символов',
        },
        'bankName': {
            'required' : 'Не указано наименование банка',
        },
        'bankSwift': {
            'required' : 'Заполните поле',
            'maxlength' : 'Максимальная длина поля - 11 символов',
        },
        'bankOtherInfo': {},
    };

    cbValidator() : ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} => {
            let v = control.value;
            if (v == null || !v.hasOwnProperty("id")) {
                return { 'required': true }
            }
            return null;
        };
    }

    get applicationDate(){
        let date = this.application.date;
        return date.getDay() + "." + date.getMonth() + "." + date.getFullYear() +
                " " + date.getHours() + ":" + date.getMinutes();
    }

}
