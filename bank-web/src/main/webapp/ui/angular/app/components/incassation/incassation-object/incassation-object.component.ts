import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/publishReplay";
import {IncassationObject} from "../../data/incassation-object";
import {EncashmentType} from "../../data/encashment-type";
import {IncassationObjectService} from "../../service/incassation-object.service";
import {AddressTypeService} from "../../service/address-type.service";
import {EncashmentTypeService} from "../../service/encashment-type.service";
import {AddressType} from "../../data/address-type";
import {IncassationPeriod} from "../../data/incassation-period";
import {IncassationPeriodService} from "../../service/incassation-period.service";
import {Currency} from "../../data/currency";
import {CurrencyService} from "../../service/currency.service";
import {SettlementType} from "../../data/settlement-type";
import {SettlementTypeService} from "../../service/settlement-type.service";
import {IncassationType} from "../../data/incassation-type";
import {IncassationTypeService} from "../../service/incassation-type.service";
import {Location} from "@angular/common";
import {FormGroup, FormBuilder, Validators, FormControl, AbstractControl, ValidatorFn} from "@angular/forms";
import {futureDateValidator} from "../../shared/validator/validators.directive";


@Component({
    templateUrl: '/resources/angular/app/components/incassation/incassation-object/incassation-object-template.html',
    styles: [
        ".req:after{ color: #af2600; content: '*'; padding-left: 4px; }",
        ".container { padding-bottom: 200px; }",
        ".alert { padding: 8px; margin-bottom: 4px;}",
        ".sep { text-align: left; border-top: 1px solid #dadada; padding: 4px 17px 0; margin-top: 15px;}",
        ".buttons-default { padding-left: 8px; margin-top: 16px }",
    ],
    selector: 'incassation-object-form',
})

export class IncassationObjectComponent implements OnInit {

    private error: any = null;
    private incassationObject: IncassationObject = null;
    private originalIncassationObject: IncassationObject = null;

    private obsIncassObject: Observable<IncassationObject[]> = null;
    private obsAddressTypes: Observable<AddressType[]> = null;
    private obsCurrencies: Observable<Currency[]> = null;
    private obsEncashmentTypes: Observable<EncashmentType[]>;
    private obsIncassationPeriods: Observable<IncassationPeriod[]>;
    private obsIncassationTypes: Observable<IncassationType[]>;
    private obsSettlementTypes: Observable<SettlementType[]> = null;

    private formSubmitting: boolean = false;
    private readinessLevel: number = 0;
    private commonErrorString: string;

    private form: FormGroup = null;

    private cbAddressType: FormControl = null;

    private bNew: boolean;
    private bSubmitting: boolean;

    private static timeRegex: RegExp = /^([01]\d:[0-5]\d)|([2][0123]:[0-5]\d)$/;
    private static dateRegex: RegExp = /^[0-3]\d\.(([0]\d)|([1][012]))\.[2]\d\d\d$/;
    private static cashRegex: RegExp = /^\d{1,16}(\.\d{1,2})?$/;

    private days = [
        {id: 0, text: 'Понедельник'},
        {id: 1, text: 'Вторник'},
        {id: 2, text: 'Среда'},
        {id: 3, text: 'Четверг'},
        {id: 4, text: 'Пятница'},
        {id: 5, text: 'Суббота'},
        {id: 6, text: 'Воскресенье'},
    ];

    constructor(private router: Router, private incassationObjectService: IncassationObjectService,
                private addressTypeService: AddressTypeService, private encashmentTypeService: EncashmentTypeService,
                private route: ActivatedRoute, private incassationPeriodService: IncassationPeriodService,
                private currencyService: CurrencyService, private settlementTypeService: SettlementTypeService,
                private incassationTypeService: IncassationTypeService, private location: Location,
                private formBuilder: FormBuilder) {
    }

    loadData(): void {
        this.readinessLevel = 0;
        this.obsAddressTypes = this.addressTypeService.getAddressTypes();
        this.obsAddressTypes.subscribe(r=>this.loadNextAndCheck());
        this.obsEncashmentTypes = this.encashmentTypeService.getEncashmentTypes();
        this.obsEncashmentTypes.subscribe(r=>this.loadNextAndCheck());
        this.obsIncassationPeriods = this.incassationPeriodService.getIncassationPeriods();
        this.obsIncassationPeriods.subscribe(r=>this.loadNextAndCheck());
        this.obsIncassationTypes = this.incassationTypeService.getIncassationTypes();
        this.obsIncassationTypes.subscribe(r=>this.loadNextAndCheck());
        this.obsCurrencies = this.currencyService.getCurrencies();
        this.obsCurrencies.subscribe(r=>this.loadNextAndCheck());
        this.obsSettlementTypes = this.settlementTypeService.getSettlementTypes();
        this.obsSettlementTypes.subscribe(r=>this.loadNextAndCheck());
    }

    ngOnInit(): void {
        this.readinessLevel = 0;
        this.commonErrorString = null;
        this.loadData();
    }

    buildForm(): void {
        this.cbAddressType = new FormControl(this.incassationObject.addressType);
        this.form = this.formBuilder.group({
                'time': [
                    this.incassationObject.time,
                    [
                        Validators.pattern(IncassationObjectComponent.timeRegex)
                    ]
                ],
                'encashmentType': [
                    this.incassationObject.encashmentType,
                    [
                        this.cbValidator()
                    ]
                ],
                'incassationPeriod': [
                    this.incassationObject.incassationPeriod,
                    [
                        this.cbValidator()
                    ]
                ],
                'dayOfWeek': [
                    this.incassationObject.dayOfWeek
                ],
                'approxCash': [
                    this.incassationObject.approxCash,
                    [
                        Validators.pattern(IncassationObjectComponent.cashRegex)
                    ]
                ],
                'currency': [
                    this.incassationObject.currency,
                    [
                        this.cbValidator()
                    ]
                ],
                'directorContactPhone': [
                    this.incassationObject.directorContactPhone,
                ],
                'openingDate': [
                    this.incassationObject.openingDate,
                    [
                        Validators.pattern(IncassationObjectComponent.dateRegex),
                        futureDateValidator
                    ]
                ],
                'addressType': this.cbAddressType,
                'settlementType': [
                    this.incassationObject.settlementType,
                    [
                        this.cbValidator()
                    ]
                ],
                'settlementName': [
                    this.incassationObject.settlementName
                ],
                'streetName': [
                    this.incassationObject.streetName
                ],
                'buildingNumber': [
                    this.incassationObject.buildingNumber
                ],
                'buildingBlockNumber': [
                    this.incassationObject.buildingBlockNumber
                ],
                'incassationType': [
                    this.incassationObject.incassationType,
                    [
                        this.cbValidator()
                    ]
                ],
                'openingTimeWeekdays': [
                    this.incassationObject.openingTimeWeekdays,
                ],
                'openingTimeSunday': [
                    this.incassationObject.openingTimeSunday,
                ],
                'openingTimeSaturday': [
                    this.incassationObject.openingTimeSaturday,
                ],
                'closingTimeWeekdays': [
                    this.incassationObject.closingTimeWeekdays,
                ],
                'closingTimeSunday': [
                    this.incassationObject.closingTimeSunday,
                ],
                'closingTimeSaturday': [
                    this.incassationObject.closingTimeSaturday,
                ],
            }, {validator: this.xrefValidator(
            [
                {k1: 'openingTimeWeekdays', k2: 'closingTimeWeekdays'},
                {k1: 'openingTimeSaturday', k2: 'closingTimeSaturday'},
                {k1: 'openingTimeSunday', k2: 'closingTimeSunday'}
            ])}
        );
        this.form.valueChanges
            .subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // (re)set validation messages now

        this.bSubmitting = false;
    }

    cbValidator() : ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} => {
            let v = control.value;
            if (v == null || !v.hasOwnProperty("id")) {
                return { 'required': true }
            }
            return null;
        };
    }

    /**
     * Validate two Time fields: Both fields should be empty or filled
     * @param args - list of pairs of field keys
     * @returns validation func
     */
    private xrefValidator(args: [{k1: string, k2: string}]) {
        let popError = (c: AbstractControl) => {
            let errs = c.errors;
            delete errs['mutural'];
            c.setErrors(errs);
        };

        let pushError = (c: AbstractControl, c2: AbstractControl) => {
            let errs1 = c.errors;
            if (!errs1)
                errs1 = {};
            errs1['mutural'] = 'x';
            c.setErrors(errs1);
            c.markAsDirty();
            if (c2.errors && c2.errors.hasOwnProperty('mutural')) {
                popError(c2);
            }
        };

        return (group: FormGroup) => {
            for (let i = 0; i < args.length; ++i) {
                let inp1 = group.controls[args[i].k1];
                let inp2 = group.controls[args[i].k2];
                let val1 = inp1.value;
                let val2 = inp2.value;
                if (val1 != null && val2 != null) {
                    if (val1.length != 0 && val2.length == 0) {
                        pushError(inp2, inp1);
                    } else if (val1.length == 0 && val2.length != 0) {
                        pushError(inp1, inp2);
                    } else {
                        if (inp2.errors && inp2.errors.hasOwnProperty('mutural')) {
                            popError(inp2);
                        }
                        if (inp1.errors && inp1.errors.hasOwnProperty('mutural')) {
                            popError(inp2);
                        }
                    }
                }
            }
        }
    }

    onValueChanged(data?: any, forceErrors?: boolean) {
        if (!this.form) { return; }
        this.commonErrorString = null;
        const form = this.form;
        this.incassationObject = form.value;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && ((forceErrors && forceErrors == true && !control.valid) ||
                (control.touched && control.dirty && !control.valid))) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    console.log("Push err " + field + ":" + key);
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    private handleError(error: any) {
        console.warn("HandleError");
        this.error = error;
    }

    private loadNextAndCheck(): void {
        ++this.readinessLevel;
        if (this.readinessLevel == 6) {
            this.loadFormData();
        }
    }

    private loadFormData(): void {
        let id = +this.route.snapshot.params['id'];
        if (typeof id !== 'undefined' && id != null && !isNaN(id)) {
            this.obsIncassObject = this.incassationObjectService.getIncassationObject(id);
            this.bNew = false;
            this.obsIncassObject.subscribe( objects => {
                if (typeof objects[0] !== 'undefined') {
                    this.incassationObject = objects[0];

                    this.incassationObject.id = objects[0].id;
                    this.originalIncassationObject = JSON.parse(JSON.stringify(this.incassationObject));

                    this.addressTypeService.getAddressType(this.incassationObject.addressType.id).subscribe( r => this.incassationObject.addressType = r[0] );
                    this.currencyService.getCurrency(this.incassationObject.currency.id).subscribe( r => {
                        this.incassationObject.currency = r[0];
                    } );
                    this.incassationPeriodService.getIncassationPeriod(this.incassationObject.incassationPeriod.id).subscribe( r => {
                        this.incassationObject.incassationPeriod = r[0];
                    } );
                    this.incassationTypeService.getIncassationType(this.incassationObject.incassationType.id).subscribe( r => {
                        this.incassationObject.incassationType = r[0];
                    } );
                    this.encashmentTypeService.getEncashmentType(this.incassationObject.encashmentType.id).subscribe( r => {
                        this.incassationObject.encashmentType = r[0];
                    } );
                    if (this.incassationObject.settlementType) {
                        this.settlementTypeService.getSettlementType(this.incassationObject.settlementType.id).subscribe(r => {
                            this.incassationObject.settlementType = r[0];
                        });
                    }

                    this.buildForm();
                }
            } );
        } else {
            this.bNew = true;
            this.incassationObject = new IncassationObject();
            this.buildForm();
        }
    }


    private submit() {
        // Make sure all errors will appear (if any)
        this.onValueChanged(null, true);

        // Show error or submit
        if (!this.form.valid) {
            this.commonErrorString = "Форма заполнена неверно";
        } else {
            this.commonErrorString = null;
            this.bSubmitting = true;
            if (this.bNew) {
                this.incassationObjectService.create(this.incassationObject).subscribe(
                    data => {
                        this.router.navigate(['incassation_object']);
                    },
                    error => {
                        this.error = error;
                        this.bSubmitting = false;
                    }
                );
            } else {
                this.incassationObject.id = this.originalIncassationObject.id;
                this.incassationObjectService.update(this.incassationObject).subscribe(
                    data => {
                        this.incassationObject = data;
                        this.router.navigate(['incassation_object']);
                    },
                    error => {
                        this.error = error;
                        this.bSubmitting = false;
                    }
                );
            }
        }
    }

    private cancel() {
        if (this.originalIncassationObject && typeof this.originalIncassationObject.id !== 'undefined') {
            this.incassationObject = this.originalIncassationObject;
        }
        this.router.navigate(['/incassation_object']);
    }

    formErrors = {
        'time': '',
        'encashmentType': '',
        'incassationPeriod': '',
        'dayOfWeek': '',
        'approxCash': '',
        'currency': '',
        'directorContactPhone': '',
        'openingDate': '',
        'addressType': '',
        'settlementType': '',
        'settlementName': '',
        'streetName': '',
        'bulidingNumber': '',
        'buildingBlockNumber': '',
        'incassationType': '',
        'openingTimeWeekdays': '',
        'openingTimeSunday': '',
        'openingTimeSaturday': '',
        'closingTimeWeekdays': '',
        'closingTimeSunday': '',
        'closingTimeSaturday': '',
    };
    validationMessages = {
        'time': {
            'required' : 'Не указано желательное время сдачи наличных (самостоятельно)/время прибытия инкассаторов в организацию',
            'pattern'  : 'Укажите время в формате ЧЧ:ММ'
        },
        'encashmentType': {
            'required' : 'Выберите значение из списка'
        },
        'incassationPeriod': {
            'required': 'Выберите значение из списка'
        },
        'dayOfWeek': {
        },
        'approxCash': {
            'required' : 'Не указана сумма, предназначенная к инкассации',
            'pattern'  : 'Значение не может быть больше 9999999999999999.99'
        },
        'currency': {
            'required': 'Не указан код валюты'
        },
        'directorContactPhone': {
            'required': 'Не указана контактная информация руководителя объекта'
        },
        'openingDate':
        {
            'required' : 'Не указана желательная дата начала обслуживания/дата открытия торговой точки',
            'pattern'  : 'Укажите дату в формате ДД.ММ.ГГГГ',
            'dateLessThanNow'  : 'Дата начала обслуживания не может быть меньше текущей даты',
        },
        'addressType': {
            'required': 'Выберите значение из списка'
        },
        'settlementType': {
            'required': 'Выберите значение из списка'
        },
        'settlementName': {},
        'streetName': {},
        'bulidingNumber': {},
        'buildingBlockNumber': {},
        'incassationType': {
            'required': 'Выберите значение из списка'
        },
        'openingTimeWeekdays': {
            'pattern': 'Введите время в формате ЧЧ:ММ',
            'mutural': 'Заполните поле'
        },
        'openingTimeSunday': {
            'pattern': 'Введите время в формате ЧЧ:ММ',
            'mutural': 'Заполните поле'
        },
        'openingTimeSaturday': {
            'pattern': 'Введите время в формате ЧЧ:ММ',
            'mutural': 'Заполните поле'
        },
        'closingTimeWeekdays': {
            'pattern': 'Введите время в формате ЧЧ:ММ',
            'mutural': 'Заполните поле'
        },
        'closingTimeSunday': {
            'pattern': 'Введите время в формате ЧЧ:ММ',
            'mutural': 'Заполните поле'
        },
        'closingTimeSaturday': {
            'pattern': 'Введите время в формате ЧЧ:ММ',
            'mutural': 'Заполните поле'
        },

    };

}
