import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    templateUrl: '/resources/angular/app/components/not-found-template.html',
    styleUrls: ['/resources/css/main.css']
})

export class NotFoundComponent {

    constructor(private router: Router) {
    }

}
