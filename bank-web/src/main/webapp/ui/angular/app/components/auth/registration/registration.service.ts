import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import {Credentials} from "../credentials";

@Injectable()
export class RegistrationService {
    private registrationUrl = '/api/v1/register';
    private regError : string = null;
    constructor(private http: Http) { }

    postData(cred: Credentials): Observable<string> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = { login: cred.login, password: cred.password };
        return this.http.post(this.registrationUrl, body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        let success = body.success || false;
        if (!success) {
            this.regError = body.error;
        } else {
            this.regError = "Success";
        }
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = err;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
