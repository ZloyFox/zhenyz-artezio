import {
    Component, Input, ComponentFactoryResolver, AfterViewInit, Output, EventEmitter
} from "@angular/core";
import {IncassationObjectService} from "../../service/incassation-object.service";
import {IncassationObject} from "../../data/incassation-object";
import {Observable} from "rxjs/Observable";

@Component({
    templateUrl: '/resources/angular/app/components/application/application-object/application-incassation-list-template.html',
    styles: [
        ".req:after{ color: #af2600; content: '*'; padding-left: 4px; }",
        ".container { padding-bottom: 200px; }",
        ".alert { padding: 8px; margin-bottom: 4px;}",
        ".sep { text-align: left; border-top: 1px solid #dadada; padding: 4px 17px 0; margin-top: 15px;}",
        ".buttons-default { padding-left: 8px; margin-top: 16px }",
        ".table tbody tr.selected {background-color: rgba(80,36,255,0.18);}",
        ".table tbody tr { cursor: pointer; }",
    ],
    selector: 'app-incass-list',
})

export class ApplicationIncassationListComponent implements AfterViewInit {

    //@ViewChild(ApplicationIncassationListDirective) dirHost: ApplicationIncassationListDirective;
    @Input() list: IncassationObject[];
    @Output() onChangedList = new EventEmitter<IncassationObject[]>();

    private obsIncassationObjects: Observable<IncassationObject[]> = null;
    private unusedIncassationObjects: IncassationObject[];
    private allIncassationObjects: IncassationObject[];

    private currentObject: IncassationObject;
    private currentRow: number;

    private popupSelectedObject: IncassationObject;

    private popupVisible: boolean;

    constructor(private incassationObjectService: IncassationObjectService,
                private componentFactoryResolver: ComponentFactoryResolver
    ) {
        this.currentObject = null;
        this.currentRow = -1;
        this.popupSelectedObject = null;
        this.unusedIncassationObjects = [];
        this.allIncassationObjects = [];
    }

    ngAfterViewInit() {
        console.log("List=" + JSON.stringify(this.list));
        this.obsIncassationObjects = this.incassationObjectService.getIncassationObjects();
        this.obsIncassationObjects.subscribe( data => {
            console.log("Recv " + data.length);
            this.allIncassationObjects = data;
            for (let i = 0; i < this.allIncassationObjects.length; ++i) {
                let objGlobal = this.allIncassationObjects[i];
                if (-1 == this.list.findIndex( r => r.id == objGlobal.id )) {
                    this.unusedIncassationObjects.push(this.allIncassationObjects[i]);
                }
            }
        });
    }

    selectRow(row: number, obj: IncassationObject) {
        this.currentRow = row;
        this.currentObject = obj;
    }

    addObjectFromPopup() {
        if (this.popupSelectedObject == null) {
            return;
        }
        let index = this.unusedIncassationObjects.findIndex( r => r.id == this.popupSelectedObject.id );
        this.unusedIncassationObjects.splice(index, 1);
        this.list.push(this.popupSelectedObject);
        this.onChangedList.emit(this.list);
        this.popupVisible = false;
        console.log("After add len = " + this.unusedIncassationObjects.length);
    }

    deleteObject() {
        let index = this.list.findIndex( r => r.id == this.currentObject.id );
        this.list.splice(index, 1);
        this.unusedIncassationObjects.push(this.currentObject);
        this.onChangedList.emit(this.list);
        this.currentObject = null;
        this.currentRow = -1;
    }

    showAddDialog() {
        this.popupSelectedObject = null;
        this.popupVisible = true;
    }

    closePopup() {
        this.popupSelectedObject = null;
        this.popupVisible = false;
    }

}