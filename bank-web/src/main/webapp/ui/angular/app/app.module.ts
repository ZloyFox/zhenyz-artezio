import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {AdminModule} from "./components/admin/admin.module";
import {AuthModule} from "./components/auth/auth.module";
import {IndexComponent} from "./components/index.component";
import {HttpService} from "./components/http/http.service";
import {HttpModule} from "@angular/http";
import {UserService} from "./components/service/user.service";
import {UserRoleService} from "./components/service/user-role.service";
import {IncassationModule} from "./components/incassation/incassation.module";
import {EncashmentTypeService} from "./components/service/encashment-type.service";
import {CurrencyService} from "./components/service/currency.service";
import {IncassationObjectService} from "./components/service/incassation-object.service";
import {AddressTypeService} from "./components/service/address-type.service";
import {IncassationPeriodService} from "./components/service/incassation-period.service";
import {NotFoundComponent} from "./components/not-found.component";
import {SessionExpiredComponent} from "./components/session-expired.component";
import {UserModule} from "./components/user/user.module";
import {SettlementTypeService} from "./components/service/settlement-type.service";
import {IncassationTypeService} from "./components/service/incassation-type.service";
import {ApplicationService} from "./components/service/application.service";
import {ApplicationTypeService} from "./components/service/application-type.service";
import {BankObjectService} from "./components/service/bank-object.service";
import {ApplicationModule} from "./components/application/application.module";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        AdminModule,
        AuthModule,
        ApplicationModule,
        IncassationModule,
        UserModule,
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        IndexComponent,
        NotFoundComponent,
        SessionExpiredComponent
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        HttpService,
        ApplicationService,
        ApplicationTypeService,
        AddressTypeService,
        BankObjectService,
        CurrencyService,
        EncashmentTypeService,
        IncassationObjectService,
        IncassationTypeService,
        IncassationPeriodService,
        SettlementTypeService,
        UserService,
        UserRoleService
    ]
})

export class AppModule { }