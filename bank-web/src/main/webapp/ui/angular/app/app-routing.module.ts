import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {IndexComponent} from "./components/index.component";
import {NotFoundComponent} from "./components/not-found.component";
import {SessionExpiredComponent} from "./components/session-expired.component";

const appRoutes : Routes = [
    { path: 'session_expired', component: SessionExpiredComponent},
    { path: '', component: IndexComponent, pathMatch: 'full' },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(appRoutes) ],
    exports: [ RouterModule ],
})

export class AppRoutingModule {}