package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.JwtTokenService;

import javax.ejb.Remote;

@Remote
public interface JwtTokenServiceBeanRemote extends JwtTokenService {

}
