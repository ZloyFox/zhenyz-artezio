package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.AddressTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface AddressTypeService {

	AddressTypeDto get(Long id);
	List<AddressTypeDto> list();
	AddressTypeDto update(AddressTypeDto model, Long updaterUserId) throws AccessViolationException;

}
