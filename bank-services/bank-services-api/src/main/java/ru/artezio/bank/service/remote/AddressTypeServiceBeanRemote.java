package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.AddressTypeService;

import javax.ejb.Remote;

@Remote
public interface AddressTypeServiceBeanRemote extends AddressTypeService {
}
