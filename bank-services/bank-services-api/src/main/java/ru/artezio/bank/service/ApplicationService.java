package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.ApplicationDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.exception.NotFoundException;

import java.util.List;

public interface ApplicationService {

	ApplicationDto get(Long id, Long userId) throws AccessViolationException, NotFoundException;
	List<ApplicationDto> findByCreatorId(Long userId, Integer offset, Integer count);
	ApplicationDto add(ApplicationDto model);
	void update(ApplicationDto model, Long updaterUserId) throws AccessViolationException, NotFoundException;

}
