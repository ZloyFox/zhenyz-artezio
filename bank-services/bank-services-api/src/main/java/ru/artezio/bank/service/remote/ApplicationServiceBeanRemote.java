package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.ApplicationService;

import javax.ejb.Remote;

@Remote
public interface ApplicationServiceBeanRemote extends ApplicationService {
}
