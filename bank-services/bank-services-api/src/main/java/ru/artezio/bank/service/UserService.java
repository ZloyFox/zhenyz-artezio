package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.exception.InvalidCredentialsException;
import ru.artezio.bank.service.exception.RegisterUserAccountException;
import ru.artezio.bank.service.exception.UserBlockedException;

import java.util.List;

public interface UserService {
	UserInfo createUser(UserDto accountDto) throws RegisterUserAccountException;
	UserDto getUser(Long id);
	UserInfo getUserInfo(Long id, Long demanderUserId) throws AccessViolationException;
	UserInfo update(UserDto model, Long updaterUserId) throws AccessViolationException;
	List<UserInfo> getAll(Long demanderUserId, Integer offset, Integer count) throws AccessViolationException;

	UserInfo authenticate(UserDto dto) throws InvalidCredentialsException, UserBlockedException;
	boolean isUserExists(String login);
}
