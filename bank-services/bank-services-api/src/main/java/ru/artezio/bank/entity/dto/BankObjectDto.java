package ru.artezio.bank.entity.dto;

import java.io.Serializable;

public class BankObjectDto implements Serializable {

	private Long id;
	private String name;
	private String address;
	private UserInfo creator;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UserInfo getCreator() {
		return creator;
	}

	public void setCreator(UserInfo creator) {
		this.creator = creator;
	}

	@Override public String toString() {
		return String.format("BankObjectDto[%s]", id);
	}

}
