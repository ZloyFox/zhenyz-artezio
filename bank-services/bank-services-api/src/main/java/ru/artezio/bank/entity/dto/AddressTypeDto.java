package ru.artezio.bank.entity.dto;

import java.io.Serializable;

public class AddressTypeDto implements Serializable {

	private Long id;
	private String text;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override public String toString() {
		return String.format("AddressTypeDto[%s,%s]", id, text);
	}

}
