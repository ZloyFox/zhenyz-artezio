package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.CurrencyService;

import javax.ejb.Remote;

@Remote
public interface CurrencyServiceBeanRemote extends CurrencyService {
}
