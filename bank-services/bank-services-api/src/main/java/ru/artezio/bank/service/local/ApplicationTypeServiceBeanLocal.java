package ru.artezio.bank.service.local;

import ru.artezio.bank.service.ApplicationTypeService;

import javax.ejb.Local;

@Local
public interface ApplicationTypeServiceBeanLocal extends ApplicationTypeService {
}
