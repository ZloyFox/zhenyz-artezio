package ru.artezio.bank.entity.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UserInfo implements Serializable {

    private String login;
    private Set<UserRoleDto> userRoles = new HashSet<>();
    private Boolean blocked = false;
    private Long id;

    public Long getId() {
        return id;
    }

    public Set<UserRoleDto> getUserRoles() {
        return this.userRoles;
    }

    public void setUserRoles(Set<UserRoleDto> userRoles) {
        this.userRoles = userRoles;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean hasRole(UserRoleDto role) {
        return this.userRoles.contains(role);
    }

    @Override
    public String toString() {
        return String.format("UserInfo[id=%d, login=%s]", id, login);
    }

}