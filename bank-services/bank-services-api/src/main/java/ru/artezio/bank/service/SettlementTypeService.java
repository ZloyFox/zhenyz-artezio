package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.SettlementTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface SettlementTypeService {

	SettlementTypeDto get(Long id);
	List<SettlementTypeDto> getAll();
	SettlementTypeDto update(SettlementTypeDto model, Long updaterUserId) throws AccessViolationException;

}
