package ru.artezio.bank.service.exception;

public class RegisterUserAccountException extends Exception {

	public final static String DEFAULT_MESSAGE = "Login and password must be not empty";

	public RegisterUserAccountException() {
		super(DEFAULT_MESSAGE);
	}

	public RegisterUserAccountException(String s) {
		super(s);
	}
}
