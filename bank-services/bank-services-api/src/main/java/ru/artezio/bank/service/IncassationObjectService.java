package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.IncassationObjectDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface IncassationObjectService {

	IncassationObjectDto get(Long id, Long userId) throws AccessViolationException;
	List<IncassationObjectDto> findByCreatorId(Long userId, Integer offset, Integer count);
	List<IncassationObjectDto> findAllForUser(UserInfo user, Integer offset, Integer count);
	IncassationObjectDto add(IncassationObjectDto model);
	IncassationObjectDto update(IncassationObjectDto model, Long updaterUserId) throws AccessViolationException;

}
