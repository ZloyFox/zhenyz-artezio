package ru.artezio.bank.service.local;

import ru.artezio.bank.service.BankObjectService;

import javax.ejb.Local;

@Local
public interface BankObjectServiceBeanLocal extends BankObjectService {
}
