package ru.artezio.bank.service.local;

import ru.artezio.bank.service.IncassationTypeService;

import javax.ejb.Local;

@Local
public interface IncassationTypeServiceBeanLocal extends IncassationTypeService {
}
