package ru.artezio.bank.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

public class UserDto implements Serializable {

	private String login;

	@JsonIgnore private String password;
	private Boolean blocked = false;
	private Set<UserRoleDto> userRoles;
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public Set<UserRoleDto> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRoleDto> userRoles) {
		this.userRoles = userRoles;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	@Override
	public String toString() {
		return String.format("UserDto[%d,%s,%s,%s, password null? %s]", id, login, userRoles != null ? userRoles.toString() : "null", Boolean.toString(blocked), Boolean.toString(password == null));
	}

	public boolean hasRole(UserRoleDto role) {
		return this.userRoles.contains(role);
	}

}
