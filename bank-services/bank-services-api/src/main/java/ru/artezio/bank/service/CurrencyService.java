package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.CurrencyDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface CurrencyService {

	CurrencyDto get(Short id);
	List<CurrencyDto> list();
	CurrencyDto update(CurrencyDto model, Long updaterUserId) throws AccessViolationException;

}
