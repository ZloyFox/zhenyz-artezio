package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.EncashmentTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface EncashmentTypeService {

	EncashmentTypeDto get(Long id);
	List<EncashmentTypeDto> getAll();
	EncashmentTypeDto update(EncashmentTypeDto model, Long updaterUserId) throws AccessViolationException;

}
