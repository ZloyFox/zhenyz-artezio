package ru.artezio.bank.entity.dto;

import java.io.Serializable;

public class CurrencyDto implements Serializable {

	private Short id;
	private String code;
	private String name;

	public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override public String toString() {
		return String.format("CurrencyDto[%d,%s,%s]", id, code, name);
	}

}
