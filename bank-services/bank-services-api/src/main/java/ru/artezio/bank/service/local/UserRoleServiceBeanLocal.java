package ru.artezio.bank.service.local;

import ru.artezio.bank.service.UserRoleService;

import javax.ejb.Local;

@Local
public interface UserRoleServiceBeanLocal extends UserRoleService {

}
