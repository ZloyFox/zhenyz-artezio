package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.IncassationTypeService;

import javax.ejb.Remote;

@Remote
public interface IncassationTypeServiceBeanRemote extends IncassationTypeService {
}
