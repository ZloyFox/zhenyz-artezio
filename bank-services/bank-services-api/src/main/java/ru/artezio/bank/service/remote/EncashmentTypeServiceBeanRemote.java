package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.EncashmentTypeService;

import javax.ejb.Remote;

@Remote
public interface EncashmentTypeServiceBeanRemote extends EncashmentTypeService {
}
