package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.UserInfo;

public interface JwtTokenService {
	/**
	 * Issue a token for specified user
	 * @return signed token string
	 */
	String issueToken(UserInfo user);

	/**
	 * Check if specified token is valid
	 * @param token Token to check
	 * @return true if valid; false if not
	 */
	boolean isTokenValid(String token);

	/**
	 * Extract user login
	 * @param token Valid token
	 * @return User login on success
	 */
	String getUserLoginFromToken(String token) throws Exception;

	/**
	 * Extract user id
	 * @param token Valid token
	 * @return User id on success
	 */
	Long getUserIdFromToken(String token) throws Exception;

	/**
	 * Load user from given token
	 * @param authToken jwt auth string
	 * @return UserModel or null if not found
	 */
	UserInfo getUserFromToken(String authToken) throws Exception;
}
