package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.IncassationPeriodDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface IncassationPeriodService {

	IncassationPeriodDto get(Long id);
	List<IncassationPeriodDto> getAll();
	IncassationPeriodDto update(IncassationPeriodDto model, Long updaterUserId) throws AccessViolationException;

}
