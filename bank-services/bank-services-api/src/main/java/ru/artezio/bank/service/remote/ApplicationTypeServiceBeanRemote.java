package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.ApplicationTypeService;

import javax.ejb.Remote;

@Remote
public interface ApplicationTypeServiceBeanRemote extends ApplicationTypeService {
}
