package ru.artezio.bank.entity.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ru.artezio.bank.util.DatetimeDeserializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApplicationDto implements Serializable {

	private Long id;
	private UserInfo creator;
	private BankObjectDto bankObject;
	private ApplicationTypeDto applicationType;

	@JsonDeserialize(using = DatetimeDeserializer.class)
	private Date date;

	private String inn;
	private String kpp;
	private String organizationName;
	private String ogrn;
	private String contactName;
	private String contactPhone;
	private String clientAccount;
	private String bankCode;
	private String bankAccountNumber;
	private String bankName;
	private String bankSwift;
	private String bankOtherInfo;

	private List<IncassationObjectDto> incassationObjects = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserInfo getCreator() {
		return creator;
	}

	public void setCreator(UserInfo creator) {
		this.creator = creator;
	}

	public BankObjectDto getBankObject() {
		return bankObject;
	}

	public void setBankObject(BankObjectDto bankObject) {
		this.bankObject = bankObject;
	}

	public ApplicationTypeDto getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(ApplicationTypeDto applicationType) {
		this.applicationType = applicationType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

	public String getKpp() {
		return kpp;
	}

	public void setKpp(String kpp) {
		this.kpp = kpp;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOgrn() {
		return ogrn;
	}

	public void setOgrn(String ogrn) {
		this.ogrn = ogrn;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getClientAccount() {
		return clientAccount;
	}

	public void setClientAccount(String clientAccount) {
		this.clientAccount = clientAccount;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankSwift() {
		return bankSwift;
	}

	public void setBankSwift(String bankSwift) {
		this.bankSwift = bankSwift;
	}

	public String getBankOtherInfo() {
		return bankOtherInfo;
	}

	public void setBankOtherInfo(String bankOtherInfo) {
		this.bankOtherInfo = bankOtherInfo;
	}

	public List<IncassationObjectDto> getIncassationObjects() {
		return incassationObjects;
	}

	public void setIncassationObjects(List<IncassationObjectDto> incassationObjects) {
		this.incassationObjects = incassationObjects;
	}

	@Override
	public String toString() {
		return String.format("ApplicationDto[id=%d]", id);
	}
}
