package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.BankObjectService;

import javax.ejb.Remote;

@Remote
public interface BankObjectServiceBeanRemote extends BankObjectService {
}
