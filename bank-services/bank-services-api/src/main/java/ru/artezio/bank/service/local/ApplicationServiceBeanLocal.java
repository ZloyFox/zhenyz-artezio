package ru.artezio.bank.service.local;

import ru.artezio.bank.service.ApplicationService;

import javax.ejb.Local;

@Local
public interface ApplicationServiceBeanLocal extends ApplicationService {
}
