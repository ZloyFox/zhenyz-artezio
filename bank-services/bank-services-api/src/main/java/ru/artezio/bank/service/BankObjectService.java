package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.BankObjectDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface BankObjectService {

	BankObjectDto create(BankObjectDto model, Long creatorId) throws AccessViolationException;
	List<BankObjectDto> getAll(Integer offset, Integer count, Long demanderUserId) throws AccessViolationException;
	BankObjectDto getById(Long id);
	List<BankObjectDto> findByAddress(String address, Integer offset, Integer count);
	BankObjectDto update(BankObjectDto model, Long updaterUserId) throws AccessViolationException;

}
