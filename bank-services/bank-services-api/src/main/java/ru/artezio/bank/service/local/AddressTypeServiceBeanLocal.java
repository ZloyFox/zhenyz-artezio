package ru.artezio.bank.service.local;

import ru.artezio.bank.service.AddressTypeService;

import javax.ejb.Local;

@Local
public interface AddressTypeServiceBeanLocal extends AddressTypeService {
}
