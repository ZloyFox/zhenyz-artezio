package ru.artezio.bank.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class UserRoleDto implements Serializable {

	private Long id;
	private String name;
	@JsonIgnore private boolean isGuest;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isGuest() {
		return isGuest;
	}

	public void setGuest(boolean guest) {
		isGuest = guest;
	}

	@Override
	public int hashCode() {
		return new Long(id).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserRoleDto)) {
			return false;
		}
		return this.id == ((UserRoleDto)obj).getId();
	}

	@Override
	public String toString() {
		return id.toString();
	}
}
