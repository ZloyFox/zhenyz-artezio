package ru.artezio.bank.entity.dto;

import java.io.Serializable;

public class LoginDto implements Serializable {

	private UserDto user;
	private String authToken;

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
}
