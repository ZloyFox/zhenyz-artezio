package ru.artezio.bank.service.local;

import ru.artezio.bank.service.JwtTokenService;

import javax.ejb.Local;

@Local
public interface JwtTokenServiceBeanLocal extends JwtTokenService {

}
