package ru.artezio.bank.service.local;

import ru.artezio.bank.service.EncashmentTypeService;

import javax.ejb.Local;

@Local
public interface EncashmentTypeServiceBeanLocal extends EncashmentTypeService {
}
