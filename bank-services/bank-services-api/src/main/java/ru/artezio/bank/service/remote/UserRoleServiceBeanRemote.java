package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.UserRoleService;

import javax.ejb.Remote;

@Remote
public interface UserRoleServiceBeanRemote extends UserRoleService {

}
