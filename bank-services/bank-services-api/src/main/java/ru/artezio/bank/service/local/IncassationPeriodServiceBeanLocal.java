package ru.artezio.bank.service.local;

import ru.artezio.bank.service.IncassationPeriodService;

import javax.ejb.Remote;

@Remote
public interface IncassationPeriodServiceBeanLocal extends IncassationPeriodService {
}
