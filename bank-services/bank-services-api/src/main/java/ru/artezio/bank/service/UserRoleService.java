package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.UserRoleDto;

import java.util.List;
import java.util.Set;

public interface UserRoleService {
	Set<UserRoleDto> getGuestRoles();
	List<UserRoleDto> getAll();
	UserRoleDto get(Long id);
	UserRoleDto getAdminRole();
}
