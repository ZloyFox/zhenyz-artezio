package ru.artezio.bank.service.local;

import ru.artezio.bank.service.SettlementTypeService;

import javax.ejb.Local;

@Local
public interface SettlementTypeServiceBeanLocal extends SettlementTypeService {
}
