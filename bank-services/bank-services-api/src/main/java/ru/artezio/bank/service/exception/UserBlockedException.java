package ru.artezio.bank.service.exception;

public class UserBlockedException extends Exception {

	public UserBlockedException() {
		super("User is blocked");
	}

}
