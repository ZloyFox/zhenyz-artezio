package ru.artezio.bank.util;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class Mapper {

	@Autowired private DozerBeanMapper mapper;

	public <T, S> List<S> mapList(List<T> sourceList, Class<S> destClass){
		List<S> resultList = new ArrayList<>(sourceList.size());
		for (T src : sourceList) {
			resultList.add(mapper.map(src, destClass));
		}
		return resultList;
	}

	public <T, S> Set<S> mapSet(Set<T> sourceSet, Class<S> destClass){
		Set<S> resultSet = new HashSet<>(sourceSet.size());
		for (T src : sourceSet) {
			resultSet.add(mapper.map(src, destClass));
		}
		return resultSet;
	}

	public <T, S> S mapModel(T source, Class<S> destClass) {
		return mapper.map(source,destClass);
	}

}
