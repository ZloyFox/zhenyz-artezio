package ru.artezio.bank.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ru.artezio.bank.entity.IncassationObjectStatus;
import ru.artezio.bank.util.DateDeserializer;
import ru.artezio.bank.util.SqlTimeDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

public class IncassationObjectDto implements Serializable {

	private Long id;

	@JsonFormat(pattern = "HH:mm:ss")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time time;

	private EncashmentTypeDto encashmentType;
	private IncassationTypeDto incassationType;
	private IncassationPeriodDto incassationPeriod;
	private Integer dayOfWeek;
	private BigDecimal approxCash;
	private CurrencyDto currency;
	private String directorContactPhone;

	@JsonDeserialize(using = DateDeserializer.class)
	private Date openingDate;

	private Boolean shared;
	private IncassationObjectStatus status = IncassationObjectStatus.ACTIVE;
	private UserDto creator;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time openingTimeWeekdays;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time openingTimeSaturday;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time openingTimeSunday;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time closingTimeWeekdays;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time closingTimeSaturday;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using= SqlTimeDeserializer.class)
	private Time closingTimeSunday;

	private AddressTypeDto addressType;
	private SettlementTypeDto settlementType;
	private String settlementName;
	private String streetName;
	private String buildingNumber;
	private String buildingBlockNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public EncashmentTypeDto getEncashmentType() {
		return encashmentType;
	}

	public void setEncashmentType(EncashmentTypeDto encashmentType) {
		this.encashmentType = encashmentType;
	}

	public IncassationPeriodDto getIncassationPeriod() {
		return incassationPeriod;
	}

	public void setIncassationPeriod(IncassationPeriodDto incassationPeriod) {
		this.incassationPeriod = incassationPeriod;
	}

	public Integer getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public BigDecimal getApproxCash() {
		return approxCash;
	}

	public void setApproxCash(BigDecimal approxCash) {
		this.approxCash = approxCash;
	}

	public CurrencyDto getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyDto currency) {
		this.currency = currency;
	}

	public String getDirectorContactPhone() {
		return directorContactPhone;
	}

	public void setDirectorContactPhone(String directorContactPhone) {
		this.directorContactPhone = directorContactPhone;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public Time getOpeningTimeWeekdays() {
		return openingTimeWeekdays;
	}

	public void setOpeningTimeWeekdays(Time openingTimeWeekdays) {
		this.openingTimeWeekdays = openingTimeWeekdays;
	}

	public Time getOpeningTimeSaturday() {
		return openingTimeSaturday;
	}

	public void setOpeningTimeSaturday(Time openingTimeSaturday) {
		this.openingTimeSaturday = openingTimeSaturday;
	}

	public Time getOpeningTimeSunday() {
		return openingTimeSunday;
	}

	public void setOpeningTimeSunday(Time openingTimeSunday) {
		this.openingTimeSunday = openingTimeSunday;
	}

	public Time getClosingTimeWeekdays() {
		return closingTimeWeekdays;
	}

	public void setClosingTimeWeekdays(Time closingTimeWeekdays) {
		this.closingTimeWeekdays = closingTimeWeekdays;
	}

	public Time getClosingTimeSaturday() {
		return closingTimeSaturday;
	}

	public void setClosingTimeSaturday(Time closingTimeSaturday) {
		this.closingTimeSaturday = closingTimeSaturday;
	}

	public Time getClosingTimeSunday() {
		return closingTimeSunday;
	}

	public void setClosingTimeSunday(Time closingTimeSunday) {
		this.closingTimeSunday = closingTimeSunday;
	}

	public AddressTypeDto getAddressType() {
		return addressType;
	}

	public void setAddressType(AddressTypeDto addressType) {
		this.addressType = addressType;
	}

	public SettlementTypeDto getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(SettlementTypeDto settlementType) {
		this.settlementType = settlementType;
	}

	public String getSettlementName() {
		return settlementName;
	}

	public void setSettlementName(String settlementName) {
		this.settlementName = settlementName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingBlockNumber() {
		return buildingBlockNumber;
	}

	public void setBuildingBlockNumber(String buildingBlockNumber) {
		this.buildingBlockNumber = buildingBlockNumber;
	}

	public Boolean isShared() {
		return shared;
	}

	public void setShared(Boolean shared) {
		this.shared = shared;
	}

	public IncassationObjectStatus getStatus() {
		return status;
	}

	public void setStatus(IncassationObjectStatus status) {
		this.status = status;
	}

	public IncassationTypeDto getIncassationType() {
		return incassationType;
	}

	public void setIncassationType(IncassationTypeDto incassationType) {
		this.incassationType = incassationType;
	}

	public UserDto getCreator() {
		return creator;
	}

	public void setCreator(UserDto creator) {
		this.creator = creator;
	}

	@Override public String toString() {
		return String.format("IncassationObjectDto[%d]", id);
	}

}
