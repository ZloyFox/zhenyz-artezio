package ru.artezio.bank.entity.dto;

import java.io.Serializable;

public class IncassationPeriodDto implements Serializable {

	private Long id;
	private String text;
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override public String toString() {
		return String.format("IncassationPeriodDto[%s,%s,%s]", id, text, description);
	}

}
