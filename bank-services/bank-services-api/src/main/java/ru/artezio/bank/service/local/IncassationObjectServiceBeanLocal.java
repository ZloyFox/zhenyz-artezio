package ru.artezio.bank.service.local;

import ru.artezio.bank.service.IncassationObjectService;

import javax.ejb.Local;

@Local
public interface IncassationObjectServiceBeanLocal extends IncassationObjectService {
}
