package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.IncassationPeriodService;

import javax.ejb.Remote;

@Remote
public interface IncassationPeriodServiceBeanRemote extends IncassationPeriodService {
}
