package ru.artezio.bank.service.exception;

public class AccessViolationException extends Exception {
    public AccessViolationException() {
        super("Access violation.");
    }
}
