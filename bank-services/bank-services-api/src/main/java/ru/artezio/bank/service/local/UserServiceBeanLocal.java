package ru.artezio.bank.service.local;

import ru.artezio.bank.service.UserService;

import javax.ejb.Local;

@Local
public interface UserServiceBeanLocal extends UserService {
}
