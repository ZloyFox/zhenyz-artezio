package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.IncassationObjectService;

import javax.ejb.Remote;

@Remote
public interface IncassationObjectServiceBeanRemote extends IncassationObjectService {
}
