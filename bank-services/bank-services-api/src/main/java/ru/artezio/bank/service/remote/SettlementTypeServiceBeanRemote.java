package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.SettlementTypeService;

import javax.ejb.Remote;

@Remote
public interface SettlementTypeServiceBeanRemote extends SettlementTypeService {
}
