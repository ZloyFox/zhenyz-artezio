package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.ApplicationTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface ApplicationTypeService {

	ApplicationTypeDto get(Long id);
	List<ApplicationTypeDto> getAll();
	ApplicationTypeDto update(ApplicationTypeDto model, Long updaterUserId) throws AccessViolationException;

}
