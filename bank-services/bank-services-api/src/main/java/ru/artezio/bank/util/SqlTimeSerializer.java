package ru.artezio.bank.util;


import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.sql.Time;
import java.text.SimpleDateFormat;

/**
 * Serialize time values with format 'HH:mm'
 */
public class SqlTimeSerializer implements JsonSerializer<Time> {

	@Override
	public JsonElement serialize(Time time, Type typeOfSrc, JsonSerializationContext context) {
		SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
		return new JsonPrimitive(fmt.format(time));
	}

}