package ru.artezio.bank.service;

import ru.artezio.bank.entity.dto.IncassationTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;

import java.util.List;

public interface IncassationTypeService {

	IncassationTypeDto get(Long id);
	List<IncassationTypeDto> getAll();
	IncassationTypeDto update(IncassationTypeDto model, Long updaterUserId) throws AccessViolationException;

}
