package ru.artezio.bank.service.local;

import ru.artezio.bank.service.CurrencyService;

import javax.ejb.Local;

@Local
public interface CurrencyServiceBeanLocal extends CurrencyService {
}
