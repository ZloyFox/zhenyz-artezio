package ru.artezio.bank.service.remote;

import ru.artezio.bank.service.UserService;

import javax.ejb.Remote;

@Remote
public interface UserServiceBeanRemote extends UserService {
}
