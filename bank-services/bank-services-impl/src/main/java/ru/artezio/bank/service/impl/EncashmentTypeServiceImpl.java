package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.EncashmentTypeDaoBeanRemote;
import ru.artezio.bank.entity.EncashmentTypeModel;
import ru.artezio.bank.entity.dto.EncashmentTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.EncashmentTypeServiceBeanLocal;
import ru.artezio.bank.service.remote.EncashmentTypeServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class EncashmentTypeServiceImpl extends BaseService
		implements EncashmentTypeServiceBeanRemote, EncashmentTypeServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/EncashmentTypeDaoImpl!ru.artezio.bank.dao.remote.EncashmentTypeDaoBeanRemote")
	private EncashmentTypeDaoBeanRemote encashmentTypeDao;

	@Override
	public EncashmentTypeDto get(Long id) {
		return mapModelToDto(encashmentTypeDao.getById(id), EncashmentTypeDto.class);
	}

	@Override
	public List<EncashmentTypeDto> getAll() {
		return mapModelListToDto(encashmentTypeDao.getAll(), EncashmentTypeDto.class);
	}

	@Override
	public EncashmentTypeDto update(EncashmentTypeDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(encashmentTypeDao.update(mapDtoToModel(model, EncashmentTypeModel.class), updaterUserId), EncashmentTypeDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

}

