package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.helper.OrderDirection;
import ru.artezio.bank.dao.helper.PagingInfo;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.remote.IncassationObjectDaoBeanRemote;
import ru.artezio.bank.entity.IncassationObjectModel;
import ru.artezio.bank.entity.dto.IncassationObjectDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.IncassationObjectServiceBeanLocal;
import ru.artezio.bank.service.local.UserRoleServiceBeanLocal;
import ru.artezio.bank.service.remote.IncassationObjectServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.Collections;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class IncassationObjectServiceImpl extends BaseService
		implements IncassationObjectServiceBeanRemote, IncassationObjectServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0//IncassationObjectDaoImpl!ru.artezio.bank.dao.remote.IncassationObjectDaoBeanRemote")
	private IncassationObjectDaoBeanRemote incassationObjectDao;

	@EJB private UserRoleServiceBeanLocal userRoleService;

	@Override
	public IncassationObjectDto get(Long id, Long userId) throws AccessViolationException {
		try {
			return mapModelToDto(incassationObjectDao.getById(id, userId), IncassationObjectDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}


	@Override
	public IncassationObjectDto add(IncassationObjectDto model) {
		return mapModelToDto(incassationObjectDao.create(mapDtoToModel(model, IncassationObjectModel.class)), IncassationObjectDto.class);
	}

	@Override
	public IncassationObjectDto update(IncassationObjectDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(incassationObjectDao.update(mapDtoToModel(model, IncassationObjectModel.class), updaterUserId), IncassationObjectDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	@Override
	public List<IncassationObjectDto> findByCreatorId(Long userId, Integer offset, Integer count) {
		PagingInfo paging = new PagingInfo(offset, count);
		QueryInfo info = new QueryInfo(OrderDirection.DESC, paging);
		info.setSortedFields("id");
		return mapModelListToDto(incassationObjectDao.findByCreatorId(userId, info), IncassationObjectDto.class);
	}

	@Override
	public List<IncassationObjectDto> findAllForUser(UserInfo user, Integer offset, Integer count) {
		List<IncassationObjectDto> resultList;
		// Select all incassation objects for admin
		if (user.hasRole(userRoleService.getAdminRole())) {
			try {
				PagingInfo paging = new PagingInfo(offset, count);
				QueryInfo info = new QueryInfo(OrderDirection.DESC, paging);
				info.setSortedFields("id");
				resultList = mapModelListToDto(incassationObjectDao.getAll(user.getId(), info), IncassationObjectDto.class);
			} catch (ru.artezio.bank.dao.exception.AccessViolationException ex) {
				log.error("User {} has admin role but still catched AccessViolationException", user);
				return Collections.emptyList();
			}
		} else {
			// Get user-created objects for user
			resultList = findByCreatorId(user.getId(), offset, count);
		}
		return resultList;
	}


}

