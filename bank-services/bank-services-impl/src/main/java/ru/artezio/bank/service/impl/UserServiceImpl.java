package ru.artezio.bank.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.exception.UserAlreadyExistsException;
import ru.artezio.bank.dao.helper.OrderDirection;
import ru.artezio.bank.dao.helper.PagingInfo;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.remote.UserDaoBeanRemote;
import ru.artezio.bank.entity.UserModel;
import ru.artezio.bank.entity.UserRoleModel;
import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.entity.dto.UserRoleDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.exception.InvalidCredentialsException;
import ru.artezio.bank.service.exception.RegisterUserAccountException;
import ru.artezio.bank.service.exception.UserBlockedException;
import ru.artezio.bank.service.local.UserRoleServiceBeanLocal;
import ru.artezio.bank.service.local.UserServiceBeanLocal;
import ru.artezio.bank.service.remote.UserServiceBeanRemote;
import ru.artezio.bank.util.Mapper;
import ru.artezio.bank.util.validator.UserRegistrationValidator;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class UserServiceImpl extends BaseService implements UserServiceBeanRemote, UserServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/UserDaoImpl!ru.artezio.bank.dao.remote.UserDaoBeanRemote")
	private UserDaoBeanRemote userDao;

	@EJB private UserRoleServiceBeanLocal userRoleService;

	@Autowired private Mapper mapper;

	@Override
	public UserDto getUser(Long id) {
		return mapModelToDto(userDao.getById(id), UserDto.class);
	}

	@Override
	public UserInfo getUserInfo(Long id, Long demanderUserId) throws AccessViolationException {
		if (id.equals(demanderUserId) || userDao.isUserAdmin(demanderUserId)) {
			return mapper.mapModel(getUser(id), UserInfo.class);
		} else {
			throw new AccessViolationException();
		}
	}

	/**
	 * Try create new user account
	 * @param userDto UserModel DTO
	 * @return created User on success; null on failure
	 * @throws RegisterUserAccountException if specified login already exists
	 */
	@Override
	public UserInfo createUser(UserDto userDto) throws RegisterUserAccountException {
		log.trace("Start register new user account, login={}", userDto.getLogin());
		validateUserData(userDto);
		UserModel userModel = createUserModel(userDto);
		log.trace("Model validated, adding new account, login {}", userDto.getLogin());
		try {
			userModel = userDao.create(userModel);
		} catch (UserAlreadyExistsException ex) {
			throw new RegisterUserAccountException(ex.getMessage());
		}
		return mapper.mapModel(userModel, UserInfo.class);
	}

	@Override
	public List<UserInfo> getAll(Long demanderUserId, Integer offset, Integer count) throws AccessViolationException {
		PagingInfo paging = new PagingInfo(offset, count);
		QueryInfo info = new QueryInfo(OrderDirection.DESC, paging);
		info.setSortedFields("id");
		try {
			return mapper.mapList(userDao.getAll(demanderUserId, info), UserInfo.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	@Override
	public boolean isUserExists(String login) {
		return userDao.isLoginExists(login);
	}

	/**
	 * Updates UserModel
	 * @implNote If provided model had no guest roles, then all available guest roles are appended and saved
	 */
	@Override
	public UserInfo update(UserDto model, Long updaterUserId) throws AccessViolationException {
		// Preserve password (not sent via DTO, being null)
		UserDto storedModel = getUser(model.getId());
		model.setPassword(storedModel.getPassword());

		// Check that user has any Guest roles
		Set<UserRoleDto> guestRoles = userRoleService.getGuestRoles();
		if (Collections.disjoint(guestRoles, model.getUserRoles())) {
			// Apply guest roles to this model
			model.getUserRoles().addAll(guestRoles);
		}
		try {
			return mapper.mapModel(userDao.update(mapDtoToModel(model, UserModel.class), updaterUserId), UserInfo.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	/**
	 * Check if user credentials are valid
	 * @param dto - credentials
	 * @return user model on success
	 * @throws InvalidCredentialsException if either login or password is incorrect
	 */
	@Override
	public UserInfo authenticate(UserDto dto) throws InvalidCredentialsException, UserBlockedException {
		log.warn("Dto " + dto);
		UserModel user = userDao.findByLoginPassword(dto.getLogin(), dto.getPassword());
		if (null == user) {
			log.info("Invalid login for user " + dto.getLogin());
			throw new InvalidCredentialsException();
		}
		if (user.isBlocked()) {
			log.debug("User " + dto.getLogin() + " is blocked");
			throw new UserBlockedException();
		}
		log.debug("Credentials are valid for user " + user.getLogin());
		return mapModelToDto(user, UserInfo.class);
	}

	// internal

	/**
	 * Validates specified UserDto
	 * @param userDto DTO
	 * @throws RegisterUserAccountException if model is invalid
	 */
	private void validateUserData(UserDto userDto) throws RegisterUserAccountException {
		UserRegistrationValidator validator = new UserRegistrationValidator();
		// throws RegisterUserAccountException if not valid
		validator.validate(userDto);
		// Login must be unique
		if (loginExist(userDto.getLogin())) {
			log.info("Model invalid: login {} already exists", userDto.getLogin());
			throw new RegisterUserAccountException(
					String.format("Login %s already exists", userDto.getLogin()));
		}
	}

	/**
	 * Create new UserModel and write hashed password
	 * @param userDto DTO
	 * @return Created UserModel
	 */
	private UserModel createUserModel(UserDto userDto) {
		UserModel userModel = mapper.mapModel(userDto, UserModel.class);

		// Add guest roles
		Set<UserRoleDto> guestRoles = userRoleService.getGuestRoles();
		userModel.setUserRoles(mapModelSetToDto(guestRoles, UserRoleModel.class));
		return userModel;
	}

	/**
	 * Check if specified login exists
	 * @param login login to check
	 * @return true if exists
	 */
	private boolean loginExist(String login) {
		return userDao.isLoginExists(login);
	}

}
