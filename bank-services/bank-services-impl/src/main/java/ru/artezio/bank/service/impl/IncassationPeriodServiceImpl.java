package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.IncassationPeriodDaoBeanRemote;
import ru.artezio.bank.entity.IncassationPeriodModel;
import ru.artezio.bank.entity.dto.IncassationPeriodDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.IncassationPeriodServiceBeanLocal;
import ru.artezio.bank.service.remote.IncassationPeriodServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class IncassationPeriodServiceImpl extends BaseService
		implements IncassationPeriodServiceBeanRemote, IncassationPeriodServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/IncassationPeriodDaoImpl!ru.artezio.bank.dao.remote.IncassationPeriodDaoBeanRemote")
	private IncassationPeriodDaoBeanRemote incassationPeriodDao;

	@Override
	public IncassationPeriodDto get(Long id) {
		return mapModelToDto(incassationPeriodDao.getById(id), IncassationPeriodDto.class);
	}

	@Override
	public IncassationPeriodDto update(IncassationPeriodDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(incassationPeriodDao.update(mapDtoToModel(model, IncassationPeriodModel.class), updaterUserId), IncassationPeriodDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	@Override
	public List<IncassationPeriodDto> getAll() {
		return mapModelListToDto(incassationPeriodDao.getAll(), IncassationPeriodDto.class);
	}

}

