package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.CurrencyDaoBeanRemote;
import ru.artezio.bank.entity.CurrencyModel;
import ru.artezio.bank.entity.dto.CurrencyDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.CurrencyServiceBeanLocal;
import ru.artezio.bank.service.remote.CurrencyServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class CurrencyServiceImpl extends BaseService implements CurrencyServiceBeanRemote, CurrencyServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/CurrencyDaoImpl!ru.artezio.bank.dao.remote.CurrencyDaoBeanRemote")
	private CurrencyDaoBeanRemote currencyDao;

	@Override
	public CurrencyDto get(Short id) {
		return mapModelToDto(currencyDao.getById(id), CurrencyDto.class);
	}

	@Override
	public List<CurrencyDto> list() {
		return mapModelListToDto(currencyDao.getAll(), CurrencyDto.class);
	}

	@Override
	public CurrencyDto update(CurrencyDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(currencyDao.update(mapDtoToModel(model, CurrencyModel.class), updaterUserId), CurrencyDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

}

