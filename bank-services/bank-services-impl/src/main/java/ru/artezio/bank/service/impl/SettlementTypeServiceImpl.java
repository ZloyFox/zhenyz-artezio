package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.SettlementTypeDaoBeanRemote;
import ru.artezio.bank.entity.SettlementTypeModel;
import ru.artezio.bank.entity.dto.SettlementTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.SettlementTypeServiceBeanLocal;
import ru.artezio.bank.service.remote.SettlementTypeServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class SettlementTypeServiceImpl extends BaseService
		implements SettlementTypeServiceBeanRemote, SettlementTypeServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/SettlementTypeDaoImpl!ru.artezio.bank.dao.remote.SettlementTypeDaoBeanRemote")
	private SettlementTypeDaoBeanRemote settlementTypeDao;

	@Override
	public SettlementTypeDto get(Long id) {
		return mapModelToDto(settlementTypeDao.getById(id), SettlementTypeDto.class);
	}

	@Override
	public List<SettlementTypeDto> getAll() {
		return mapModelListToDto(settlementTypeDao.getAll(), SettlementTypeDto.class);
	}

	@Override
	public SettlementTypeDto update(SettlementTypeDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(settlementTypeDao.update(mapDtoToModel(model, SettlementTypeModel.class), updaterUserId), SettlementTypeDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

}

