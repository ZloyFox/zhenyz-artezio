package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.helper.OrderDirection;
import ru.artezio.bank.dao.helper.PagingInfo;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.remote.ApplicationDaoBeanRemote;
import ru.artezio.bank.entity.ApplicationModel;
import ru.artezio.bank.entity.dto.ApplicationDto;
import ru.artezio.bank.service.exception.NotFoundException;
import ru.artezio.bank.service.local.ApplicationServiceBeanLocal;
import ru.artezio.bank.service.remote.ApplicationServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class ApplicationServiceImpl extends BaseService implements ApplicationServiceBeanRemote, ApplicationServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/ApplicationDaoImpl!ru.artezio.bank.dao.remote.ApplicationDaoBeanRemote")
	private ApplicationDaoBeanRemote applicationDao;

	@Override
	public ApplicationDto get(Long id, Long userId) throws NotFoundException {
		try {
			return mapModelToDto(applicationDao.getById(id, userId), ApplicationDto.class);
		} catch (NoResultException ex) {
			throw new NotFoundException();
		}
	}


	@Override
	public ApplicationDto add(ApplicationDto dto) {
		ApplicationModel model = mapDtoToModel(dto, ApplicationModel.class);
		return mapModelToDto(applicationDao.create(model), ApplicationDto.class);
	}

	@Override
	public void update(ApplicationDto dto, Long updaterUserId) throws NotFoundException {
		try {
			applicationDao.update(mapDtoToModel(dto, ApplicationModel.class), updaterUserId);
		} catch (NoResultException ex) {
			throw new NotFoundException();
		}
	}

	@Override
	public List<ApplicationDto> findByCreatorId(Long userId, Integer offset, Integer count) {
		PagingInfo paging = new PagingInfo(offset, count);
		QueryInfo info = new QueryInfo(OrderDirection.DESC, paging);
		info.setSortedFields("id");
		return mapModelListToDto(applicationDao.findByCreatorId(userId, info), ApplicationDto.class);
	}

}

