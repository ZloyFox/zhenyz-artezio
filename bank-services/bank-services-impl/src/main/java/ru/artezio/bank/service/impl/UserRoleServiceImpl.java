package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.UserRoleDaoBeanRemote;
import ru.artezio.bank.entity.dto.UserRoleDto;
import ru.artezio.bank.service.local.UserRoleServiceBeanLocal;
import ru.artezio.bank.service.remote.UserRoleServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;
import java.util.Set;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class UserRoleServiceImpl extends BaseService
		implements UserRoleServiceBeanRemote, UserRoleServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/UserRoleDaoImpl!ru.artezio.bank.dao.remote.UserRoleDaoBeanRemote")
	private UserRoleDaoBeanRemote userRoleDao;

	@Override
	public Set<UserRoleDto> getGuestRoles() {
		return mapModelSetToDto(userRoleDao.getGuestRoles(), UserRoleDto.class);
	}

	@Override
	public List<UserRoleDto> getAll() {
		return mapModelListToDto(userRoleDao.getAll(), UserRoleDto.class);
	}

	public UserRoleDto get(Long id) {
		return mapModelToDto(userRoleDao.getById(id), UserRoleDto.class);
	}

	public UserRoleDto getAdminRole() {
		return mapModelToDto(userRoleDao.findByName("Admin"), UserRoleDto.class);
	}

}

