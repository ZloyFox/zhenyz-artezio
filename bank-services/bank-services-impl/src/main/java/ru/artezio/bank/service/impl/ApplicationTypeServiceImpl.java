package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.ApplicationTypeDaoBeanRemote;
import ru.artezio.bank.entity.ApplicationTypeModel;
import ru.artezio.bank.entity.dto.ApplicationTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.ApplicationTypeServiceBeanLocal;
import ru.artezio.bank.service.remote.ApplicationTypeServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class ApplicationTypeServiceImpl extends BaseService
		implements ApplicationTypeServiceBeanRemote, ApplicationTypeServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/ApplicationTypeDaoImpl!ru.artezio.bank.dao.remote.ApplicationTypeDaoBeanRemote")
	private ApplicationTypeDaoBeanRemote applicationTypeDao;

	@Override
	public ApplicationTypeDto get(Long id) {
		return mapModelToDto(applicationTypeDao.getById(id), ApplicationTypeDto.class);
	}

	@Override
	public ApplicationTypeDto update(ApplicationTypeDto model, Long updaterUserId) throws AccessViolationException {
		try {
			ApplicationTypeModel updated = applicationTypeDao.update(mapDtoToModel(model, ApplicationTypeModel.class), updaterUserId);
			return mapModelToDto(updated, ApplicationTypeDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	@Override
	public List<ApplicationTypeDto> getAll() {
		return mapModelListToDto(applicationTypeDao.getAll(), ApplicationTypeDto.class);
	}

}

