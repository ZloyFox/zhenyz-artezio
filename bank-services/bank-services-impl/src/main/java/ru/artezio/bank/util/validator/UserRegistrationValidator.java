package ru.artezio.bank.util.validator;

import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.service.exception.RegisterUserAccountException;

import java.util.regex.Pattern;

public class UserRegistrationValidator {

	private static Pattern whitespaceOrEmptyPattern = Pattern.compile("^$|\\s+");

	public void validate(UserDto userDto) throws RegisterUserAccountException {
		if (userDto.getLogin() == null || userDto.getPassword() == null
				|| whitespaceOrEmptyPattern.matcher(userDto.getLogin()).matches()
				|| whitespaceOrEmptyPattern.matcher(userDto.getPassword()).matches()) {
			throw new RegisterUserAccountException();
		}
	}
}
