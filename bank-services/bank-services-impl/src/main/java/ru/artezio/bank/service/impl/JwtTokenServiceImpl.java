package ru.artezio.bank.service.impl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.entity.dto.UserDto;
import ru.artezio.bank.entity.dto.UserInfo;
import ru.artezio.bank.service.local.JwtTokenServiceBeanLocal;
import ru.artezio.bank.service.local.UserServiceBeanLocal;
import ru.artezio.bank.service.remote.JwtTokenServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Stateless(mappedName="JwtTokenService")
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class JwtTokenServiceImpl extends BaseService implements JwtTokenServiceBeanRemote, JwtTokenServiceBeanLocal {

	@EJB private UserServiceBeanLocal userService;

	private final static String FIELD_LOGIN = "username";
	private final static String FIELD_ID = "userID";
	private final static String FIELD_ROLES = "roles";
	private final static String ISSUER = "test_issuer";

	// Secure key
	private static final String JWT_KEY = "abc123";

	/**
	 * Issue new jwt token to specified user object
	 * @param user - valid user model
	 * @return string representation of Jwt token
	 */
	public String issueToken(UserInfo user) {
		Map<String, Object> tokenData = new HashMap<>();
		tokenData.put(FIELD_ID, user.getId().toString());
		tokenData.put(FIELD_LOGIN, user.getLogin());
		tokenData.put(FIELD_ROLES, user.getUserRoles());
		JwtBuilder jwtBuilder = Jwts.builder();
		jwtBuilder.setClaims(tokenData);
		// Mark expiry and issue dates
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, 1);
		jwtBuilder.setExpiration(calendar.getTime());
		jwtBuilder.setIssuedAt(new Date());
		// Issuer
		// TODO: Advanced Issuer
		jwtBuilder.setIssuer(ISSUER);
		return jwtBuilder.signWith(SignatureAlgorithm.HS512, JwtTokenServiceImpl.JWT_KEY).compact();
	}

	/**
	 * Test jwt token for validity
	 * @param token - jwt token string
	 * @return true if token is valid and not expired; false otherwise
	 */
	public boolean isTokenValid(String token) {
		boolean valid;
		try {
			log.trace("Start validate token");
			// Parse argument
			Claims claims = getClaimsOfToken(token);
			log.trace("Parsed claims");
			// Check issuer valid
			valid = (0 == claims.getIssuer().compareTo(ISSUER));
			log.trace("After check issuer valid = {}", valid);
			// Check not expired
			log.trace("Expiration {}", claims.getExpiration());
			valid &= (0 < claims.getExpiration().compareTo(new Date()));
			log.trace("After check expiration valid = {}", valid);
			// Check issued correctly
			log.trace("Issued {}", claims.getIssuedAt());
			valid &= (0 > claims.getIssuedAt().compareTo(new Date()));
			log.trace("After check issued at, valid = {}", valid);
		} catch (Exception ex) {
			log.trace("Ecxeption {}", ex);
			valid = false;
		}
		return valid;
	}

	public String getUserLoginFromToken(String token) throws Exception {
		return (String)getClaimsOfToken(token).get(FIELD_LOGIN);
	}

	public Long getUserIdFromToken(String token) throws Exception {
		return Long.parseLong((String)(getClaimsOfToken(token).get(FIELD_ID)));
	}

	public UserInfo getUserFromToken(String token) throws Exception {
		Long userId = getUserIdFromToken(token);
		UserDto user = userService.getUser(userId);
		if (null == user) {
			throw new Exception("Invalid user id");
		}
		return mapper.mapModel(user, UserInfo.class);
	}

	// internal

	private Claims getClaimsOfToken(String token) {
		return Jwts.parser().setSigningKey(JwtTokenServiceImpl.JWT_KEY).parseClaimsJws(token).getBody();
	}

}
