package ru.artezio.bank.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.bank.util.Mapper;

import java.util.List;
import java.util.Set;

abstract class BaseService {

	@Autowired protected Mapper mapper;

	protected Logger log = LoggerFactory.getLogger(getClass().getName());

	protected <T, S> S mapModelToDto(T model, Class<S> dtoClass) {
		return model == null ? null : mapper.mapModel(model, dtoClass);
	}

	protected <T, S> T mapDtoToModel(S dto, Class<T> modelClass) {
		return dto == null ? null : mapper.mapModel(dto, modelClass);
	}

	protected <T, S> List<S> mapModelListToDto(List<T> modelList, Class<S> dtoClass) {
		return mapper.mapList(modelList, dtoClass);
	}

	protected <T, S> Set<S> mapModelSetToDto(Set<T> modelSet, Class<S> dtoClass) {
		return mapper.mapSet(modelSet, dtoClass);
	}
}
