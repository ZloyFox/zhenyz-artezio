package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.IncassationTypeDaoBeanRemote;
import ru.artezio.bank.entity.IncassationTypeModel;
import ru.artezio.bank.entity.dto.IncassationTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.IncassationTypeServiceBeanLocal;
import ru.artezio.bank.service.remote.IncassationTypeServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class IncassationTypeServiceImpl extends BaseService
		implements IncassationTypeServiceBeanRemote, IncassationTypeServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/IncassationTypeDaoImpl!ru.artezio.bank.dao.remote.IncassationTypeDaoBeanRemote")
	private IncassationTypeDaoBeanRemote incassationTypeDao;

	@Override
	public IncassationTypeDto get(Long id) {
		return mapModelToDto(incassationTypeDao.getById(id), IncassationTypeDto.class);
	}

	@Override
	public List<IncassationTypeDto> getAll() {
		return mapModelListToDto(incassationTypeDao.getAll(), IncassationTypeDto.class);
	}

	@Override
	public IncassationTypeDto update(IncassationTypeDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(incassationTypeDao.update(mapDtoToModel(model, IncassationTypeModel.class), updaterUserId), IncassationTypeDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

}

