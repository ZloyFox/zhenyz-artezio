package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.helper.OrderDirection;
import ru.artezio.bank.dao.helper.PagingInfo;
import ru.artezio.bank.dao.helper.QueryInfo;
import ru.artezio.bank.dao.remote.BankObjectDaoBeanRemote;
import ru.artezio.bank.entity.BankObjectModel;
import ru.artezio.bank.entity.dto.BankObjectDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.BankObjectServiceBeanLocal;
import ru.artezio.bank.service.remote.BankObjectServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class BankObjectServiceImpl extends BaseService implements BankObjectServiceBeanRemote, BankObjectServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/BankObjectDaoImpl!ru.artezio.bank.dao.remote.BankObjectDaoBeanRemote")
	private BankObjectDaoBeanRemote bankObjectDao;

	@Override
	public BankObjectDto create(BankObjectDto model, Long creatorId) throws AccessViolationException {
		try {
			return mapModelToDto(bankObjectDao.create(mapDtoToModel(model, BankObjectModel.class), creatorId), BankObjectDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	@Override
	public List<BankObjectDto> getAll(Integer offset, Integer count, Long demanderUserId) throws AccessViolationException {
		PagingInfo paging = new PagingInfo(offset, count);
		QueryInfo queryInfo = new QueryInfo(OrderDirection.ASC, paging);
		queryInfo.setSortedFields("id");
		try {
			return mapModelListToDto(bankObjectDao.getAll(queryInfo, demanderUserId), BankObjectDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

	@Override
	public BankObjectDto getById(Long id) {
		return mapModelToDto(bankObjectDao.getById(id), BankObjectDto.class);
	}

	@Override
	public List<BankObjectDto> findByAddress(String address, Integer offset, Integer count) {
		PagingInfo paging = new PagingInfo(offset, count);
		QueryInfo queryInfo = new QueryInfo(OrderDirection.ASC, paging);
		queryInfo.setSortedFields("address");
		return mapModelListToDto(bankObjectDao.findByAddress(address, queryInfo), BankObjectDto.class);
	}

	@Override
	public BankObjectDto update(BankObjectDto model, Long updaterUserId) throws AccessViolationException {
		try {
			return mapModelToDto(bankObjectDao.update(mapDtoToModel(model, BankObjectModel.class), updaterUserId),
					BankObjectDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException e) {
			throw new AccessViolationException();
		}
	}

}

