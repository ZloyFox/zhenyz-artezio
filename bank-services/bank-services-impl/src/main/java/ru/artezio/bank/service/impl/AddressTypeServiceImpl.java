package ru.artezio.bank.service.impl;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Service;
import ru.artezio.bank.dao.remote.AddressTypeDaoBeanRemote;
import ru.artezio.bank.entity.AddressTypeModel;
import ru.artezio.bank.entity.dto.AddressTypeDto;
import ru.artezio.bank.service.exception.AccessViolationException;
import ru.artezio.bank.service.local.AddressTypeServiceBeanLocal;
import ru.artezio.bank.service.remote.AddressTypeServiceBeanRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Service
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class AddressTypeServiceImpl extends BaseService implements AddressTypeServiceBeanRemote, AddressTypeServiceBeanLocal {

	@EJB(mappedName = "ejb:bank-domain-ear-1.0/bank-domain-impl-1.0/AddressTypeDaoImpl!ru.artezio.bank.dao.remote.AddressTypeDaoBeanRemote")
	private AddressTypeDaoBeanRemote addressTypeDao;

	@Override
	public AddressTypeDto get(Long id) {
		return mapModelToDto(addressTypeDao.getById(id), AddressTypeDto.class);
	}

	@Override
	public List<AddressTypeDto> list() {
		return mapModelListToDto(addressTypeDao.getAll(), AddressTypeDto.class);
	}

	@Override
	public AddressTypeDto update(AddressTypeDto dto, Long updaterUserId) throws AccessViolationException {
		AddressTypeModel model = mapDtoToModel(dto, AddressTypeModel.class);
		try {
			return mapModelToDto(addressTypeDao.update(model, updaterUserId), AddressTypeDto.class);
		} catch (ru.artezio.bank.dao.exception.AccessViolationException ex) {
			throw new AccessViolationException();
		}
	}

}

