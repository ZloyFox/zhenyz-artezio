package ru.artezio.bank.entity;

public enum IncassationObjectStatus {
    ACTIVE,
    BLOKED,
    DELETED;
}